<?php
namespace  common\utils;


use Qiniu\Auth;

class Qiniu{
    public  function token(){
        $auth  = new Auth(\Yii::$app->params['ak'],\Yii::$app->params['sk']);
        return [
            'token'=>$auth->uploadToken(\Yii::$app->params['bucket']),
            'imageHost'=>\Yii::$app->params['imageHost']
        ];
    }
}