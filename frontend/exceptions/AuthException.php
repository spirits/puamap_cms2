<?php
namespace frontend\exceptions;
use yii\base\Exception;
class AuthException extends Exception{
    public function __construct()
    {
        parent::__construct("未登录", 403, null);
    }
}
