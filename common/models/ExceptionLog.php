<?php
namespace common\models;
/**
 * Class ExceptionLog
 * @package common\models
 * @property int $id
 * @property string $name
 * @property int $code
 * @property int $line
 * @property string $file
 * @property string $message
 * @property string $trace
 * @property int $create_time
 * @property int $update_time
 * @property int $id_deleted
 * @property int $status
 * @property int $operate_id
 *
 */
class ExceptionLog extends ParentActiveRecord{
    public static function tableName()
    {
        return "{{%exception_log}}";

    }

    public function attributeLabels()
    {
        return parent::attributeLabels();
    }
}