<?php
namespace frontend\controllers;
use common\models\ParentActiveRecord;
use frontend\forms\AssistantForm;
use frontend\models\Assistant;
use frontend\models\Role;

class AssistantController extends BaseController{
    public $condition = ['role_id'=>Role::ASSISTANT_NO];
    public function actionIndex(){
        $model = new Assistant();
        $data['fields'] = $model->getFields();
        $data['formFields'] = $model->getFormFields();
        $data['controller'] = $model->getAuthController();
        $data['roles'] = $model->getTeacherList();
        $pageData =  $model->getPage($this->condition);
        $data['items'] = $pageData['list'];
        $data['pages'] = $model->getPageInfo($this->condition);
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }

    public function actionPage(){
        $page = \Yii::$app->request->post('page',1);
        $params = \Yii::$app->request->post();
        $model = new Assistant();
        $data = $model->getPage($this->condition,$page,$params);
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }

    public function actionCreate(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $data["AssistantForm"] =$post_data;
            $adminForm = new AssistantForm();
            $adminForm->load($data);
            if($adminForm->validate()){
                $model = new Assistant();
                $model->role_id = Role::ASSISTANT_NO;
                $model->leader_id = $adminForm->leader_id;
                $model->username = $adminForm->username;
                $model->nickname = $adminForm->nickname;
                $model->status = $adminForm->status;
                $model->password = $adminForm->password ? \Yii::$app->security->generatePasswordHash($adminForm->password) : \Yii::$app->security->generatePasswordHash("123456");
                if($model->save()){
                    $this->in['status'] = true;
                    $pageData =  $model->getPage($this->condition);
                    $this->in['data'] = $pageData['list'];
                }
            }else{
                $this->in['status'] = false;
                $this->in['message'] = "验证失败";
            }
        }
        return $this->in;
    }

    public function actionUpdate(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $data["AssistantForm"] =$post_data;
            $adminForm = new AssistantForm();
            $adminForm->load($data);
            if($adminForm->validate()){
                $model = Assistant::findOne($post_data['id']);
                $model->leader_id = $adminForm->leader_id;
                $model->username = $adminForm->username;
                $model->nickname = $adminForm->nickname;
                $model->status = $adminForm->status;
                if($adminForm->password ){
                    $model->password = \Yii::$app->security->generatePasswordHash($adminForm->password);
                }
                if($model->save()){
                    $this->in['status'] = true;
                    $this->in['data'] = $model->toArray();
                }
            }else{
                $this->in['status'] = false;
                $this->in['message'] = "验证失败";
            }
        }
        return $this->in;
    }

    public function actionDelete(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $model = Assistant::findOne($post_data['id']);
            if($model){
                $model->is_deleted = ParentActiveRecord::IS_DELETED;
                if($model->save()){
                    $this->in['status'] = true;
                    $this->in['message'] = '';
                }else{
                    $this->in['message']= "操作失败！";
                }
            }else{
                $this->in['message']= "数据不存在！";
            }
        }
        return $this->in;
    }
}