<?php
namespace frontend\controllers;
use common\utils\RedisUtils;
use frontend\models\Admin;
use frontend\models\Order;
use frontend\models\Role;

class TutorIncomeController extends BaseController{
    public function actionIndex(){
        $start_time = \Yii::$app->request->post("start_time","");
        $end_time = \Yii::$app->request->post("end_time","");
        $data['fields'] = $this->getFields();
        $data['items'] = $this->getTutorIncome($start_time,$end_time);
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }

    public function getFields(){
        return [
            [
                "label"=>"ID",
                'field'=>"id",
                'type'=>"String",
                'search'=>0,
                'style'=>"width:80px;"
            ],
            [
                "label"=>"导师",
                'field'=>"name",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>"底薪",
                'field'=>"income",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>"提成",
                'field'=>"push_money",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>"总收入",
                'field'=>"total",
                'type'=>"String",
                'search'=>0
            ],
        ];
    }

    public function getTutorIncome($start_time,$end_time){
        $role_id = RedisUtils::getLogin($this->getAccessToken(),['role_id']);
        $key = ($start_time || $end_time) ? 'tutor_income_' : 'tutor_income';
        if($start_time){
            $key .= $end_time ? "".strtotime($start_time)."_" : "".strtotime($start_time);
        }
        if($end_time){
            $key .="".strtotime($end_time);
        }
        $admin_model = new Admin();
        $sale_list = $admin_model->getTeacherList(true);
        $query=Order::find()->select('belong_teacher_id,sum(push_money) as push_money')->where($admin_model->baseWhere())->andWhere(['service_status'=>Order::SERVICE_STATUS_5]);
        if($start_time){
            $stime = strtotime($start_time." 00:00:00");
            $query->andWhere(['>=','service_time',$stime]);
        }
        if($end_time){
            $etime = strtotime($end_time." 23:59:59");
            $query->andWhere(['<=','service_time',$etime]);
        }


        $uid = RedisUtils::getLogin($this->getAccessToken(),['uid']);
        if($role_id){
            switch ($role_id){
                case Role::TEACHER_NO;
                    $query->andWhere(['belong_teacher_id'=>$uid]);
                    break;
                case Role::ASSISTANT_NO:
                    $leader_id = RedisUtils::getLogin($this->getAccessToken(),['leader_id']);
                    if(!$leader_id){
                        $leader_id =  Admin::findOne($uid)->leader_id;
                    }
                    $query->andWhere(['belong_teacher_id'=>$leader_id]);
                    break;

            }
        }
        $order_models = $query->groupBy(['belong_teacher_id'])->asArray()->all();
        $base_income = Role::getBaseIncome(Role::TEACHER_NO);
        if($sale_list){
            foreach ($sale_list as &$item){
                $item['push_money'] = 0;
                $item['income'] = $base_income;
                $item['total'] =  $base_income;
                if($order_models) {
                    foreach ($order_models as $v) {
                        if ($v['belong_teacher_id'] == $item['id']) {
                            $item['push_money'] = $v['push_money'];
                            $item['total'] += $v['push_money'];
                            break;
                        }
                    }
                }
            }
        }
        if($sale_list){
            \Yii::$app->redis->set($key,json_encode($sale_list));
            RedisUtils::SetExpire($key);
        }
        return $sale_list;
    }
}