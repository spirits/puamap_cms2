<?php
namespace common\models;
use common\utils\RedisUtils;
use frontend\models\Node;
use yii\behaviors\TimestampBehavior;
use \yii\db\ActiveRecord;
use yii\web\Request;

class ParentActiveRecord extends ActiveRecord{
    //删除
    const IS_DELETED = 1;
    const NO_DELETED = 0;

    //状态
    const TRUE_STATUS = 0;
    const FALSE_STATUS = 1;
    public static  $status_maps = [
        self::TRUE_STATUS=>"启用",
        self::FALSE_STATUS=>'禁用'
    ];

    public $timeFormat='Y-m-d H:i:s';
    public $pageSize = "15";

    /**
     * @return array
     * 属性名称
     */
    public function attributeLabels()
    {
        return [
            'id'=>"ID",
            'create_time'=>'创建',
            'update_time'=>"更新",
            'is_deleted'=>'删除',
            'status'=>"状态",
            'operate_id'=>'操作者'
        ];
    }

    /**
     * @return mixed
     * 状态选项
     */
    public function getArray(){
        $arr['status'] = static::$status_maps;
        return $arr;
    }

    /**
     * @return array
     * 行为设置
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                [
                    'class'=>TimestampBehavior::className(),
                    'createdAtAttribute'=>"create_time",
                    'updatedAtAttribute'=>'update_time',
                    'value'=>time()
                ]
            ];
    }

    public function baseWhere(){
        return ["is_deleted"=>self::NO_DELETED];
    }

    public function getAttrLabel($attr){
        return $this->getAttributeLabel($attr).":";
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields['create_time'] =function ($model){
            return date($model->timeFormat,$model->create_time);
        };
        $fields['update_time'] =function ($model){
            return date($model->timeFormat,$model->update_time);
        };
        $fields['operate_id'] = function ($model){
            return RedisUtils::getNickname($model->operate_id);
        };
        return $fields;
    }

    public function getAccessToken(){
        $request = new Request();
        $params =$request->queryParams;
        return isset($params['access_token']) ? $params['access_token'] : "";
    }

    public function beforeSave($insert)
    {
            $request = new Request();
            $params =$request->queryParams;
            if(isset($params['access_token'])){
                $this->operate_id =  RedisUtils::getLogin($params['access_token'],['uid']);
            }else{
                $this->operate_id = $this->operate_id ? $this->operate_id : -1;
            }

        return parent::beforeSave($insert);
    }

    public function getPageInfo($condition=[]){
        $query = self::find()->where($this->baseWhere());
        if($condition){
            $query->andWhere($condition);
        }
        $totalCell = $query->count();
        return [
            'totalCell'=>$totalCell,
            'pageSize'=>$this->pageSize,
            'currentPage'=>"1",
            'showNum'=>"10"
        ];
    }

    /**
     * @param $data
     * 格式化数据
     */
    public function formatArray($data){
        $pay_status_maps = [];
        foreach ($data as $k=>$v){
            $temp = [
                'id'=>$k,
                'name'=>$v
            ];
            $pay_status_maps[] = $temp;
            unset($temp);
        }
        return $pay_status_maps;
    }

    public static function descAttr($oldAttr,$dirty){
        $attribute = [];
        foreach ($dirty as $k=>$v){
            if($oldAttr[$k] != $v){
                $attribute[$k]=$v;
            }

        }
        return $attribute;
    }

    /**
     * @return array
     * 操作权限
     */
    public function getAuthController(){
        $params =\Yii::$app->request->queryParams;
        $action = [];
        if($access_token = $params['access_token']) {
            $role_id = RedisUtils::getLogin($access_token,['role_id']);
            $auth = \Yii::$app->redis->get("role_".$role_id);
            if(!$auth){
                $auth = (Role::findOne($role_id)->auth);
                if($auth){
                    \Yii::$app->redis->set("role_".$role_id,$auth);
                }
            }
            $auth =  $auth ? explode(",",$auth) : [];
            $auth = $role_id == 1 ? "all": $auth;
            $controller = \Yii::$app->controller->id;
            $node_model = new Node();
            $actionsRoute  = $node_model->getActionListByRoute($controller);
            foreach ($actionsRoute as $i){
                if($auth== 'all' || in_array($i['id'],$auth)){
                    $action[] = str_replace($controller."/","",$i['route']);
                }
            }
        }
        return $action ? [
            'label'=>"操作",
            "action"=>$action,
        ] : [];
    }
}