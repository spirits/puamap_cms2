<?php
namespace frontend\controllers;
use common\models\ParentActiveRecord;
use common\utils\RedisUtils;
use frontend\forms\RoleForm;
use frontend\models\Node;
use frontend\models\Role;

class RoleController extends  BaseController{
    /**
     * @return array
     * 获取显示
     */
    public function actionIndex(){
        $model = new Role();
        $data['fields'] = $model->getFields();
        $data['formFields'] = $model->getFormFields();
        $data['controller'] = $model->getAuthController();
        $items = $model->getALL();
        $data['items'] = $this->formTreeData($items);
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }


    public function actionCreate(){
        if(\Yii::$app->request->isPost){
            $params = \Yii::$app->request->post();
            $data['parent_id']=$params['parent_id'];
            $data['name'] = $params['name'];
            $data['income'] =$params['income'];
            $data['income_status'] =$params['income_status'];
            $income_ratio = $params['income_ratio'];
            $data['big'] = $income_ratio['big'];
            $data['small'] = $income_ratio['small'];
            $data['seek'] = $income_ratio['seek'];
            $data['status'] = $params['status'];
            unset($params);
            $temp['RoleForm'] = $data;
            unset($data);
            $roleForm = new RoleForm();
            $roleForm->load($temp);
            unset($temp);
            if($roleForm->validate()){
                $model = new Role();
                $model->parent_id = $roleForm->parent_id;
                $model->name = $roleForm->name;
                $model->income = $roleForm->income;
                $model->status = $roleForm->status;
                $model->income_status = $roleForm->income_status;
                if($roleForm->income_status == 2 ){
                    $income_ratio = [
                        'big'=>$roleForm->big,
                        'small'=>$roleForm->small,
                        'seek'=>$roleForm->seek
                    ];
                    $model->income_ratio  = json_encode($income_ratio);
                }
                if($model->save()){
                    $this->in['status'] = true;
                    $this->in['message'] = '';
                    $this->in['data'] = $this->formTreeData($model->getALL());
                }else{
                    $this->in['message'] = "操作失败";
                }
            }else{
                $this->in['message'] = "操作失败";
            }
        }
        return $this->in;
    }

    public function actionUpdate(){
        if(\Yii::$app->request->isPost){
            $params = \Yii::$app->request->post();
            $data['parent_id']=$params['parent_id'];
            $data['name'] = $params['name'];
            $data['income'] =$params['income'];
            $data['income_status'] =$params['income_status'];
            $income_ratio = $params['income_ratio'];
            $data['big'] = $income_ratio['big'];
            $data['small'] = $income_ratio['small'];
            $data['seek'] = $income_ratio['seek'];
            $data['status'] = $params['status'];
            $temp['RoleForm'] = $data;
            unset($data);
            $roleForm = new RoleForm();
            $roleForm->load($temp);
            unset($temp);
            if($roleForm->validate()){
                $model = Role::findOne($params['id']);
                if($model){
                    $model->parent_id = $roleForm->parent_id;
                    $model->name = $roleForm->name;
                    $model->income = $roleForm->income;
                    $model->status = $roleForm->status;
                    $model->income_status = $roleForm->income_status;
                    if($roleForm->income_status == 2 ){
                        $income_ratio = [
                            'big'=>$roleForm->big,
                            'small'=>$roleForm->small,
                            'seek'=>$roleForm->seek
                        ];
                        $model->income_ratio  = json_encode($income_ratio);
                    }else{
                        $model->income_ratio="";
                    }
                    if($model->save()){
                        $this->in['status'] = true;
                        $this->in['message'] = '';
                        $tempData = $model->toArray();
                        if($tempData['income_status'] == 2){
                            $temp = @json_decode($tempData['income_ratio'],true);
                            if($temp){
                                $tempData['income_ratio'] = "【大课:".$temp['big'].",小课:".$temp['small'].",咨询:".$temp['seek']."】";
                                $tempData['income_ratio_origin'] = $temp;
                            }
                        }else{
                            $tempData['income_ratio'] = "-";
                        }
                        $this->in['data'] = $tempData;
                    }else{
                        $this->in['message'] = "操作失败";
                    }
                }else{
                    $this->in['message'] = "当前行,不存在!";
                }

            }else{
                $this->in['message'] = "验证失败";
            }
        }
        return $this->in;
    }

    public function actionDelete(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $model = Role::findOne($post_data['id']);
            if($model){
                $model->is_deleted = ParentActiveRecord::IS_DELETED;
                if($model->save()){
                    $this->in['status'] = true;
                    $this->in['message'] = '';
                }else{
                    $this->in['message']= "操作失败！";
                }
            }else{
                $this->in['message']= "数据不存在！";
            }
        }
        return $this->in;
    }

    /**
     * @param $items
     * @param int $parent_id
     * @param int $index
     * @return array
     * 格式数据
     */
    public function formTreeData($items,$parent_id=0,$index=0){
        $data= [];
        foreach ($items as $item) {
            if($item['parent_id'] == $parent_id){
                $item['name'] = str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;",$index).$item['name'];
                $item['create_time'] = $this->formatTime($item['create_time']);
                $item['update_time'] = $this->formatTime($item['update_time']);
                $item['operate_id'] = RedisUtils::getNickname($item['operate_id']);
                if($item['income_status'] == 2){
                    $temp = @json_decode($item['income_ratio'],true);
                    if($temp){
                        $item['income_ratio'] = "【大课:".$temp['big'].",小课:".$temp['small'].",咨询:".$temp['seek']."】";
                        $item['income_ratio_origin'] = $temp;
                    }
                }else{
                    $item['income_ratio'] = "-";
                }
                $data[] = $item;
                $num = $index + 1;
                $temp = $this->formTreeData($items,$item['id'],$num);
                if($temp){
                    foreach ($temp as $t){
                        $data[] = $t;
                    }
                }
            }
        }
        return $data;
    }


    public function actionAuth(){
        if(\Yii::$app->request->isPost){
            $make = \Yii::$app->request->post('make');
            $id = \Yii::$app->request->post('id');
            switch ($make){
                case  "get":
                    $model = new Node();
                    $fields = ['id','parent_id','name','route','status'];
                    $data = $model->getALL($fields);
                    $formatData = $model->formatTree($data);
                    $this->in['status'] = true;
                    $this->in['data'] = $formatData[0]['child'];
                    break;
                case "post":
                    $auth =  \Yii::$app->request->post('auth');
                    if($auth){
                        $role_model = Role::findOne($id);
                        $role_model->auth =  \Yii::$app->request->post('auth');
                        if($role_model->save()){
                            $this->in['status'] = true;
                            $this->in['data']=$role_model->toArray();
                        }else{
                            $this->in['message']="操作失败";
                        }
                    }else{
                        $this->in['message']="请分配权限";
                    }
            }
            return $this->in;
        }

    }



}