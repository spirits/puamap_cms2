<?php
namespace frontend\controllers;
use frontend\forms\SourceForm;
use frontend\models\Source;

class SourceController extends  BaseController{
    public function actionIndex(){
        $model = new Source();
        $data['fields'] = $model->getFields();
        $data['formFields'] = $model->getFormFields();
        $data['controller'] = $model->getAuthController();
        $pageData = $model->getPage();
        $data['items'] = $pageData['list'];
        $data['pages'] = $model->getPageInfo();
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }

    public function actionPage(){
        $page = \Yii::$app->request->post('page',1);
        $params = \Yii::$app->request->post();
        $model = new Source();
        $data = $model->getPage($page,$params);
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }

    public function actionCreate(){
        if(\Yii::$app->request->isPost){
            $params = \Yii::$app->request->post();
            $data['name'] = $params['name'];
            $data['status'] = $params['status'];
            unset($params);
            $temp['SourceForm'] = $data;
            unset($data);
            $sourceForm = new SourceForm();
            $sourceForm->load($temp);
            unset($temp);
            if($sourceForm->validate()){
                if($sourceForm->save()){
                    $this->in['status'] = true;
                    $pageData =  $sourceForm->getPage();
                    $this->in['data'] = $pageData['list'];
                }else{
                    $this->in['message'] = "操作失败";
                }
            }else{
                $this->in['message'] = "操作失败";
            }
        }
        return $this->in;
    }

    public function actionUpdate(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $data["SourceForm"] =$post_data;
            $paymentForm = SourceForm::findOne($post_data['id']);
            $paymentForm->load($data);
            if($paymentForm->validate()){
                if($paymentForm->save()){
                    $this->in['status'] = true;
                    $this->in['data'] = $paymentForm->toArray();
                }
            }else{
                $this->in['status'] = false;
                $this->in['message'] = "验证失败";
            }
        }
        return $this->in;
    }

    public function actionDelete(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $model = Source::findOne($post_data['id']);
            if($model){
                $model->is_deleted = Source::IS_DELETED;
                if($model->save()){
                    $this->in['status'] = true;
                    $this->in['message'] = '';
                }else{
                    $this->in['message']= "操作失败！";
                }
            }else{
                $this->in['message']= "数据不存在！";
            }
        }
        return $this->in;
    }
}