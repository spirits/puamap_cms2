<?php
use frontend\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="static/assets/css/font-awesome-ie7.min.css"/>
    <![endif]-->
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="assets/css/ace-ie.min.css"/>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="login-layout">
<?php $this->beginBody() ?>
<div class="logintop">
    <span>欢迎后台管理界面平台</span>
</div>
<div class="loginbody">
    <div class="login-container">
        <div class="center">
            <h1>
                <span class="white">后台管理系统</span>
            </h1>
            <h4 class="white">Background Management System</h4>
        </div>

        <div class="space-6"></div>

        <div class="position-relative">
            <div id="login-box" class="login-box widget-box no-border visible">
                <div class="widget-body">
                    <div class="widget-main">
                        <h4 class="header blue lighter bigger">
                            <i class="icon-coffee green"></i>
                            管理员登陆
                        </h4>

                        <div class="login_icon"><img src="/static/images/login.png"/></div>

                        <form>
                            <fieldset>
                                <label class="block clearfix">
									<span class="block input-icon input-icon-right">
										<input type="text" class="form-control username" placeholder="登录名" name="登录名"><i class="icon-user"></i>
									</span>
                                </label>
                                <label class="block clearfix">
									<span class="block input-icon input-icon-right">
										<input type="password" class="form-control password" placeholder="密码" name="密码"><i class="icon-lock"></i>
									</span>
                                </label>
                                <div class="space"></div>
                                <div class="clearfix">
                                    <label class="inline">
                                        <input type="checkbox" class="ace remember">
                                        <span class="lbl">保存密码</span>
                                    </label>
                                    <button type="button" class="width-35 pull-right btn btn-sm btn-primary"
                                            id="login_btn">
                                        <i class="icon-key"></i>
                                        登陆
                                    </button>
                                </div>

                                <div class="space-4"></div>
                            </fieldset>
                        </form>

                        <div class="social-or-login center">
                            <span class="bigger-110">通知</span>
                        </div>

                        <div class="social-login center">
                            本网站系统不再对IE8以下浏览器支持，请见谅。
                        </div>
                    </div><!-- /widget-main -->

                    <div class="toolbar clearfix">


                    </div>
                </div><!-- /widget-body -->
            </div><!-- /login-box -->
        </div><!-- /position-relative -->
    </div>
</div>
<div class="loginbm">版权所有 2016 <a href="">南京思美软件系统有限公司</a></div>
<strong></strong>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<script>
    $('#login_btn').on('click', function () {
        var username = $(".username").val();
        if (username == "") {
            layer.alert( $(".username").attr("name") + "不能为空！\r\n", {
                title: '提示框',
                icon: 0,
            });
            return false;
        }
        var password = $(".password").val();
        if (password == "") {
            layer.alert($(".password").attr("name") + "不能为空！\r\n", {
                title: '提示框',
                icon: 0,
            });
            return false;
        }
        var remember = $(".remember").is(':checked') ? 1 :0;
        $.ajax({
            url:"<?=\yii\helpers\Url::to(['login/sign-in'])?>",
            type:"POST",
            dataType:"JSON",
            data:{'LoginForm[username]':username,'LoginForm[password]':password,"LoginForm[remember]":remember,"<?=Yii::$app->request->csrfParam?>":"<?=Yii::$app->request->csrfToken?>"},
            success:function(result){
                if(result.status){
                    location.href ="/";
                }else{
                    layer.alert(result.message + "\r\n", {
                        title: '提示框',
                        icon: 0,
                    });
                    $(".username").val("");
                    $(".password").val("")
                }
            }
        })
    })
</script>
