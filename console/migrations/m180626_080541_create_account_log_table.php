<?php

use yii\db\Migration;

/**
 * Handles the creation of table `account_log`.
 */
class m180626_080541_create_account_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%account_log}}', [
            'id' => $this->primaryKey(),
            'order_no'=>$this->string(200)->notNull()->comment("单号"),
            'actions'=>$this->tinyInteger()->notNull()->comment("1表示打卡2表示大课结算"),
            'create_time'=>$this->integer(11)->comment("创建时间"),
            'update_time'=>$this->integer(11)->comment("更新时间"),
            'is_deleted'=>$this->smallInteger(1)->defaultValue(0)->comment("是否删除"),
            'status'=>$this->smallInteger(1)->defaultValue(0)->comment("是否结束"),
            'operate_id'=>$this->integer(11)->defaultValue(0)->comment("0表示系统结算")
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%account_log}}');
    }
}
