<?php
namespace frontend\forms;
use common\models\ParentActiveRecord;
use common\utils\RedisUtils;
use frontend\models\Admin;
use yii\base\Model;

class LoginForm extends Model{
    public $remember = 0;
    public $username="";
    public $password = "";
    private $_model = null;

    /**
     * @return array
     * 验证规则
     */
    public function rules()
    {
        return [
            [['username','password'],'required'],
            [['password'],'validatePwd'],
            [['remember'],'integer'],
            [['remember'],'default','value'=>0]
        ];
    }

    /**
     * @return null|static
     * 获取对象
     */
    public function getAdmin(){
        if(!$this->_model){
            $query = Admin::find();
            $this->_model = $query->where(['username'=>$this->username])->one();
        }
        return $this->_model;
    }

    /**
     * @param $attribute
     * @param $params
     * @return bool
     * 自定义验证
     */
    public function validatePwd($attribute, $params){

            $model = $this->getAdmin();
            if(!$model){
                $this->addError("username",\Yii::t("common",'admin_not_exist'));
                return false;
            }
            if($model->is_deleted == ParentActiveRecord::IS_DELETED){
                $this->addError("username",\Yii::t("common",'admin_is_deleted'));
                return false;
            }
            if($model->status == ParentActiveRecord::FALSE_STATUS){
                $this->addError("username",\Yii::t("common",'admin_false_status'));
                return false;
            }
            if(!$model->validatePassword($this->password)){
                $this->addError($attribute,\Yii::t('common','username_or_password_not_exist'));
                return false;
            }


        return true;
    }

    public function login(){
        if ($this->validate()) {
            $admin_model = $this->getAdmin();
            $admin_model->login_time = time();
            $access_token = \Yii::$app->security->generateRandomString();
            $admin_model->access_token = $access_token;
            if($admin_model->save()){
                $duration =  $this->remember ? 86400 : 3600;
                RedisUtils::setLogin($access_token,["nickname",$admin_model->nickname,'uid',$admin_model->id,'role_id',$admin_model->role_id,'leader_id',$admin_model->leader_id],$duration);
                return $access_token;
            }
        }
        return "";

    }
}