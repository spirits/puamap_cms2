<?php
namespace frontend\models;
class OrderDetail extends \common\models\OrderDetail{
    public function fields()
    {
        $fields = parent::fields();
        $fields['remark'] = function ($model){
            return $model->remark ? $model->remark : "";
        };
        $fields['images'] = function ($model){
            return $model->images ? explode(',',$model->images) : [];
        };
        $fields['bonus'] = function ($model){
            $order = new Order();
            $order_model = $order->getOrderInfo($model->order_no);
            return $order_model ? $order_model->bonus : 0;
        };
        return $fields;
    }

    public function getFields(){
        return [
            [
                "label"=>$this->getAttributeLabel("id"),
                'field'=>"id",
                'type'=>"String",
                'search'=>0,
                'style'=>"width:80px;"
            ],
            [
                "label"=>$this->getAttributeLabel("payment_id"),
                'field'=>"payment_id",
                'type'=>"ChosenSelect",
                'search'=>1,
            ],
            [
                "label"=>$this->getAttributeLabel("payment_type"),
                'field'=>"payment_type",
                'type'=>"ChosenSelect",
                'search'=>1,
            ],
            [
                "label"=>$this->getAttributeLabel("course_id"),
                'field'=>"course_id",
                'type'=>"ChosenSelect",
                'search'=>1,
                'style'=>"width:180px;display:inline-block;"
            ],
            [
                "label"=>$this->getAttributeLabel("belong_sale_group_id"),
                'field'=>"belong_sale_group_id",
                'type'=>"ChosenSelect",
                'search'=>1,
            ],

            [
                "label"=>$this->getAttributeLabel("belong_sale_id"),
                'field'=>"belong_sale_id",
                'type'=>"ChosenSelect",
                'search'=>1,
            ],
            [
                "label"=>$this->getAttributeLabel("sale_wechat_no"),
                'field'=>"sale_wechat_no",
                'type'=>"String",
                'search'=>0,
            ],
            [
                "label"=>$this->getAttributeLabel("sale_price"),
                'field'=>"sale_price",
                'type'=>"String",
                'search'=>0,
            ],
            [
                "label"=>$this->getAttributeLabel("order_detail_status"),
                'field'=>"order_detail_status",
                'type'=>"ChosenSelect",
                'search'=>1,
            ],
            [
                "label"=>$this->getAttributeLabel("images"),
                'field'=>"images",
                'type'=>"Image",
                'search'=>0,
            ],
            [
                "label"=>$this->getAttributeLabel("remark"),
                'field'=>"remark",
                'type'=>"String",
                'search'=>0,
            ],
            [
                "label"=>$this->getAttributeLabel("status"),
                'field'=>"status",
                'type'=>"ChosenSelect",
                'search'=>1
            ],
            [
                "label"=>$this->getAttributeLabel("create_time"),
                'field'=>"create_time",
                'type'=>"String",
                'search'=>0,
                'style'=>'width:200px;display:inline-block'
            ],
            [
                "label"=>$this->getAttributeLabel("operate_id"),
                'field'=>"operate_id",
                'type'=>"String"
            ]
        ];
    }

    public function getFormFields($order_no){

        $model = static::findOne(['order_no'=>$order_no,'position'=>1]);
        $order_model = (new Order())->getOrderInfo($order_no);
        return [
            [
                "label"=>$this->getAttrLabel('course_id'),
                'name'=>"course_id",
                'type'=>"ChosenSelect",
                'value'=>$model->course_id,
                "rules"=>['required'=>true]
            ],
            [
                "label"=>$this->getAttrLabel('payment_id'),
                'name'=>"payment_id",
                'type'=>"ChosenSelect",
                'value'=>0,
                "rules"=>['required'=>true]
            ],
            [
                "label"=>$this->getAttrLabel('payment_type'),
                'name'=>"payment_type",
                'type'=>"Switch",
                'value'=>Order::PAYMENT_TYPE_1,
                'options'=>[['label'=>"回款",'value'=>Order::PAYMENT_TYPE_1],
                    ['label'=>"尾款",'value'=>Order::PAYMENT_TYPE_2],['label'=>"退款",'value'=>Order::PAYMENT_TYPE_4]]
            ],
            [
                "label"=>$order_model->getAttrLabel('pay_end_time'),
                'name'=>"pay_end_time",
                'value'=>"",
                'type'=>"DateTime",
            ],
            [
                "label"=>$this->getAttrLabel('sale_price'),
                'name'=>"sale_price",
                'type'=>"String",
                'value'=>"",
                "rules"=>['required'=>true,"number"=>true]
            ],
            [
                "label"=>$this->getAttrLabel('belong_sale_id'),
                'name'=>"belong_sale_id",
                'type'=>"ChosenSelect",
                'value'=>$order_model->belong_sale_id,
                "rules"=>['required'=>true]
            ],
            [
                "label"=>$this->getAttrLabel('sale_wechat_no'),
                'name'=>"sale_wechat_no",
                'type'=>"String",
                'value'=>"",
                "rules"=>['required'=>true]
            ],
            [
                "label"=>$order_model->getAttrLabel('remark'),
                'name'=>"remark",
                'type'=>"String",
                'value'=>""
            ],
            [
                "label"=>$this->getAttrLabel('images'),
                'name'=>"images",
                'type'=>"Image",
                'value'=>[]
            ]

        ];
    }


    public function getFormFields2(){
    $order_model = new Order();
        return [

            [
                "label"=>$order_model->getAttrLabel('bonus'),
                'name'=>"bonus",
                'type'=>"String",
                'value'=>"",
                "rules"=>['required'=>true,"number"=>true]
            ],
            [
                "label"=>$this->getAttrLabel('order_detail_status'),
                'name'=>"order_detail_status",
                'type'=>"Switch",
                'value'=>OrderDetail::ORDER_DETAIL_STATUS_2,
                'options'=>[['label'=>"通过",'value'=>OrderDetail::ORDER_DETAIL_STATUS_2]]
            ]
        ];
    }

    /**
     * @return array
     * 列表
     */
    public function getTableChoseSelect(){
        $payment_model = new Payment();
        $course_model = new Course();
        $admin_model = new Admin();

        return [
            'payment_id'=>$payment_model->paymentArray(),
            'payment_type'=>$this->formatArray(Order::$payment_type_maps),
            'course_id'=>$course_model->courseArray(),
            'belong_sale_group_id'=>$admin_model->getSaleLeaderList(),
            'belong_sale_id'=>$admin_model->getSaleList(),
            "order_detail_status"=>$this->formatArray(static::$order_detail_status_maps),
            'status'=>[
                [
                    "id"=>0,
                    'name'=>"开启"
                ],
                [
                    "id"=>1,
                    'name'=>"禁用"
                ]
            ]

        ];
    }

    public function getFormChoseSelect($order_no){
        $model= static::find()->where(['order_no'=>$order_no,'position'=>1])->one();
        $order_model = (new Order())->getOrderInfo($order_no);
        $course_model = new Course();
        $courses = $course_model->courseArray();
        $courses_item = [];
        foreach ($courses as $v){
            if($v['id'] == $model->course_id){
                $courses_item[] =$v;
                break;
            }
        }
        $admin_model = new Admin();
        $saleLists = $admin_model->getSaleList();
        $saleLists_items = [];
        foreach ($saleLists as $v){
            if($v['id'] == $order_model->belong_sale_id){
                $saleLists_items[] = $v;
                break;
            }
        }
        $payment_model = new Payment();
        return [
            'course_id'=>$courses_item,
            'belong_sale_id'=>$saleLists_items,
            'payment_id'=>$payment_model->paymentArray(),
        ];

    }


    public function getPage($condition,$page = 1,$params=[]){
        $query = self::find()->where($this->baseWhere());
        if($condition){
            $query->andWhere($condition);
        }
        $query->orderBy(['id'=>SORT_ASC]);
        if($params){
            if(isset($params['payment_id']) && $params['payment_id'] > -1) {
                $query->andFilterWhere(['payment_id' => intval($params['payment_id'])]);
            }
            if(isset($params['status']) && $params['status'] > -1){
                $query->andFilterWhere(['status'=>intval($params['status'])]);
            }
            if(isset($params['payment_type']) && $params['payment_type'] && $params['payment_type'] > -1){
                $query->andFilterWhere(['payment_type'=>intval($params['payment_type'])]);
            }
            if(isset($params['course_id']) && $params['course_id'] && $params['course_id'] > -1){
                $query->andFilterWhere(['course_id'=>intval($params['course_id'])]);
            }
            if(isset($params['belong_sale_id']) && $params['belong_sale_id'] && $params['belong_sale_id'] > -1){
                $query->andFilterWhere(['belong_sale_id'=>intval($params['belong_sale_id'])]);
            }
            if(isset($params['belong_sale_group_id']) && $params['belong_sale_group_id'] && $params['belong_sale_group_id'] > -1){
                $query->andFilterWhere(['belong_sale_group_id'=>intval($params['belong_sale_group_id'])]);
            }
            if(isset($params['order_detail_status']) && $params['order_detail_status'] && $params['order_detail_status'] > -1){
                $query->andFilterWhere(['order_detail_status'=>intval($params['order_detail_status'])]);
            }

        }
        $offset = ($page - 1) * $this->pageSize;
        $totalCell = $query->count();
        $models= $query->offset($offset)->limit($this->pageSize)->all();
        $data = [];
        if($models){
            foreach ($models as $model) {
                $data[] = $model->toArray();

            }
        }
        return [
            'totalCell'=>$totalCell,
            'list'=>$data
        ];
    }

    public static function getPaidPriceByOrderNo($order_no){
        $models = static::find()->select(['sale_price','payment_type'])->where(['is_deleted'=>0,'order_no'=>$order_no])->all();
        $paid = 0;
        if($models){
            foreach ($models as $model){
                switch ($model->payment_type){
                    case Order::PAYMENT_TYPE_4;
                    $paid -= $model->sale_price;
                    break;
                    default :
                        $paid += $model->sale_price;
                        break;
                }
            }
        }
        return $paid;
    }

}