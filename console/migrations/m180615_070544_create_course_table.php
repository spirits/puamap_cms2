<?php

use yii\db\Migration;

/**
 * Handles the creation of table `course`.
 */
class m180615_070544_create_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%course}}', [
            'id' => $this->primaryKey(),
            'title'=>$this->string()->notNull()->comment('课程名称'),
            'belong_teacher_id'=>$this->integer(10)->notNull()->comment("所属导师"),
            'total_price'=>$this->integer(10)->notNull()->comment("总价"),
            'bonus'=>$this->smallInteger(5)->defaultValue(0)->comment("奖金"),
            'apply_persons'=>$this->integer(10)->defaultValue(0)->comment("报名人数"),
            'attend_persons'=>$this->integer(10)->defaultValue(0)->comment("上课人数"),
            'refund_persons'=>$this->integer(10)->defaultValue(0)->comment("退款人数"),
            'redirect_persons'=>$this->integer(10)->defaultValue(0)->comment("转课人数"),
            'duration'=>$this->integer(10)->defaultValue(0)->comment("时长"),
            'small_duration'=>$this->integer(10)->defaultValue(0)->comment("小课节数"),
            'create_time'=>$this->integer(11)->comment("创建时间"),
            'update_time'=>$this->integer(11)->comment("更新时间"),
            'is_deleted'=>$this->smallInteger(1)->defaultValue(0)->comment("是否删除"),
            'status'=>$this->smallInteger(1)->defaultValue(0)->comment("是否正常"),
            'operate_id'=>$this->integer(11)->defaultValue(1)->comment("操作者")
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%course}}');
    }
}
