<?php
namespace common\models;
use yii\web\Request;

/**
 * Class CourseService
 * @package common\models
 * @property int  $id
 * @property int $student_id
 * @property int $belong_teacher_id
 * @property string $order_no
 * @property int $big_process
 * @property int $small_process
 * @property int $service_status
 * @property string $remark
 * @property int $create_time
 * @property int $update_time
 * @property int $id_deleted
 * @property int $status
 * @property int $operate_id
 *
 */
class CourseService extends  ParentActiveRecord{
    public static function tableName()
    {
        return "{{%course_service}}";
    }

    public function attributeLabels()
    {
        return parent::attributeLabels() + [
                'student_id'=>'学员',
                "belong_teacher_id"=>'导师',
                'order_no'=>'订单',
                'big_process'=>'大课进度',
                'small_process'=>'小课进度',
                'service_status'=>'服务状态',
                'remark'=>'备注日志'
            ];

    }

    public function afterSave($insert, $changedAttributes)
    {
        $request = new Request();
        $params =$request->queryParams;
        if(isset($params['access_token'])){
            $model = new CourseServiceLog();
            $model->service_id = $this->id;
            $model->service_status = $this->service_status;
            switch ($model->service_status){
                case Order::SERVICE_STATUS_2:
                    $model->remark = "开始服务";
                    break;
                case Order::SERVICE_STATUS_3:
                    $model->remark = "暂停服务";
                    break;
                case Order::SERVICE_STATUS_4:
                    $model->remark = "继续服务";
                    break;
                case Order::SERVICE_STATUS_5:
                    $model->remark = "服务结束";
                    break;
            }
            $model->save();
        }

        parent::afterSave($insert, $changedAttributes);
    }
}