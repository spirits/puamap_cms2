<?php
namespace frontend\forms;
use frontend\models\Course;
use yii\base\Model;

class CourseForm extends Model{
    public $title;
    public $belong_teacher_id;
    public $total_price;
    public $duration;
    public $small_duration;
    public $status = 0;
    public $course_type = 1;
    public function rules()
    {
        return parent::rules() + [
                [['title','total_price','course_type'],'required'],
                [['title'],'string'],
                [['belong_teacher_id','duration','small_duration'],'checkValidateType'],
                [['belong_teacher_id','total_price','duration','small_duration','status'],'integer'],
                [['status'],'default','value'=>0]
            ];
    }

    public function checkValidateType($attr,$params){
        if($this->course_type == Course::COURSE_TYPE_1){
            if(!$this->$attr){
                $this->addError($attr,"不可空");
            }
        }
    }
}