<?php
namespace frontend\models;
class CourseServiceLog extends \common\models\CourseServiceLog{
    public function getFields(){
        return [
            [
                "label"=>$this->getAttributeLabel("id"),
                'field'=>"id",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("service_status"),
                'field'=>"service_status",
                'type'=>"ChosenSelect",
                'search'=>1
            ],
            [
                "label"=>$this->getAttributeLabel("remark"),
                'field'=>"remark",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("create_time"),
                'field'=>"create_time",
                'type'=>"String",
                'search'=>0,
                'style'=>'width:200px;display:inline-block'
            ],
            [
                "label"=>$this->getAttributeLabel("operate_id"),
                'field'=>"operate_id",
                'type'=>"String",
                'search'=>0,
            ]
        ];
    }

    public function getTableChoseSelect(){
        $service_maps = [];
        foreach (Order::$service_status_maps as $k=>$v){
            if($k != Order::SERVICE_STATUS_1){
                $service_maps[$k]=$v;
            }
        }
        return [
            'service_status'=>$this->formatArray($service_maps),
        ];
    }


    public function getPage($condition,$page = 1,$params=[]){
        $query = self::find()->where($this->baseWhere());
        if($condition){
            $query->andWhere($condition);
        }
        $query->orderBy(['id'=>SORT_ASC]);
        $offset = ($page - 1) * $this->pageSize;
        $totalCell = $query->count();
        $models= $query->offset($offset)->limit($this->pageSize)->all();
        $data = [];
        if($models){
            foreach ($models as $model) {
                $data[] = $model->toArray();
            }
        }
        return [
            'totalCell'=>$totalCell,
            'list'=>$data
        ];
    }
}