<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot/static';
    public $baseUrl = '@web/static';
    public $css = [
        'assets/css/bootstrap.min.css',
        'assets/css/font-awesome.min.css',
        'assets/css/ace.min.css',
        'assets/css/ace-rtl.min.css',
        'assets/css/ace-skins.min.css',
        'css/style.css',

    ];
 /*   public $js = [
        'assets/js/ace-extra.min.js',
        'js/jquery-1.9.1.min.js',
        'assets/layer/layer.js'
    ];*/
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
