<?php

use yii\db\Migration;

/**
 * Handles the creation of table `detail`.
 */
class m180619_022011_create_order_detail_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_detail}}', [
            'id' => $this->primaryKey(),
            'order_no'=>$this->string(200)->notNull()->comment("订单"),
            'payment_id'=>$this->smallInteger(5)->notNull()->comment("支付方式"),
            'payment_type'=>$this->smallInteger(5)->notNull()->comment("支付类型"),//1.定金 2.回款 3.尾款  4.全款  5.退款
            'course_id'=>$this->integer()->notNull()->comment("课程"),
            'belong_teacher_id'=>$this->integer(11)->notNull()->comment("所属导师"),
            'belong_sale_id'=>$this->integer(11)->notNull()->comment("所属销售"),
            'sale_wechat_no'=>$this->string(200)->defaultValue("")->comment("微信号"),
            'remark'=>$this->string()->defaultValue("")->comment("备注"),
            "belong_sale_group_id"=>$this->integer(11)->notNull()->comment("销售组"),
            'sale_price'=>$this->decimal(10,2)->defaultValue(0)->comment("当前收款金额"),
            'student_id'=>$this->integer(11)->notNull()->comment("用户id"),
            'position'=>$this->smallInteger(1)->defaultValue(0)->comment("位置"),
            "images"=>$this->string(1000)->defaultValue("")->comment("图片"),
            'order_detail_status'=>$this->smallInteger()->defaultValue(0)->comment("订单状态"),//1未审核 2.财务审核 3已审核
            'create_time'=>$this->integer(11)->comment("创建时间"),
            'update_time'=>$this->integer(11)->comment("更新时间"),
            'is_deleted'=>$this->smallInteger(1)->defaultValue(0)->comment("是否删除"),
            'status'=>$this->smallInteger(1)->defaultValue(0)->comment("是否正常"),
            'operate_id'=>$this->integer(11)->defaultValue(1)->comment("操作者"),
        ]);
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_detail}}');
    }
}
