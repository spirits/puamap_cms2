<?php
namespace frontend\controllers;
use common\models\ParentActiveRecord;
use common\utils\Qiniu;
use common\utils\RedisUtils;
use frontend\forms\OrderForm;
use frontend\forms\OrderRedirectForm;
use frontend\models\Course;
use frontend\models\CourseService;
use frontend\models\Order;
use frontend\models\OrderDetail;
use frontend\models\OrderLog;
use frontend\models\Role;
use frontend\models\Student;
use yii\db\Exception;

class OrderController extends BaseController{
    public function actionIndex(){
        $model = new Order();
        $data['fields'] = $model->getFields();
        $data['formFields'] = $model->getFormFields();
        $data['controller'] = $model->getAuthController();
        $data['tableChose'] = $model->getTableChoseSelect();
        $data['formChose']=$model->getFormChoseSelect();
        $pageData = $model->getPage();
        $data['items'] = $pageData['list'];
        $data['pages'] = $model->getPageInfo();
        $data['pages']['totalCell']= $pageData['totalCell'];
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }


    public function actionPage(){
        $page = \Yii::$app->request->post('page',1);
        $params = \Yii::$app->request->post();
        $model = new Order();
        $data = $model->getPage($page,$params);
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }



    /**
     * @return array
     * 创建
     */
    public function actionCreate(){
        if(\Yii::$app->request->isPost){
            $data['OrderForm'] = \Yii::$app->request->post();
            $orderForm = new OrderForm();
            $orderForm->load($data);
            if($orderForm->validate()){
                $transaction = \Yii::$app->db->beginTransaction();
                try{
                    //=====订单数据
                    $order_model = new Order();
                    $order_model->title = $orderForm->title;
                    $order_model->order_status = Order::ORDER_STATUS_3;
                    $order_model->source_id = $orderForm->source_id;
                    $order_model->order_no = $this->getRandom().date('YmdHis');
                    $order_model->course_id= $orderForm->course_id;
                    $course_info = RedisUtils::getCourseInfo($orderForm->course_id);
                    if(!$course_info){
                        throw  new  Exception("课程不存在");
                    }
                    $order_model->course_title = $course_info['title'];
                    $order_model->total_price = $course_info['total_price'];
                    $order_model->coupon = $orderForm->coupon;
                    $order_model->paid_price = $orderForm->sale_price;
                    $order_model->course_type =  $course_info['course_type'];
                    if($course_info['course_type'] == Course::COURSE_TYPE_1){
                        $big_small_seek_price = $this->ratio($course_info['total_price'] - $orderForm->coupon);
                        $order_model->big_price = $big_small_seek_price['big_price'];
                        $order_model->small_price = $big_small_seek_price['small_price'];
                        $order_model->seek_price = $big_small_seek_price['seek_price'];
                        $order_model->duration = $course_info['duration'];
                        $order_model->small_duration = $course_info['small_duration'];
                        $teacher_info =RedisUtils::getTeacherAndSaleInfo($course_info['belong_teacher_id']);
                        if(!$teacher_info){
                            throw  new  Exception("导师不存在");
                        }
                        $order_model->belong_teacher_id = $course_info['belong_teacher_id'];
                        $order_model->belong_teacher_ratio =Role::getTeacherRatio();
                    }
                    $order_model->service_status = Order::SERVICE_STATUS_1;
                    switch ($orderForm->payment_type){
                        case Order::PAYMENT_TYPE_0://定金
                            $order_model->pay_status = Order::PAY_STATUS_1;
                            break;
                        case Order::PAYMENT_TYPE_3://全款
                            $order_model->pay_status = Order::PAY_STATUS_2;
                            $order_model->pay_end_time = $orderForm->pay_end_time ? (@strtotime($orderForm->pay_end_time) ? strtotime($orderForm->pay_end_time) : time()) : time();
                            break;
                    }
                    $order_model->remark = $orderForm->remark;
                    $order_model->belong_sale_id = $orderForm->belong_sale_id;
                    $sale_info = RedisUtils::getTeacherAndSaleInfo($orderForm->belong_sale_id);
                    if(!$sale_info){
                        throw  new  Exception("销售不存在");
                    }
                    $order_model->belong_sale_group_id = $sale_info['leader_id'];

                    //=====用户数据
                    $student_model = new Student();
                    $student_model->wechat_no = $orderForm->wechat_no;
                    $student_model->mobile=$orderForm->mobile;
                    $student_model->wechat_name = $orderForm->wechat_name;
                    $student_model->income = $orderForm->income;
                    $student_model->age =$orderForm->age;
                    $student_model->major = $orderForm->major;
                    $student_model->address = $orderForm->address;
                    $student_model->add_time = $orderForm->add_time ? strtotime($orderForm->add_time) : 0;
                    $student_model->inside  = $orderForm->inside;
                    $student_model->compact = $orderForm->compact;
                    $student_model->mobile = $orderForm->mobile;
                    if($student_model->save()){
                        $order_model->student_id = $student_model->id;
                        //详情内容
                        $order_detail_model = new OrderDetail();
                        $order_detail_model->order_no = $order_model->order_no;
                        $order_detail_model->payment_id = $orderForm->payment_id;
                        $order_detail_model->payment_type =$orderForm->payment_type;
                        $order_detail_model->course_id = $orderForm->course_id;
                        $order_detail_model->belong_teacher_id = $order_model->belong_teacher_id;
                        $order_detail_model->belong_sale_id = $order_model->belong_sale_id;
                        $order_detail_model->sale_wechat_no = $orderForm->sale_wechat_no;
                        $order_detail_model->belong_sale_group_id = $order_model->belong_sale_group_id;
                        $order_detail_model->sale_price = $orderForm->sale_price;
                        $order_detail_model->student_id = $student_model->id;
                        $order_detail_model->position = 1;
                        $order_detail_model->remark = $order_model->remark;
                        $order_detail_model->images = $orderForm->images ? implode(",",$orderForm->images) : "";
                        $order_detail_model->order_detail_status = OrderDetail::ORDER_DETAIL_STATUS_1;
                        if($order_detail_model->save() && $order_model->save()){
                            $order_log_model = new OrderLog();
                            $order_log_model->order_no = $order_model->order_no;
                            $order_log_model->course_id = $order_model->course_id;
                            $order_log_model->source_id = $order_model->source_id;
                            $order_log_model->belong_teacher_id = $order_model->belong_teacher_id;
                            $order_log_model->belong_sale_id = $order_model->belong_sale_id;
                            $order_log_model->belong_sale_group_id = $order_model->belong_sale_group_id;
                            $order_log_model->payment_id = $order_detail_model->payment_id;
                            $order_log_model->payment_type = $order_detail_model->payment_type;
                            $order_log_model->total_price = $order_model->total_price;
                            $order_log_model->paid_price = $order_model->paid_price;
                            $order_log_model->sale_price = $order_detail_model->sale_price;
                            $order_log_model->coupon = $order_model->coupon;
                            $order_log_model->big_price = $order_model->big_price;
                            $order_log_model->small_price = $order_model->small_price;
                            $order_log_model->seek_price = $order_model->seek_price;
                            $order_log_model->bonus = $order_model->bonus;
                            $order_log_model->push_money = $order_model->push_money;
                            $order_log_model->order_status = $order_model->order_status;
                            $order_log_model->service_status = $order_model->service_status;
                            $order_log_model->service_time = $order_model->service_time;
                            $order_log_model->pay_status = $order_model->pay_status;
                            $order_log_model->pay_end_time = $order_model->pay_end_time;
                            $order_log_model->sale_wechat_no = $order_detail_model->sale_wechat_no;
                            $order_log_model->order_detail_status = $order_detail_model->order_detail_status;
                            $order_log_model->wechat_name = $student_model->wechat_name;
                            $order_log_model->wechat_no = $student_model->wechat_no;
                            $order_log_model->income = $student_model->income;
                            $order_log_model->age = $student_model->age;
                            $order_log_model->major = $student_model->major;
                            $order_log_model->address = $student_model->address;
                            $order_log_model->add_time = $student_model->add_time;
                            $order_log_model->inside = $student_model->inside;
                            $order_log_model->compact = $student_model->compact;
                            $order_log_model->mobile = $student_model->mobile;
                            $order_log_model->remark = $order_model->remark;
                            $order_log_model->images = $order_detail_model->images;
                            $order_log_model->course_type = $order_model->course_type;
                            $order_log_model->actions = OrderLog::CREATE;
                            $course_model = Course::findOne($course_info['id']);
                            $course_model->apply_persons+=1;
                            if($order_log_model->save() && $course_model->save()){
                                $this->in['status'] = true;
                                $pageData =  $order_model->getPage();
                                $this->in['data'] = $pageData['list'];
                                $this->in['order_no'] = $order_model->order_no;
                            }else{
                                throw  new  Exception("日志错误");
                            }
                        }else{
                            throw  new  Exception("数据保存错误");
                        }
                    }else{
                        throw  new  Exception("用户保存错误");
                    }
                    $transaction->commit();
                }catch (Exception $e){
                    $this->in['status'] = false;
                    $this->in['message'] = $e->getMessage();
                    $transaction->rollBack();
                }
            }else{
                $this->in['message'] = $orderForm->getErrors();
            }
        }
        return $this->in;
    }

    public function actionUpdate(){
        if(\Yii::$app->request->isPost){
            $data['OrderForm'] = \Yii::$app->request->post();
            $orderForm = new OrderForm();
            $orderForm->load($data);
            if($orderForm->validate()){
                $transaction = \Yii::$app->db->beginTransaction();
                try{
                    //=====订单数据
                    $order_model = Order::findOne($data['OrderForm']['id']);
                    $order_model->title = $orderForm->title;
                    $order_model->source_id = $orderForm->source_id;
                    $order_model->course_id= $orderForm->course_id;
                    $course_info = RedisUtils::getCourseInfo($orderForm->course_id);
                    if(!$course_info){
                        throw  new  Exception("课程不存在");
                    }
                    $order_model->course_title = $course_info['title'];
                    $order_model->total_price = $course_info['total_price'];
                    $order_model->coupon = $orderForm->coupon;
                    $order_model->paid_price = $orderForm->sale_price;
                    $order_model->course_type =  $course_info['course_type'];
                    if($course_info['course_type'] == Course::COURSE_TYPE_1){
                        $big_small_seek_price = $this->ratio($course_info['total_price'] - $orderForm->coupon);
                        $order_model->big_price = $big_small_seek_price['big_price'];
                        $order_model->small_price = $big_small_seek_price['small_price'];
                        $order_model->seek_price = $big_small_seek_price['seek_price'];
                        $order_model->duration = $course_info['duration'];
                        $order_model->small_duration = $course_info['small_duration'];
                        $teacher_info =RedisUtils::getTeacherAndSaleInfo($course_info['belong_teacher_id']);
                        if(!$teacher_info){
                            throw  new  Exception("导师不存在");
                        }
                        $order_model->belong_teacher_id = $course_info['belong_teacher_id'];
                        $order_model->belong_teacher_ratio =Role::getTeacherRatio();
                    }
                    switch ($orderForm->payment_type){
                        case Order::PAYMENT_TYPE_0://定金
                            $order_model->pay_status = Order::PAY_STATUS_1;
                            $order_model->pay_end_time =0;
                            break;
                        case Order::PAYMENT_TYPE_3://全款
                            $order_model->pay_status = Order::PAY_STATUS_2;
                            $order_model->pay_end_time = $orderForm->pay_end_time ? (@strtotime($orderForm->pay_end_time) ? strtotime($orderForm->pay_end_time) : time()) : time();
                            break;
                    }
                    $order_model->remark = $orderForm->remark;
                    $order_model->belong_sale_id = $orderForm->belong_sale_id;
                    $sale_info = RedisUtils::getTeacherAndSaleInfo($orderForm->belong_sale_id);
                    if(!$sale_info){
                        throw  new  Exception("销售不存在");
                    }
                    $order_model->belong_sale_group_id = $sale_info['leader_id'];
                    //=====用户数据
                    $student_model = Student::findOne($order_model->student_id);
                    $student_model->wechat_no = $orderForm->wechat_no;
                    $student_model->mobile=$orderForm->mobile;
                    $student_model->wechat_name = $orderForm->wechat_name;
                    $student_model->income = $orderForm->income;
                    $student_model->age =$orderForm->age;
                    $student_model->major = $orderForm->major;
                    $student_model->address = $orderForm->address;
                    $student_model->add_time = $orderForm->add_time ? strtotime($orderForm->add_time) : 0;
                    $student_model->inside  = $orderForm->inside;
                    $student_model->compact = $orderForm->compact;
                    $student_model->mobile = $orderForm->mobile;
                    $updateStudentAttr =ParentActiveRecord::descAttr($student_model->getOldAttributes(),$student_model->getDirtyAttributes());
                    if($student_model->save()){
                        //详情内容
                        $order_detail_model = OrderDetail::find()->where(['order_no'=>$order_model->order_no,'position'=>1])->one();
                        $order_detail_model->payment_id = $orderForm->payment_id;
                        $order_detail_model->payment_type =$orderForm->payment_type;
                        $order_detail_model->course_id = $orderForm->course_id;
                        $order_detail_model->belong_teacher_id = $order_model->belong_teacher_id;
                        $order_detail_model->belong_sale_id = $order_model->belong_sale_id;
                        $order_detail_model->sale_wechat_no = $orderForm->sale_wechat_no;
                        $order_detail_model->belong_sale_group_id = $order_model->belong_sale_group_id;
                        $order_detail_model->sale_price = $orderForm->sale_price;
                        $order_detail_model->remark = $order_model->remark;
                        //$order_detail_model->order_detail_status = OrderDetail::ORDER_DETAIL_STATUS_1;
                        $order_detail_model->images = $orderForm->images ? implode(",",$orderForm->images) : "";

                        $updateOrderDetailAttr = ParentActiveRecord::descAttr($order_detail_model->getOldAttributes(),$order_detail_model->getDirtyAttributes());
                        if($order_detail_model->save()){
                            $order_model->paid_price = OrderDetail::getPaidPriceByOrderNo($order_model->order_no);
                            $updateOrderAttr = ParentActiveRecord::descAttr($order_model->getOldAttributes(),$order_model->getDirtyAttributes());
                            if(!$order_model->save()){
                                throw  new  Exception("更新错误");
                            }

                            if(count($updateStudentAttr) > 0 || count($updateOrderAttr) > 0 || count($updateOrderDetailAttr) > 0){
                                $order_log_model = new OrderLog();
                                $order_log_model->order_no = $order_model->order_no;
                                $orderAttr = ["course_id","source_id","belong_teacher_id","belong_sale_id","belong_sale_group_id","total_price",'paid_price','coupon',
                                    'big_price',"small_price","seek_price","bonus","push_money","order_status","service_status","service_time","pay_status","pay_end_time",'remark','course_type'
                                ];
                                foreach ($orderAttr as $attr){
                                    $order_log_model->$attr =  isset($updateOrderAttr[$attr]) ? $updateOrderAttr[$attr] : "-";
                                }

                                $orderDetailAttr = ["payment_id","payment_type","sale_wechat_no",'order_detail_status','sale_price','images'];
                                foreach ($orderDetailAttr as $attr){
                                    $order_log_model->$attr =  isset($updateOrderDetailAttr[$attr]) ? $updateOrderDetailAttr[$attr] : "-";
                                }

                                $studentAttr = ["wechat_name","wechat_no","income","age","major","address","add_time","inside","compact",'mobile'];
                                foreach ($studentAttr as $attr){
                                    $order_log_model->$attr =  isset($updateStudentAttr[$attr]) ? $updateStudentAttr[$attr] : "-";
                                }
                                $order_log_model->actions = OrderLog::UPDATE;
                                if($order_log_model->save()){
                                    $this->in['status'] = true;
                                    $this->in['data'] = $order_model->getInfo($order_model->id);
                                }else{
                                    throw  new  Exception("日志错误");
                                }
                            }else{
                                $this->in['status'] = true;
                                $this->in['data'] = $order_model->getInfo($order_model->id);
                            }
                        }else{
                            throw  new  Exception("数据修改错误");
                        }
                    }else{
                        throw  new  Exception("用户修改错误");
                    }
                    $transaction->commit();
                }catch (Exception $e){
                    $this->in['status'] = false;
                    $this->in['message'] = $e->getMessage();
                    $transaction->rollBack();
                }

            }else{
                $this->in['message'] = $orderForm->getErrors();//"数据格式错误，或者订单已经被添加";
            }
        }
        return $this->in;
    }



    public function getRandom($len = 5){
        $str ="QWERTYUIOP0987654321AZSXDCFVGBHNJMKL";
        $start = rand(0,31);
        return substr(str_shuffle($str),$start,$len);
    }

    public function ratio($total){
        $model= new Course();
        $ratio = $model->ratio;
        return [
            'big_price'=> round($total * $ratio['big'] / 10,2),
            'small_price'=> round($total * $ratio['small'] / 10,2),
            'seek_price'=> round($total * $ratio['seek'] / 10,2)
        ];
    }

    public function actionDelete(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $model = Order::findOne($post_data['id']);
            if($model){
                if($model->service_status == Order::SERVICE_STATUS_1){
                    $model->is_deleted = Order::IS_DELETED;
                    $order_log_model= new OrderLog();
                    $order_log_model->order_no = $model->order_no;
                    $order_log_model->actions=OrderLog::DELETE;
                    $order_log_model->remark ="删除源订单";
                    $course_model = Course::findOne($model->course_id);
                    $course_model->apply_persons -= 1;

                    if($model->save() && $course_model->save() && $order_log_model->save()){
                        $student_model = Student::findOne($model->student_id);
                        RedisUtils::deleteOrderCheck($model->course_id,$student_model->wechat_no,$student_model->mobile);
                        $this->in['status'] = true;
                        $this->in['message'] = '';
                    }else{
                        $this->in['message']= "操作失败！";
                    }
                }else{
                    $this->in['message']= "服务中不可删除";
                }
            }else{
                $this->in['message']= "数据不存在！";
            }
        }
        return $this->in;
    }

    /**
     * 转销售
     */
    public function actionReturn(){
        $post_data = \Yii::$app->request->post();
        $model = Order::findOne($post_data['id']);
        if(\Yii::$app->request->isPost){
            $sale_info = RedisUtils::getTeacherAndSaleInfo($post_data['belong_sale_id']);
            if($sale_info){
                if($post_data['belong_sale_id'] == $model->belong_sale_id &&  $sale_info['leader_id'] == $model->belong_sale_group_id){
                    $this->in['message']= "不能自己转给自己";
                }else{
                    $transaction = \Yii::$app->db->beginTransaction();
                    try{
                        if(in_array($model->order_status,Order::$return_maps)){
                            $new_order_model = new Order();
                            $order_log_model = new OrderLog();
                            $order_log_model->order_no = $model->order_no;
                            $order_log_model->actions = OrderLog::RESALE;
                            $attributes = $model->getAttributes(null,['create_time','update_time','status','operate_id','is_deleted']);
                            foreach ($attributes as $attr=>$v){
                                if($attr != 'id'){
                                    $new_order_model->$attr = $v;
                                }
                            }
                            $new_order_model->belong_sale_id = $post_data['belong_sale_id'];
                            $new_order_model->belong_sale_group_id = $sale_info['leader_id'];
                            $new_order_model->order_status = Order::ORDER_STATUS_6;
                            $model->order_status = Order::ORDER_STATUS_5;
                            $order_log_model->belong_sale_id = $new_order_model->belong_sale_id;
                            $order_log_model->belong_sale_group_id = $new_order_model->belong_sale_group_id;
                            $order_log_model->order_status = Order::ORDER_STATUS_6;
                            if($new_order_model->save() && $model->save() &&  $order_log_model->save()){
                                $this->in['status'] = true;
                                $pageData =  $new_order_model->getPage();
                                $this->in['data'] = $pageData['list'];
                                $student_model = Student::findOne($model->student_id);
                                RedisUtils::deleteOrderCheck($model->course_id,$student_model->wechat_no,$student_model->mobile);
                            }else{
                                throw  new Exception("操作失败");
                            }
                        }else{
                            throw  new Exception("不满足转课条件");
                        }
                    }catch (Exception $e){
                        $this->in['message']= $e->getMessage();
                        $this->in['status'] = false;
                        $transaction->rollBack();
                    }

                }
            }else{
                $this->in['message']= "该销售不存在！";
            }

        }
        return $this->in;
    }

    /**
     * @return array
     * 转课
     */
    public function actionRedirect(){
        if(\Yii::$app->request->isPost){
            $data = \Yii::$app->request->post();
            $old_order_model = Order::findOne($data['id']);
            if($old_order_model){
                if($old_order_model->course_id == $data['course_id']){
                    $this->in['message']="课程没有改变";
                }else{
                    $student_model = Student::findOne($old_order_model->student_id);
                    $data['OrderRedirectForm'] = $data;
                    $data['OrderRedirectForm']['wechat_no'] = $student_model->wechat_no;
                    $data['OrderRedirectForm']['mobile'] = $student_model->mobile;
                    $orderForm = new OrderRedirectForm();
                    $orderForm->load($data);
                    if($orderForm->validate()){
                        $transaction = \Yii::$app->db->beginTransaction();
                        try{
                            //=====订单数据
                            $order_model = new Order();
                            $order_model->title = $orderForm->title;
                            $order_model->order_status = Order::ORDER_STATUS_8;
                            $order_model->source_id = $orderForm->source_id;
                            $order_model->order_no = $this->getRandom().date('YmdHis');
                            $order_model->course_id= $orderForm->course_id;
                            $course_info = RedisUtils::getCourseInfo($orderForm->course_id);
                            if(!$course_info){
                                throw  new  Exception("课程不存在");
                            }
                            $order_model->course_title = $course_info['title'];
                            $order_model->total_price = $course_info['total_price'];
                            $order_model->coupon = $orderForm->coupon;
                            $big_small_seek_price = $this->ratio($course_info['total_price'] - $orderForm->coupon);
                            $order_model->big_price = $big_small_seek_price['big_price'];
                            $order_model->small_price = $big_small_seek_price['small_price'];
                            $order_model->seek_price = $big_small_seek_price['seek_price'];
                            $order_model->paid_price = $orderForm->sale_price;
                            $order_model->service_status = Order::SERVICE_STATUS_1;
                            $order_model->duration = $course_info['duration'];
                            $order_model->small_duration = $course_info['small_duration'];
                            switch ($orderForm->payment_type){
                                case Order::PAYMENT_TYPE_0://定金
                                    $order_model->pay_status = Order::PAY_STATUS_1;
                                    break;
                                case Order::PAYMENT_TYPE_3://全款
                                    $order_model->pay_status = Order::PAY_STATUS_2;
                                    $order_model->pay_end_time = $orderForm->pay_end_time ? (@strtotime($orderForm->pay_end_time) ? strtotime($orderForm->pay_end_time) : time()) : time();
                                    break;
                            }
                            $order_model->remark = $orderForm->remark;
                            $teacher_info =RedisUtils::getTeacherAndSaleInfo($course_info['belong_teacher_id']);
                            if(!$teacher_info){
                                throw  new  Exception("导师不存在");
                            }
                            $order_model->belong_teacher_id = $course_info['belong_teacher_id'];
                            $order_model->belong_teacher_ratio =Role::getTeacherRatio();
                            $order_model->belong_sale_id = $orderForm->belong_sale_id;
                            $sale_info = RedisUtils::getTeacherAndSaleInfo($orderForm->belong_sale_id);
                            if(!$sale_info){
                                throw  new  Exception("销售不存在");
                            }
                            $order_model->belong_sale_group_id = $sale_info['leader_id'];
                            $order_model->student_id = $student_model->id;
                                //详情内容
                                $order_detail_model = new OrderDetail();
                                $order_detail_model->order_no = $order_model->order_no;
                                $order_detail_model->payment_id = $orderForm->payment_id;
                                $order_detail_model->payment_type =$orderForm->payment_type;
                                $order_detail_model->course_id = $orderForm->course_id;
                                $order_detail_model->belong_teacher_id = $order_model->belong_teacher_id;
                                $order_detail_model->belong_sale_id = $order_model->belong_sale_id;
                                $order_detail_model->sale_wechat_no = $orderForm->sale_wechat_no;
                                $order_detail_model->belong_sale_group_id = $order_model->belong_sale_group_id;
                                $order_detail_model->sale_price = $orderForm->sale_price;
                                $order_detail_model->student_id = $student_model->id;
                                $order_detail_model->position = 1;
                                $order_detail_model->remark = $order_model->remark;
                                $order_detail_model->images = $orderForm->images ? implode(",",$orderForm->images) : "";
                                $order_detail_model->order_detail_status = OrderDetail::ORDER_DETAIL_STATUS_1;
                                if($order_detail_model->save() && $order_model->save()){
                                    $order_log_model = new OrderLog();
                                    $order_log_model->order_no = $order_model->order_no;
                                    $order_log_model->course_id = $order_model->course_id;
                                    $order_log_model->source_id = $order_model->source_id;
                                    $order_log_model->belong_teacher_id = $order_model->belong_teacher_id;
                                    $order_log_model->belong_sale_id = $order_model->belong_sale_id;
                                    $order_log_model->belong_sale_group_id = $order_model->belong_sale_group_id;
                                    $order_log_model->payment_id = $order_detail_model->payment_id;
                                    $order_log_model->payment_type = $order_detail_model->payment_type;
                                    $order_log_model->total_price = $order_model->total_price;
                                    $order_log_model->paid_price = $order_model->paid_price;
                                    $order_log_model->sale_price = $order_detail_model->sale_price;
                                    $order_log_model->coupon = $order_model->coupon;
                                    $order_log_model->big_price = $order_model->big_price;
                                    $order_log_model->small_price = $order_model->small_price;
                                    $order_log_model->seek_price = $order_model->seek_price;
                                    $order_log_model->bonus = $order_model->bonus;
                                    $order_log_model->push_money = $order_model->push_money;
                                    $order_log_model->order_status = $order_model->order_status;
                                    $order_log_model->service_status = $order_model->service_status;
                                    $order_log_model->service_time = $order_model->service_time;
                                    $order_log_model->pay_status = $order_model->pay_status;
                                    $order_log_model->pay_end_time = $order_model->pay_end_time;
                                    $order_log_model->sale_wechat_no = $order_detail_model->sale_wechat_no;
                                    $order_log_model->order_detail_status = $order_detail_model->order_detail_status;
                                    $order_log_model->wechat_name = $student_model->wechat_name;
                                    $order_log_model->wechat_no = $student_model->wechat_no;
                                    $order_log_model->income = $student_model->income;
                                    $order_log_model->age = $student_model->age;
                                    $order_log_model->major = $student_model->major;
                                    $order_log_model->address = $student_model->address;
                                    $order_log_model->add_time = $student_model->add_time;
                                    $order_log_model->inside = $student_model->inside;
                                    $order_log_model->compact = $student_model->compact;
                                    $order_log_model->mobile = $student_model->mobile;
                                    $order_log_model->remark = $order_model->remark;
                                    $order_log_model->images = $order_detail_model->images;
                                    $order_log_model->actions = OrderLog::CREATE;
                                    $course_model = Course::findOne($course_info['id']);
                                    $course_model->apply_persons+=1;
                                    if($order_log_model->save() && $course_model->save()){
                                        $old_course_model = Course::findOne($old_order_model->course_id);
                                        $old_course_model->apply_persons -=1;
                                        $old_order_model->order_status = Order::ORDER_STATUS_7;
                                        $old_order_model->child_no = $order_model->order_no;
                                        $old_order_log_model = new OrderLog();
                                        $old_order_log_model->actions= OrderLog::RECOURSE;
                                        $old_order_log_model->order_no = $old_order_model->order_no;
                                        $old_order_log_model->child_no = $old_order_model->child_no;
                                        if($old_order_model->service_status != Order::SERVICE_STATUS_5){
                                            $old_order_model->service_status = Order::SERVICE_STATUS_5;
                                            $old_order_log_model->service_status = Order::SERVICE_STATUS_5;
                                            $old_order_model->service_time = time();
                                            $old_order_log_model->service_time = time();
                                        }
                                        if($old_order_model->save() && $old_course_model->save() && $old_order_log_model->save()){
                                            $course_service_model =  CourseService::findOne(['order_no'=>$old_order_model->order_no]);
                                            if($course_service_model){
                                                $course_service_model->service_status = $old_order_model->service_status;
                                                $course_service_model->save();
                                            }
                                            $this->in['status'] = true;
                                            $pageData = $order_model->getPage();
                                            $this->in['data'] = $pageData['list'];
                                            $this->in['order_no'] = $order_model->order_no;
                                        }else{
                                            throw  new  Exception("转课失败");
                                        }

                                    }else{
                                        throw  new  Exception("日志错误");
                                    }
                                }else{
                                    throw  new  Exception("数据保存错误");
                                }
                            $transaction->commit();
                        }catch (Exception $e){
                            $this->in['status'] = false;
                            $this->in['message'] = $e->getMessage();
                            $transaction->rollBack();
                        }

                    }else{
                        $this->in['message'] = "数据格式错误，或者订单已经被添加";
                    }
                }
            }else{
                $this->in['message']="订单不存在";
            }

        }
        return $this->in;
    }

    /**
     * @return array
     * 转换状态到服务
     */
    public function actionPlay(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $model = Order::findOne($post_data['id']);
            if($model && $model->course_type == Course::COURSE_TYPE_1){
                $transaction = \Yii::$app->db->beginTransaction();
                try{
                    if($model->pay_status == Order::PAY_STATUS_2){
                        $order_log_model= new OrderLog();
                        $order_log_model->order_no = $model->order_no;
                        if($model->service_status == Order::SERVICE_STATUS_1){//开始服务
                            $order_log_model->actions = OrderLog::PLAY;
                            $model->service_status = Order::SERVICE_STATUS_2;
                            $model->service_time = time();
                            $course_service_model = new CourseService();
                            $course_service_model->order_no = $model->order_no;
                            $course_service_model->belong_teacher_id = $model->belong_teacher_id;
                            $course_service_model->student_id = $model->student_id;
                            $course_service_model->service_status = Order::SERVICE_STATUS_2;
                            $course_service_model->remark = $model->remark;
                        }elseif ($model->service_status == Order::SERVICE_STATUS_3){//继续服务
                            $model->service_status = Order::SERVICE_STATUS_4;
                            $model->service_time = time();
                            $order_log_model->actions = OrderLog::REPLAY;
                            $course_service_model = CourseService::findOne(['order_no'=>$model->order_no]);
                            $course_service_model->service_status = Order::SERVICE_STATUS_4;
                            $course_service_model->remark = $model->remark;
                        }
                        $order_log_model->service_status = $model->service_status;
                        $order_log_model->service_time =$model->service_time;
                        if($model->save() && $order_log_model->save() && $course_service_model->save()){
                            $this->in['status'] = true;
                            $this->in['data']= $model->service_status;
                        }else{
                            throw  new Exception("操作失败");
                        }
                    }else{
                        $this->in['message']= "付款没有结束！";
                    }
                    $transaction->commit();
                }catch (Exception $e){
                    $this->in['status'] = false;
                    $this->in['message']= $e->getMessage();
                    $transaction->rollBack();
                }

            }else{
                $this->in['message']= "数据不存在或该课程为视频课程！";
            }
        }
        return $this->in;
    }

    public function actionPause(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $model = Order::findOne($post_data['id']);
            if($model && $model->course_type == Course::COURSE_TYPE_1){
                $transaction = \Yii::$app->db->beginTransaction();
                try{
                    if($model->pay_status == Order::PAY_STATUS_2){
                        $order_log_model= new OrderLog();
                        $order_log_model->order_no = $model->order_no;
                        if($model->service_status == Order::SERVICE_STATUS_2){//开始服务
                            $order_log_model->actions = OrderLog::PAUSE;
                            $model->service_status = Order::SERVICE_STATUS_3;
                            $model->service_time = time();
                            $course_service_model = CourseService::findOne(['order_no'=>$model->order_no]);
                            $course_service_model->service_status = $model->service_status;
                            $order_log_model->service_status = $model->service_status;
                            $order_log_model->service_time =$model->service_time;
                            if($model->save() && $order_log_model->save() && $course_service_model->save()){
                                $this->in['status'] = true;
                                $this->in['data']= $model->service_status;
                            }else{
                                throw  new Exception("操作失败");
                            }
                        }else{
                            throw  new Exception("不可暂停");
                        }
                    }else{
                        $this->in['message']= "付款没有结束！";
                    }
                    $transaction->commit();
                }catch (Exception $e){
                    $this->in['status'] = false;
                    $this->in['message']= $e->getMessage();
                    $transaction->rollBack();
                }

            }else{
                $this->in['message']= "数据不存在或该课程为视频课程！";
            }
        }
        return $this->in;
    }

    /**
     * 提成
     */
    public function actionPush(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $model = Order::findOne($post_data['id']);
            $push_money = intval($post_data['push_money']);
            if($model){
                $transaction = \Yii::$app->db->beginTransaction();
                try{
                    if($model->service_status == Order::SERVICE_STATUS_5){
                        if(intval($model->push_money) == $push_money){
                            $this->in['status'] = true;
                            $this->in['data'] = $push_money;
                        }else{
                            $order_log_model= new OrderLog();
                            $order_log_model->actions = OrderLog::PUSH;
                            $order_log_model->order_no = $model->order_no;
                            $order_log_model->push_money = $push_money;
                            $model->push_money = $push_money;
                            if($model->save() && $order_log_model->save()){
                                $this->in['status'] = true;
                                $this->in['data'] = $push_money;
                            }else{
                                throw new Exception("数据错误");
                            }
                        }
                    }else{
                        $this->in['message']= "服务未结算";
                    }
                    $transaction->commit();
                }catch (Exception $e){
                    $this->in['status'] = false;
                    $this->in['message']= $e->getMessage();
                    $transaction->rollBack();
                }

            }else{
                $this->in['message']= "数据不存在！";
            }
        }
        return $this->in;
    }

}