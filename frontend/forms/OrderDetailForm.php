<?php
namespace frontend\forms;
use yii\base\Model;

class OrderDetailForm extends Model{
    public $id = 0;
    public $course_id;
    public $payment_id;
    public $payment_type;
    public $pay_end_time;
    public $sale_price;
    public $belong_sale_id;
    public $sale_wechat_no;
    public $remark;
    public $images;
    public function rules()
    {
        return parent::rules() + [
                [['course_id','payment_id','payment_type','sale_price','belong_sale_id','sale_wechat_no'],'required'],
                [['pay_end_time','sale_wechat_no','remark'],'string'],
                [['course_id','payment_id','payment_type','belong_sale_id','id'],'integer'],
                [['sale_price'],'double'],
                [['images'],'imageValidate']
            ];
    }

    public function imageValidate($attr,$params){
        if($this->$attr && !is_array($this->$attr)){
            $this->addError($attr,"图片格式错误");
        }
    }

}