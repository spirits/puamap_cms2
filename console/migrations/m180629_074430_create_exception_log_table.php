<?php

use yii\db\Migration;

/**
 * Handles the creation of table `exception_log`.
 */
class m180629_074430_create_exception_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('{{%exception_log}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string()->defaultValue(""),
            'code'=>$this->integer()->defaultValue(0),
            'line'=>$this->integer()->defaultValue(0),
            'file'=>$this->string()->defaultValue(""),
            'message'=>$this->text()->defaultValue(""),
            'trace'=>$this->text()->defaultValue(""),
            'create_time'=>$this->integer(11)->comment("创建时间"),
            'update_time'=>$this->integer(11)->comment("更新时间"),
            'is_deleted'=>$this->smallInteger(1)->defaultValue(0)->comment("是否删除"),
            'status'=>$this->smallInteger(1)->defaultValue(0)->comment("是否结束"),
            'operate_id'=>$this->integer(11)->defaultValue(0)->comment("0表示系统结算")
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%exception_log}}');
    }
}
