<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@common'=>dirname(__DIR__),
        '@frontend'=>dirname(dirname(__DIR__)) . '/frontend',
        '@console'=>dirname(dirname(__DIR__)) . '/console'
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser'
            ],
            'cookieValidationKey' => '_DNA_ZG5hQmFja2VuZAbeta5_api',
            'enableCsrfValidation'=>false
        ],

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'redis' =>[
            'class' => 'yii\redis\Connection',
            'hostname' => '127.0.0.1',
            'port' => 6379,
            'database' => 0,
        ],
        'db'=>[
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=rdsref3zq63ky6xefv0jepublic.mysql.rds.aliyuncs.com;dbname=yihe_from',
            'username' => 'tom',
            'password' => 'pua1900110',
            'charset' => 'utf8',
            'tablePrefix' => 'dna_',
        ],

    ],

];
