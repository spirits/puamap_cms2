<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m180615_094908_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'title'=>$this->string()->notNull()->comment("订单名词"),
            'order_no'=>$this->string()->notNull()->comment("订单号"),
            'child_no'=>$this->string()->defaultValue("")->comment("接课订单"),
            'course_id'=>$this->integer()->notNull()->comment("课程id"),
            'course_title'=>$this->string()->notNull()->comment('课程名称'),
            'belong_teacher_id'=>$this->integer(11)->notNull()->comment("所属导师"),
            'belong_teacher_ratio'=>$this->string()->notNull()->comment("导师收益率"),
            'push_money'=>$this->decimal(10,2)->defaultValue(0)->comment("提成金额"),
            'belong_sale_id'=>$this->integer(11)->notNull()->comment("所属销售"),
            "belong_sale_group_id"=>$this->integer(11)->notNull()->comment("销售组"),
            'duration'=>$this->integer(10)->defaultValue(0)->comment("时长"),
            'small_duration'=>$this->integer(10)->defaultValue(0)->comment("小课节数"),
            'bonus'=>$this->integer(11)->defaultValue(0)->comment("奖金"),
            'total_price'=>$this->decimal(10,2)->defaultValue(0)->comment("总价"),
            'paid_price'=>$this->decimal(10,2)->defaultValue(0)->comment("已付金额"),
            'coupon'=>$this->decimal(10,2)->defaultValue(0)->comment("优惠金额"),
            'big_price'=>$this->decimal(10,2)->defaultValue(0)->comment("大课价格"),
            'small_price'=>$this->decimal(10,2)->defaultValue(0)->comment("小课价格"),
            'seek_price'=>$this->decimal(10,2)->defaultValue(0)->comment("咨询价格"),
            'source_id'=>$this->integer(11)->notNull()->comment("来源"),
            'student_id'=>$this->integer(11)->notNull()->comment("用户id"),
            'order_status'=>$this->smallInteger()->defaultValue(0)->comment("订单状态"),//1.未审核 3.已审核 4.退款  5.转课  6.转接
            'service_status'=>$this->smallInteger()->defaultValue(0)->comment("是否开课"),//1.未开始服务，//2.服务中 //3.服务冻结,4.继续服务解冻 5服务结束
            'service_time'=>$this->integer(11)->comment("服务开始时间"),
            'pay_status'=>$this->smallInteger()->defaultValue(0)->comment("订单状态"),//1未完成 2已完成
            "pay_end_time"=>$this->integer(11)->comment("尾款时间"),
            'create_time'=>$this->integer(11)->comment("创建时间"),
            'update_time'=>$this->integer(11)->comment("更新时间"),
            'is_deleted'=>$this->smallInteger(1)->defaultValue(0)->comment("是否删除"),
            'status'=>$this->smallInteger(1)->defaultValue(0)->comment("是否正常"),
            'operate_id'=>$this->integer(11)->defaultValue(1)->comment("操作者"),
            'remark'=>$this->string(200)->defaultValue("")->comment("备注"),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%order}}');
    }
}
