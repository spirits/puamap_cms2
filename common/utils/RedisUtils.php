<?php
namespace  common\utils;
use common\models\Order;
use frontend\models\Admin;
use frontend\models\Course;
use yii\db\Query;

class RedisUtils{
    const PREFIX_LOGIN="login_";
    /**
     * @param $key
     * @param $data
     * @param int $duration
     * @return bool
     * 保存登录状态
     */
    public static function setLogin($key,$data,$duration=0){
        if(!$data){
            return false;
        }
        $key=self::PREFIX_LOGIN.$key;
        array_unshift($data,$key);
        \Yii::$app->redis->executeCommand("hmset",$data);
        \Yii::$app->redis->executeCommand("expire",[$key,$duration]);
    }

    /**
     * @param $key
     * @return mixed
     * 获取登录信息
     */
    public static function getLogin($key,$fields){
        $acckey=self::PREFIX_LOGIN.$key;
        array_unshift($fields,$acckey);
        return \Yii::$app->redis->executeCommand('hget',$fields);
    }

    /**
     * @param $key
     * @return mixed
     */
    public static function existsLogin($key){
        $key=self::PREFIX_LOGIN.$key;
        return static::existsKey($key);
    }

    /**
     * @param $key
     * @return mixed
     */
    public static function existsKey($key){
        return \Yii::$app->redis->exists($key);
    }

    /**
     * @param $key
     * @return mixed
     */
    public static function deleteLoginKey($key){
        $key=self::PREFIX_LOGIN.$key;
        return static::deleteKey($key);
    }

    /**
     * @param $key
     * @return mixed
     * 删除
     */
    public static function deleteKey($key){
        return \Yii::$app->redis->del($key);
    }
    public static function getNickname($id){
        $key = "manager_".$id;
        if(static::existsKey($key)){
            return \Yii::$app->redis->get($key);
        }
        $model = Admin::find()->select(['nickname'])->where(['id'=>$id])->one();
        if($model){
            $nickname =  $model->nickname;
            \Yii::$app->redis->set($key,$nickname);
        }else{
            $nickname = "-系统-";
        }
        return $nickname;
    }

    /**
     * @param $id
     * @param $data
     * 存储用户信息
     */
    public static function setTeacherAndSaleInfo($id,$data){
        $key="user_info_".$id;
        \Yii::$app->redis->set($key,json_encode($data));
    }

    /**
     * @param $id
     * 获取导师或销售信息
     */
    public static function getTeacherAndSaleInfo($id){
        $key="user_info_".$id;
        if(static::existsKey($key)){
            return json_decode(\Yii::$app->redis->get($key),true);
        }
        $model = Admin::findOne($id);
        if($model){
            $data = $model->toArray();
            static::setTeacherAndSaleInfo($id,$data);
            return $data;
        }
        return [];
    }

    /**
     * @param $id
     * @param $data
     * 存储课程信息
     */
    public static function setCourseInfo($id,$data){
        $key="course_info_".$id;
        \Yii::$app->redis->set($key,json_encode($data));
        \Yii::$app->redis->executeCommand("expire",[$key,2592000]);
    }

    /**
     * @param $id
     * @return array|mixed
     * 获取课程信息
     */
    public static function getCourseInfo($id){
        $key="course_info_".$id;
        if(static::existsKey($key)){
            return json_decode(\Yii::$app->redis->get($key),true);
        }
        $model = Course::findOne($id);
        if($model){
            $data = $model->toArray();
            static::setCourseInfo($id,$data);
            return $data;
        }
        return [];
    }

    public static function uniqueOrderCheck($course_id,$wechat_no,$mobile,$id =0){
        $key = "order_".$course_id."_".$wechat_no."_".$mobile;

        if($fid = static::existsKey($key)){
            if($id && $fid == $id){
                return false;
            }
            return true;
        }

        $query = new Query();
        $prefix = \Yii::$app->db->tablePrefix;
        $data = $query->select("o.id")->from($prefix."order as o")->leftJoin($prefix."student as s","o.student_id=s.id")
            ->where('o.is_deleted=0')
            ->andWhere('o.course_id='.intval($course_id))
            ->andWhere("s.wechat_no='".trim($wechat_no)."'")
            ->andWhere("o.order_status !=".Order::ORDER_STATUS_5)
            ->andWhere("s.mobile=".$mobile)->orderBy("o.id desc")->one();
        if($data){
            if($id && $data['id'] == $id){
               return false;
            }
            static::setUniqueOrder($course_id,$wechat_no,$mobile,$id);
            return true;
        }
        return false;
    }

    public static function deleteOrderCheck($course_id,$wechat_no,$mobile){
        $key = "order_".$course_id."_".$wechat_no."_".$mobile;
        static::deleteKey($key);
    }

    public static function setUniqueOrder($course_id,$wechat_no,$mobile,$id){
        $key = "order_".$course_id."_".$wechat_no."_".$mobile;
        \Yii::$app->redis->set($key,$id);
        \Yii::$app->redis->executeCommand("expire",[$key,2592000]);
    }

    /**
     * @param $k
     * @param int $time
     * 设置过去时间
     */
    public static function SetExpire($k,$time=60){
        \Yii::$app->redis->executeCommand("expire",[$k,$time]);
    }

    public static function regexDelete($key){
       $delKeys =  \Yii::$app->redis->executeCommand("keys",[$key]);
       if($delKeys){
           foreach ($delKeys as $v){
               static::deleteKey($v);
           }
       }

    }

}