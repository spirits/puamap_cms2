<?php
namespace frontend\controllers;
use frontend\models\OrderLog;

class OrderLogController extends BaseController{
    public function actionIndex(){
        $order_no = \Yii::$app->request->get("order_no","");
        $model = new OrderLog();
        $data['fields'] = $model->getFields();
        $pageData = $model->getPage($order_no);
        $data['items'] = $pageData['list'];
        $data['tableChose'] = $model->getTableChoseSelect();
        $data['pages'] = $model->getPageInfo(['order_no'=>$order_no]);
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }

    public function actionPage(){
        $page = \Yii::$app->request->post('page',1);
        $order_no = \Yii::$app->request->get("order_no","");
        $model = new OrderLog();
        $data = $model->getPage($order_no,$page);
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }

}