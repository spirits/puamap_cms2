<?php
namespace frontend\forms;
use common\utils\RedisUtils;
use yii\base\Model;

class OrderRedirectForm extends Model{
    public $source_id;
    public $payment_id;
    public $payment_type;
    public $title;
    public $course_id;
    public $sale_price;
    public $belong_sale_id;
    public $sale_wechat_no;
    public $wechat_no;
    public $mobile;
    public $status;
    public $remark;
    public $coupon;
    public $pay_end_time;
    public $images;
    public function rules()
    {
        return parent::rules() + [
                [['title','course_id','sale_price','belong_sale_id','sale_wechat_no','wechat_no','mobile','source_id','payment_type','payment_id'],'required'],
                [['title','sale_wechat_no','wechat_no','pay_end_time','remark'],'string'],
                [['course_id','mobile','status','belong_sale_id','source_id','payment_type','payment_id'],'integer'],
                [['sale_price','coupon'],'double'],
                ['mobile','match','pattern'=>'/1[3|4|5|7|8]\d{9}/','message'=>"电话号码错误"],
                [['course_id','wechat_no','mobile'],'uniqueCheck'],
                [['status','coupon'],'default','value'=>0],
                [['images'],'imageValidate']
            ];
    }


    public function uniqueCheck($attr,$params){
        if(!$this->hasErrors() && $this->course_id && $this->wechat_no && $this->mobile){
            if(RedisUtils::uniqueOrderCheck($this->course_id,$this->wechat_no,$this->mobile,0)){
                $this->addError($attr,"该订单已经被添加");
                return false;
            }
        }
    }
    public function imageValidate($attr,$params){
        if($this->$attr && !is_array($this->$attr)){
            $this->addError($attr,"图片格式错误");
        }
    }
}