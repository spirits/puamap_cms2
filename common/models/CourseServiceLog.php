<?php
namespace common\models;
/**
 * Class CourseServiceLog
 * @package common\models
 * @property int $id
 * @property int $service_id
 * @property int $service_status
 * @property int $remark
 * @property int $create_time
 * @property int $update_time
 * @property int $id_deleted
 * @property int $status
 * @property int $operate_id
 */
class CourseServiceLog extends ParentActiveRecord{
    public static function tableName()
    {
        return "{{%course_service_log}}";
    }

    public function attributeLabels()
    {
        return parent::attributeLabels() + [
                'service_id'=>'服务号',
                'service_status'=>'服务状态',
                'remark'=>'日志'
            ];
    }

}