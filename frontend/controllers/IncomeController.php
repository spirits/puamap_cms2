<?php
namespace frontend\controllers;
use common\utils\RedisUtils;
use frontend\models\Admin;
use frontend\models\Order;
use frontend\models\Role;
use yii\helpers\ArrayHelper;

class IncomeController extends BaseController{
    public function actionIndex(){
        $start_time = \Yii::$app->request->post("start_time","");
        $end_time = \Yii::$app->request->post("end_time","");
        $data['fields'] = $this->getFields();
        $data['items'] = $this->getSaleIncome($start_time,$end_time);
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }

    public function getFields(){
        return [
            [
                "label"=>"ID",
                'field'=>"id",
                'type'=>"String",
                'search'=>0,
                'style'=>"width:80px;"
            ],
            [
                "label"=>"销售",
                'field'=>"name",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>"底薪",
                'field'=>"income",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>"奖金",
                'field'=>"bonus",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>"总收入",
                'field'=>"total",
                'type'=>"String",
                'search'=>0
            ],
        ];
    }

    public function getSaleIncome($start_time,$end_time){
        $key = ($start_time || $end_time)  ?  'income_info_' : "income_info";
        if($start_time){
            $key .= $end_time ? "".strtotime($start_time)."_" : "".strtotime($start_time);
        }
        if($end_time){
            $key .="".strtotime($end_time);
        }

        $admin_model = new Admin();
        $sale_list = $admin_model->getSaleList(true);
       $query = Order::find()->select('belong_sale_id,sum(bonus) as bonus')->where($admin_model->baseWhere())
            ->andWhere(['pay_status'=>Order::PAY_STATUS_2]);
        $role_id = RedisUtils::getLogin($this->getAccessToken(),['role_id']);
        $uid = RedisUtils::getLogin($this->getAccessToken(),['uid']);
        if($role_id){
            switch ($role_id){
                case Role::SALE_NO;
                    $query->andWhere(['belong_sale_id'=>$uid]);
                    break;
                case Role::SALE_LEADER_NO:
                    $sale_list_items = Admin::find()->select(['id'])->andWhere(['leader_id'=>$uid,'is_deleted'=>0])->asArray()->all();
                    if($sale_list_items){
                        $uids  = ArrayHelper::getColumn($sale_list_items,'id');
                        $query->andWhere(["in",'belong_sale_id',$uids]);
                    }
                    break;

            }
        }

        if($start_time){
            $stime = strtotime($start_time." 00:00:00");
            $query->andWhere(['>=','pay_end_time',$stime]);
        }
        if($end_time){
            $etime = strtotime($end_time." 23:59:59");
            $query->andWhere(['<=','pay_end_time',$etime]);
        }
        $order_models = $query ->groupBy(['belong_sale_id'])->asArray()->all();
        $base_income = Role::getBaseIncome(Role::SALE_NO);
        if($sale_list){
            foreach ($sale_list as &$item){
                $item['bonus'] = 0;
                $item['income'] = $base_income;
                $item['total'] =  $base_income;
                if($order_models){
                    foreach ($order_models as $v){
                        if($v['belong_sale_id'] == $item['id']){
                            $item['bonus'] = $v['bonus'];
                            $item['total'] += $v['bonus'];
                            break;
                        }
                    }
                }
            }
        }
        if($sale_list){
            \Yii::$app->redis->set($key,json_encode($sale_list));
            RedisUtils::SetExpire($key);
        }

        return $sale_list;
    }
}