<?php
namespace frontend\forms;
use yii\base\Model;

class AdminForm extends  Model{
    public $username;
    public $nickname;
    public $password;
    public $role_id;
    public $status;

    public function rules()
    {
        return parent::rules() + [
                [['username','role_id'],'required'],
                [['username','password','nickname'],'string'],
                [['role_id','status'],'integer'],
                [['status'],'default','value'=>0]
            ];
    }
}