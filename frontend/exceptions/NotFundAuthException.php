<?php
namespace frontend\exceptions;
use yii\base\Exception;

class NotFundAuthException extends Exception{
    public function __construct()
    {
        parent::__construct("没有该权限", 402, null);
    }
}