<?php
namespace frontend\models;
use common\utils\RedisUtils;

class Course extends \common\models\Course{
    public $ratio = ["big"=>4,"small"=>4,'seek'=>2];
    /**
     * @return array
     * 显示列
     */
    public function getFields(){
        return [
            [
                "label"=>$this->getAttributeLabel("id"),
                'field'=>"id",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("title"),
                'field'=>"title",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$this->getAttributeLabel("course_type"),
                'field'=>"course_type",
                'type'=>"ChosenSelect",
                'search'=>1
            ],
            [
                "label"=>$this->getAttributeLabel("belong_teacher_id"),
                'field'=>"belong_teacher_id",
                'type'=>"ChosenSelect",
                'search'=>1,
                'style'=>"width:180px;"
            ],
            [
                "label"=>$this->getAttributeLabel("total_price"),
                'field'=>"total_price",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("duration"),
                'field'=>"duration",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("small_duration"),
                'field'=>"small_duration",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("apply_persons"),
                'field'=>"apply_persons",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("attend_persons"),
                'field'=>"attend_persons",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("refund_persons"),
                'field'=>"refund_persons",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("redirect_persons"),
                'field'=>"redirect_persons",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("create_time"),
                'field'=>"create_time",
                'type'=>"String",
                'search'=>0,
                'style'=>"width:200px;"
            ],
            [
                "label"=>$this->getAttributeLabel("update_time"),
                'field'=>"update_time",
                'type'=>"String",
                'search'=>0,
                'style'=>"width:200px;"
            ],
            [
                "label"=>$this->getAttributeLabel("operate_id"),
                'field'=>"operate_id",
                'type'=>"String",
                'search'=>0,
                'style'=>"width:200px;"
            ]
        ];
    }

    public function getTableChoseSelect(){
        $adminModel = new Admin();

        return [
            'belong_teacher_id'=>$adminModel->getTeacherList(),
            'course_type'=>$this->formatArray(static::$course_type_maps)
        ];
    }

    /**
     * @return array
     * 表单数据
     */
    public function getFormFields(){
        return [
            [
                "label"=>$this->getAttrLabel('course_type'),
                'name'=>"course_type",
                'type'=>"Switch",
                'value'=>1,
                'options'=>[['label'=>"私教",'value'=>1],['label'=>"视频",'value'=>2]]
            ],
            [
                "label"=>$this->getAttrLabel('title'),
                'name'=>"title",
                'type'=>"String",
                'value'=>"",
                "rules"=>['required'=>true]
            ],
            [
                "label"=>$this->getAttrLabel('total_price'),
                'name'=>"total_price",
                'type'=>"String",
                'value'=>"0",
                "rules"=>['required'=>true,"number"=>true]
            ],
            [
                "label"=>$this->getAttrLabel('belong_teacher_id'),
                'name'=>"belong_teacher_id",
                'type'=>"ChosenSelect",
                'value'=>0,
                "rules"=>['required'=>true]
            ],
            [
                "label"=>$this->getAttrLabel('duration'),
                'name'=>"duration",
                'type'=>"String",
                'value'=>"",
                "rules"=>['required'=>true,"number"=>true]
            ],
            [
                "label"=>$this->getAttrLabel('small_duration'),
                'name'=>"small_duration",
                'type'=>"String",
                'value'=>"",
                "rules"=>['required'=>true,"number"=>true]
            ],

        ];
    }

    public function beforeSave($insert)
    {
        return parent::beforeSave($insert);
    }

    public function  afterSave($insert, $changedAttributes)
    {
        RedisUtils::setCourseInfo($this->id,$this->toArray());
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }


    public function getPage($page=1,$params=[]){
        $query = self::find()->where($this->baseWhere())
            ->orderBy(['id'=>SORT_DESC]);

        $role_id = RedisUtils::getLogin($this->getAccessToken(),['role_id']);
        $uid = RedisUtils::getLogin($this->getAccessToken(),['uid']);
        if($role_id){
            switch ($role_id){
                case Role::TEACHER_NO;
                    $query->andWhere(['belong_teacher_id'=>$uid]);
                    break;
                case Role::ASSISTANT_NO:
                    $leader_id = RedisUtils::getLogin($this->getAccessToken(),['leader_id']);
                    if(!$leader_id){
                        $leader_id =  Admin::findOne($uid)->leader_id;
                    }
                    $query->andWhere(['belong_teacher_id'=>$leader_id]);
                    break;

            }
        }

        if($params){
            $query->andFilterWhere(['like','title',$params['title']]);
            if(isset($params['belong_teacher_id']) && $params['belong_teacher_id'] > -1) {
                $query->andFilterWhere(['belong_teacher_id' => intval($params['belong_teacher_id'])]);
            }
            if(isset($params['course_type']) && $params['course_type'] > -1) {
                $query->andFilterWhere(['course_type' => intval($params['course_type'])]);
            }
            if(isset($params['status']) && $params['status'] > -1){
                $query->andFilterWhere(['status'=>intval($params['status'])]);
            }
        }
        $offset = ($page - 1) * $this->pageSize;
        $totalCell = $query->count();
        $models= $query->offset($offset)->limit($this->pageSize)->all();
        $data = [];
        if($models){
            foreach ($models as $model) {
                $data[] = $model->toArray();
            }
        }
        return [
            'totalCell'=>$totalCell,
            'list'=>$data
        ];
    }

    public function courseArray($real=false){
        $query = static::find()->select(['id','title','total_price'])->where($this->baseWhere());
        if($real){
            $query->andWhere(['status'=>self::TRUE_STATUS]);
        }
        $data = $query->asArray()->all();;
        if($data){
            foreach ($data as &$v){
                $v['name'] = $v['title'];
            }
        }
        return $data;
    }
}