<?php
namespace common\models;
/**
 * Class Student
 * @package common\models
 * @property int $id
 * @property string $wechat_name
 * @property string $wechat_no
 * @property  double $income
 * @property int $age
 * @property string $major
 * @property string $address
 * @property int $add_time
 * @property int $inside
 * @property int $compact
 * @property int $create_time
 * @property int $update_time
 * @property int $id_deleted
 * @property int $status
 * @property int $operate_id
 * @property string $mobile
 */
class Student extends ParentActiveRecord{
    const NO_INSIDE = 0;
    const IS_INSIDE = 1;
    public static $inside_maps = [
        self::NO_INSIDE=>'否',
        self::IS_INSIDE=>"是"
    ];
    public static function tableName()
    {
        return "{{%student}}";
    }

    public function attributeLabels()
    {
        return parent::attributeLabels() + [
                'wechat_name'=>'微信名',
                'wechat_no'=>'微信号',
                'income'=>'月收入',
                'age'=>'年龄',
                'major'=>"职业",
                'address'=>'地址',
                'add_time'=>'好友时间',
                'inside'=>'内部',
                'compact'=>'合同',
                'mobile'=>'电话'
            ];
    }
}