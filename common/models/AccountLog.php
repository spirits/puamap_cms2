<?php
namespace common\models;
/**
 * Class AccountLog
 * @package common\models
 * @property int $id
 * @property string $order_no
 * @property int $actions
 * @property int $create_time
 * @property int $update_time
 * @property int $id_deleted
 * @property int $status
 * @property int $operate_id
 */
class AccountLog extends ParentActiveRecord{
    const ACTIONS_CARD = 1; //小课
    const ACTIONS_BIG = 2;  //大课
    public static $actions_map = [
        self::ACTIONS_CARD=>'小课',
        self::ACTIONS_BIG=>'大课'
    ];
    public static function tableName()
    {
        return "{{%account_log}}";
    }

    public function attributeLabels()
    {
        return parent::attributeLabels() + [
                'order_no'=>"单号",
                'actions'=>"行为"
            ];
    }
}