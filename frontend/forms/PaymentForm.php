<?php
namespace frontend\forms;
class PaymentForm extends \frontend\models\Payment {
    public function rules()
    {
        return parent::rules() + [
            [['name'],'required'],
            [['name'],'uniqueCheck']
        ];
    }
    public function uniqueCheck($attr,$params){
        $model = static::find()->where($this->baseWhere())->where(['name'=>$this->$attr])->one();
        if(!$this->id && $model){
            $this->addError($attr,$this->getAttributeLabel($attr)." 已经添加!");
            return false;
        }
        if($model && $this->id  != $model->id){
            $this->addError($attr,$this->getAttributeLabel($attr)." 已经添加!");
            return false;
        }
        return true;
    }
}