<?php
namespace frontend\models;
class OrderLog extends \common\models\OrderLog{



    public function fields()
    {
        $fields = parent::fields();
        $fields['images'] = function($model){
            return $model->images ? explode(",",$model->images) : [];
        };
        $fields['add_time'] = function($model){
            return ($model->add_time && $model->add_time !="-") ? date($model->timeFormat,$model->add_time) : "-";
        };
        $fields['service_time'] = function($model){
            return ($model->service_time && $model->service_time !="-") ? date($model->timeFormat,$model->service_time) : "-";
        };
        $fields['pay_end_time'] = function($model){
            return ($model->pay_end_time && $model->pay_end_time !="-") ? date($model->timeFormat,$model->pay_end_time) : "-";
        };
        return $fields;
    }

    public function getFields(){
        $attributes = $this->getAttributes();
        $fields = [];

        foreach ($attributes as $k=>$v){
            if(!in_array($k,["is_deleted",'update_time','status','order_no'])){

                switch ($k){
                    case 'images':
                        $type = "Image";
                        break;
                    case "payment_id":
                    case "payment_type":
                    case "course_id":
                    case "belong_sale_group_id":
                    case "belong_sale_id":
                    case "order_detail_status":
                    case "belong_teacher_id":
                    case "order_status":
                    case 'service_status':
                    case 'pay_status':
                    case 'inside':
                    case "compact":
                    case 'source_id':
                    case 'actions':
                    case "course_type":
                        $type = 'ChosenSelect';
                        break;
                    default:
                        $type='String';
                        break;
                }
                $temp = [
                    "label"=>$this->getAttributeLabel($k),
                    'field'=>$k,
                    'type'=>$type,
                    'search'=>0
                ];
                $fields[] = $temp;
            }

        }
        return $fields;
    }

    public function getTableChoseSelect(){
        $payment_model = new Payment();
        $course_model = new Course();
        $admin_model = new Admin();
        $source_model = new Source();
        return [
            'payment_id'=>$payment_model->paymentArray(),
            'payment_type'=>$this->formatArray(Order::$payment_type_maps),
            'course_id'=>$course_model->courseArray(),
            'belong_sale_group_id'=>$admin_model->getSaleLeaderList(),
            'belong_sale_id'=>$admin_model->getSaleList(),
            "order_detail_status"=>$this->formatArray(OrderDetail::$order_detail_status_maps),
            'belong_teacher_id'=>$admin_model->getTeacherList(),
            'order_status'=>$this->formatArray(Order::$order_status_maps),
            'service_status'=>$this->formatArray(Order::$service_status_maps),
            'pay_status'=>$this->formatArray(Order::$pay_status_maps),
            'inside'=>$this->formatArray(Student::$inside_maps),
            'compact'=>$this->formatArray(Student::$inside_maps),
            'source_id'=>$source_model->sourceArray(),
            'actions'=>$this->formatArray(static::$actions_maps),
            'course_type'=>$this->formatArray(Course::$course_type_maps)
        ];
    }

    public function getPage($order_no,$page=1){
        $offset = ($page - 1) * $this->pageSize;
        $query = self::find()->where(['order_no'=>$order_no]);
        $totalCell = $query->count();
        $data = $query->offset($offset)->limit($this->pageSize)->orderBy(['id'=>SORT_ASC])->all();
        if($data){
            foreach ($data as &$v){
               $v=$v->toArray();
            }
        }
        return [
            'totalCell'=>$totalCell,
            'list'=>$data
        ];
    }
}