<?php

use yii\db\Migration;

/**
 * Handles the creation of table `role`.
 */
class m180613_081341_create_role_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%role}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(200)->notNull()->comment("名称"),
            "income_status"=>$this->smallInteger(1)->defaultValue(1)->comment("收益状态"),
            "income"=>$this->decimal(10,2)->comment("底薪"),
            'income_ratio'=>$this->string(2000)->comment("收益比"),
            'parent_id'=>$this->integer(11)->defaultValue(0)->comment("父级"),
            'sort'=>$this->integer(11)->defaultValue(1)->comment("排序"),
            'create_time'=>$this->integer(11)->comment("创建时间"),
            'update_time'=>$this->integer(11)->comment("更新时间"),
            'is_deleted'=>$this->smallInteger(1)->defaultValue(0)->comment("是否删除"),
            'status'=>$this->smallInteger(1)->defaultValue(0)->comment("是否正常"),
            'operate_id'=>$this->integer(11)->defaultValue(1)->comment("操作者")
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%role}}');
    }
}
