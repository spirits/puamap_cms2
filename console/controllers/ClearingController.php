<?php
namespace console\controllers;
use common\models\AccountLog;
use common\models\Course;
use common\models\CourseService;
use common\models\CourseServiceLog;
use common\models\ExceptionLog;
use common\models\Order;
use common\models\OrderLog;
use yii\console\Controller;

class ClearingController extends Controller{

    public function actionIndex(){
        $transaction = \Yii::$app->db->beginTransaction();
        try{
            $course_service_model_list = $this->getCourseServiceDuring();
            $course_service_log_model = null;
            $account_log_model = null;
            if($course_service_model_list){
                foreach ($course_service_model_list as $course_service_model){
                    $order_model = (new Order)->getOrderInfo($course_service_model->order_no);
                    if($order_model){
                        if($course_service_model->big_process >= $order_model->duration){//已近结算了已经结算
                            if($course_service_model->small_process >= $order_model->small_duration){
                                $order_model->service_status = Order::SERVICE_STATUS_5;
                                $order_model->service_time = time();
                                $order_model->save();
                                $order_log = new OrderLog();
                                $order_log->order_no = $order_model->order_no;
                                $order_log->service_status = Order::SERVICE_STATUS_5;
                                $order_log->service_time = time();
                                $order_log->actions = OrderLog::END;
                                $order_log->save();
                                $course_service_model->service_status = Order::SERVICE_STATUS_5;
                                $course_service_model->update();
                                $course_service_log_model = new CourseServiceLog();
                                $course_service_log_model->service_id = $course_service_model->id;
                                $course_service_log_model->service_status = $course_service_model->service_status;
                                $course_service_log_model->remark ="系统判定服务结束";
                                $course_service_log_model->save();
                            }
                        }else if($course_service_model->big_process == ($order_model->duration - 1)){//差一次满足结算
                            $course_service_model->big_process += 1;
                            if($course_service_model->small_process >= $order_model->small_duration){
                                $order_model->service_status = Order::SERVICE_STATUS_5;
                                $order_model->service_time = time();
                                $order_model->save();
                                $order_log = new OrderLog();
                                $order_log->order_no = $order_model->order_no;
                                $order_log->service_status = Order::SERVICE_STATUS_5;
                                $order_log->service_time = time();
                                $order_log->actions = OrderLog::END;
                                $order_log->save();
                                $course_service_model->service_status = Order::SERVICE_STATUS_5;
                                $course_service_model->update();
                                $course_service_log_model = new CourseServiceLog();
                                $course_service_log_model->service_id = $course_service_model->id;
                                $course_service_log_model->service_status = $course_service_model->service_status;
                                $course_service_log_model->remark ="系统判定服务结束";
                                $course_service_log_model->save();
                            }else{
                                $course_service_model->update();
                            }
                            $account_log_model =  new AccountLog();
                            $account_log_model->order_no=$course_service_model->order_no;
                            $account_log_model->actions = AccountLog::ACTIONS_BIG;
                            $account_log_model->save();
                        }else{//未满足结算条件
                            $course_service_model->big_process += 1;
                            $course_service_model->update();
                            $account_log_model =  new AccountLog();
                            $account_log_model->order_no=$course_service_model->order_no;
                            $account_log_model->actions = AccountLog::ACTIONS_BIG;
                            $account_log_model->save();
                        }

                    }
                }
            }
            $transaction->commit();
        }catch (\Exception $e){
            $transaction->rollBack();
            $exception_log = new ExceptionLog();
            $exception_log->name = $e->getName();
            $exception_log->code = $e->getCode();
            $exception_log->file = $e->getFile();
            $exception_log->line = $e->getLine();
            $exception_log->message = $e->getMessage();
            $exception_log->trace = $e->getTraceAsString();
            $exception_log->save();
        }

    }

    /**
     * 获取所有正在服务的课程
     */
    public function getCourseServiceDuring(){
        $course_service_models = CourseService::find()->where(['is_deleted'=>0,'status'=>0])
            ->andWhere(['in','service_status',Order::$able_service_maps])->all();
       return $course_service_models;
    }


}