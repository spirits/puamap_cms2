/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50547
Source Host           : 127.0.0.1:3306
Source Database       : cms2.0

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2018-06-29 18:32:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `dna_account_log`
-- ----------------------------
DROP TABLE IF EXISTS `dna_account_log`;
CREATE TABLE `dna_account_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(200) NOT NULL COMMENT '单号',
  `actions` tinyint(3) NOT NULL COMMENT '1表示打卡2表示大课结算',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` smallint(1) DEFAULT '0' COMMENT '是否删除',
  `status` smallint(1) DEFAULT '0' COMMENT '是否结束',
  `operate_id` int(11) DEFAULT '0' COMMENT '0表示系统结算',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dna_account_log
-- ----------------------------
INSERT INTO `dna_account_log` VALUES ('1', 'HN92R20180623113636', '1', '1530001312', '1530001312', '0', '0', '1');
INSERT INTO `dna_account_log` VALUES ('2', 'HN92R20180623113636', '1', '1530001361', '1530001361', '0', '0', '1');
INSERT INTO `dna_account_log` VALUES ('3', 'HN92R20180623113636', '1', '1530001412', '1530001412', '0', '0', '1');
INSERT INTO `dna_account_log` VALUES ('4', 'HN92R20180623113636', '1', '1530002770', '1530002770', '0', '0', '1');
INSERT INTO `dna_account_log` VALUES ('5', 'HN92R20180623113636', '1', '1530002784', '1530002784', '0', '0', '1');
INSERT INTO `dna_account_log` VALUES ('6', 'HN92R20180623113636', '1', '1530081229', '1530081229', '0', '0', '1');
INSERT INTO `dna_account_log` VALUES ('8', 'HN92R20180623113636', '2', '1530260602', '1530260602', '0', '0', '-1');
INSERT INTO `dna_account_log` VALUES ('9', 'HN92R20180623113636', '2', '1530261058', '1530261058', '0', '0', '-1');
INSERT INTO `dna_account_log` VALUES ('10', 'HN92R20180623113636', '2', '1530261142', '1530261142', '0', '0', '-1');

-- ----------------------------
-- Table structure for `dna_admin`
-- ----------------------------
DROP TABLE IF EXISTS `dna_admin`;
CREATE TABLE `dna_admin` (
  `leader_id` int(11) unsigned DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL COMMENT '账户',
  `nickname` varchar(200) DEFAULT NULL COMMENT '昵称',
  `access_token` varchar(200) DEFAULT NULL,
  `password` varchar(200) NOT NULL COMMENT '密码',
  `role_id` smallint(6) DEFAULT '0' COMMENT '类型',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建',
  `update_time` int(11) DEFAULT NULL COMMENT '更新',
  `login_time` int(11) DEFAULT NULL COMMENT '本次登录',
  `is_deleted` smallint(1) DEFAULT '0' COMMENT '删除',
  `status` smallint(1) DEFAULT '0' COMMENT '状态',
  `operate_id` int(11) DEFAULT '0' COMMENT '操作者',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dna_admin
-- ----------------------------
INSERT INTO `dna_admin` VALUES ('0', '1', 'admin', '系统管理员', 'urFq-G6gSVmZBy67q4j_Ze5Ottoe10J0', '$2y$13$noHxLTcBcZEkEEtCYJFlQeSu/CPBpihdrF..msN2aqwqclMo9U3UK', '1', '1528185085', '1530266278', '1530266278', '0', '0', '1');
INSERT INTO `dna_admin` VALUES ('0', '2', 'soso', '思琪', 'rkkLUQhQKkmbPdYzirSoH3X3mrS1Wi7c', '$2y$13$fguSsgSBNlhyNt72uxkiDOc/P7dfxEKm8E63u4v8Ympv/k7ahZtqm', '3', '1528962103', '1528967565', null, '0', '1', '1');
INSERT INTO `dna_admin` VALUES ('0', '3', 'puamap', '明明', 'xM35JsVwu0t091m1kQMLd0dtFgr0voRF', '$2y$13$ff49Uac8A1DeHzEj8HyGyuiCS0yn7hSi98xwvbImoqYRaW8a.UqU2', '2', '1528964604', '1528964604', null, '0', '0', '1');
INSERT INTO `dna_admin` VALUES ('0', '4', 'langzi', '浪子', 'mEBl0PIxDs7vPsmniproVROUtxzMnECB', '$2y$13$9tqqNPlVJUUa5akkuJYTqOs2mKYKUrdXIbPwwOy3LnMABtmhYHVzK', '5', '1529039910', '1529039910', null, '0', '0', '1');
INSERT INTO `dna_admin` VALUES ('0', '5', 'zuoyang', '左阳', 'O6GQXiQUaT86mqWvCtcXW-5FFc7st1lg', '$2y$13$p4IxNxyJrsWQHfLA.4CVFOqwEhuC/WgxkgIHfZVnOTzRoPjdplbOW', '5', '1529039978', '1530265410', '1530265410', '0', '0', '1');
INSERT INTO `dna_admin` VALUES ('4', '6', 'xiaobai', '小白', '6rOCakNtiDoT-eGngBWWpx78NTBVYbUf', '$2y$13$aoPUa9ZytXfKo0nGif9SkuymGGeR9G3aFfPKPkVQP75tgf3LpVuVW', '6', '1529041898', '1529041898', null, '0', '0', '1');
INSERT INTO `dna_admin` VALUES ('5', '7', 'dabai', '大白猫', 'tLpIMYtyVYFsEccXLMPlRPLcb--B9PrH', '$2y$13$IA5an3NxSaTb7iYGfDU4GOtRVusg/fP1r.J3JK7oc82ul5wqTEkYC', '6', '1529042153', '1529042315', null, '0', '0', '1');
INSERT INTO `dna_admin` VALUES ('0', '8', 'sale_first', '销售一号主管', '1-jL1JyHdpXEu83yPRuRpTFHALCIhyQE', '$2y$13$6wxhMPOowI6aIPfGm3BEdu2bCImJu0vYvA8d71b3QphAXV1XJUip6', '8', '1529043173', '1530264199', '1530264200', '0', '0', '1');
INSERT INTO `dna_admin` VALUES ('0', '9', 'sale_second', '销售二号主管', 'YemGqGGxWhkcsgtl6KtWxI-Bbx-V0qp5', '$2y$13$5wgViNUP8WJxnm37e6EC2eoZAkzOEJHOgz.WF.OVq505Zun9M5PLS', '8', '1529043220', '1529043220', null, '0', '0', '1');
INSERT INTO `dna_admin` VALUES ('8', '10', 'xs_one', '销售一号', 'c6CBN5JP8DbnSiEdxig98PaL8eXmRSy-', '$2y$13$yChx8saCTKzO3R8tv0Ff1OtOBOMlnLXNOOMN.VfYqo4DaNipODW6u', '9', '1529043329', '1529045917', null, '0', '0', '1');
INSERT INTO `dna_admin` VALUES ('8', '11', 'xs_two', '销售二号', '5-D-6-Zgwc50K1TLNzWKPgMsm78AXbsj', '$2y$13$g.c6zk5zLbPqWGIjBjxVMeILWv7Q/JNS4LBHS1pALXY8i7pLeC49C', '9', '1529043381', '1529043381', null, '0', '0', '1');
INSERT INTO `dna_admin` VALUES ('9', '12', 'sale_three', '销售三号', 'GWtZnqtvY3bd0jp3nwv99a4Ok1e6a4K3', '$2y$13$uf8S8rZ5y8CljaDIZ9LFXetrzhcU4nJaJTuS0vlWZLbOEJfQ8RIUG', '9', '1529043569', '1529043569', null, '0', '0', '1');
INSERT INTO `dna_admin` VALUES ('9', '13', 'sale_4', '销售四号', 'OLNT3Lh9bYwUiQzowtUETazQQBP85L0t', '$2y$13$lUha8wBExC.H7cyrNxIvEuV0OiTNtoj3Arv3vWky3lVutF28PCEPq', '9', '1529043783', '1529045714', null, '0', '0', '1');

-- ----------------------------
-- Table structure for `dna_course`
-- ----------------------------
DROP TABLE IF EXISTS `dna_course`;
CREATE TABLE `dna_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT '课程名称',
  `belong_teacher_id` int(10) NOT NULL COMMENT '所属导师',
  `total_price` int(10) NOT NULL COMMENT '总价',
  `bonus` smallint(5) DEFAULT '0' COMMENT '奖金',
  `apply_persons` int(10) DEFAULT '0' COMMENT '报名人数',
  `attend_persons` int(10) DEFAULT '0' COMMENT '上课人数',
  `refund_persons` int(10) DEFAULT '0' COMMENT '退款人数',
  `redirect_persons` int(10) DEFAULT '0' COMMENT '转课人数',
  `duration` int(10) DEFAULT '0' COMMENT '时长',
  `small_duration` int(10) DEFAULT '0' COMMENT '小课节数',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` smallint(1) DEFAULT '0' COMMENT '是否删除',
  `status` smallint(1) DEFAULT '0' COMMENT '是否正常',
  `operate_id` int(11) DEFAULT '1' COMMENT '操作者',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dna_course
-- ----------------------------
INSERT INTO `dna_course` VALUES ('1', '浪子蓉城计划', '4', '20000', '0', '-1', '0', '0', '0', '15', '6', '1529052424', '1529724996', '0', '0', '1');
INSERT INTO `dna_course` VALUES ('2', '左阳广州计划', '5', '22000', '0', '2', '0', '0', '0', '30', '10', '1529053799', '1529724996', '0', '0', '1');

-- ----------------------------
-- Table structure for `dna_course_service`
-- ----------------------------
DROP TABLE IF EXISTS `dna_course_service`;
CREATE TABLE `dna_course_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL COMMENT '用户id',
  `order_no` varchar(255) NOT NULL COMMENT '订单id',
  `big_process` int(10) DEFAULT '0' COMMENT '大课进度',
  `small_process` int(10) DEFAULT '0' COMMENT '小课进度',
  `service_status` tinyint(3) DEFAULT NULL COMMENT '服务状态',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` smallint(1) DEFAULT '0' COMMENT '是否删除',
  `status` smallint(1) DEFAULT '0' COMMENT '是否结束',
  `operate_id` int(11) DEFAULT '1' COMMENT '操作者',
  `belong_teacher_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dna_course_service
-- ----------------------------
INSERT INTO `dna_course_service` VALUES ('1', '1', 'HN92R20180623113636', '3', '6', '4', '继续服务', '1529737327', '1530261142', '0', '0', '-1', '5');

-- ----------------------------
-- Table structure for `dna_course_service_log`
-- ----------------------------
DROP TABLE IF EXISTS `dna_course_service_log`;
CREATE TABLE `dna_course_service_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL COMMENT '服务',
  `service_status` tinyint(3) DEFAULT NULL COMMENT '服务状态',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` smallint(1) DEFAULT '0' COMMENT '是否删除',
  `status` smallint(1) DEFAULT '0' COMMENT '是否结束',
  `operate_id` int(11) DEFAULT '1' COMMENT '操作者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dna_course_service_log
-- ----------------------------
INSERT INTO `dna_course_service_log` VALUES ('1', '1', '2', '开始服务', '1529737327', '1529737327', '0', '0', '1');
INSERT INTO `dna_course_service_log` VALUES ('2', '1', '3', '暂停服务', '1529737734', '1529737734', '0', '0', '1');
INSERT INTO `dna_course_service_log` VALUES ('3', '1', '4', '继续服务', '1529737806', '1529737806', '0', '0', '1');
INSERT INTO `dna_course_service_log` VALUES ('4', '1', '4', '继续服务', '1529737879', '1529737879', '0', '0', '1');
INSERT INTO `dna_course_service_log` VALUES ('5', '1', '4', '当前需要上课极度不认真', '1529991907', '1529991907', '0', '0', '1');
INSERT INTO `dna_course_service_log` VALUES ('6', '1', '4', '学习认真程度有待改善', '1529991948', '1529991948', '0', '0', '1');
INSERT INTO `dna_course_service_log` VALUES ('8', '1', '4', '继续服务', '1530260602', '1530260602', '0', '0', '-1');

-- ----------------------------
-- Table structure for `dna_exception_log`
-- ----------------------------
DROP TABLE IF EXISTS `dna_exception_log`;
CREATE TABLE `dna_exception_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `code` int(11) DEFAULT '0',
  `file` varchar(255) DEFAULT '',
  `line` int(11) DEFAULT NULL,
  `message` text,
  `trace` text,
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` smallint(1) DEFAULT '0' COMMENT '是否删除',
  `status` smallint(1) DEFAULT '0' COMMENT '是否结束',
  `operate_id` int(11) DEFAULT '0' COMMENT '0表示系统结算',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dna_exception_log
-- ----------------------------

-- ----------------------------
-- Table structure for `dna_migration`
-- ----------------------------
DROP TABLE IF EXISTS `dna_migration`;
CREATE TABLE `dna_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dna_migration
-- ----------------------------
INSERT INTO `dna_migration` VALUES ('m000000_000000_base', '1528185083');
INSERT INTO `dna_migration` VALUES ('m130524_201442_init', '1528185085');
INSERT INTO `dna_migration` VALUES ('m180605_074449_create_admin_table', '1528185085');
INSERT INTO `dna_migration` VALUES ('m180612_030557_create_node_table', '1528774287');
INSERT INTO `dna_migration` VALUES ('m180613_081341_create_role_table', '1528877680');
INSERT INTO `dna_migration` VALUES ('m180615_070544_create_course_table', '1529047023');
INSERT INTO `dna_migration` VALUES ('m180615_094908_create_order_table', '1529393412');
INSERT INTO `dna_migration` VALUES ('m180619_022011_create_order_detail_table', '1529393413');
INSERT INTO `dna_migration` VALUES ('m180619_030721_create_student_table', '1529393413');
INSERT INTO `dna_migration` VALUES ('m180619_035908_create_payment_table', '1529388276');
INSERT INTO `dna_migration` VALUES ('m180619_035543_create_source_table', '1529393413');
INSERT INTO `dna_migration` VALUES ('m180619_055457_create_order_log_table', '1529393413');
INSERT INTO `dna_migration` VALUES ('m180623_062147_create_course_service_table', '1529735777');
INSERT INTO `dna_migration` VALUES ('m180623_062155_create_course_service_log_table', '1529735777');
INSERT INTO `dna_migration` VALUES ('m180626_080541_create_account_log_table', '1530000704');
INSERT INTO `dna_migration` VALUES ('m180629_074430_create_exception_log_table', '1530258467');

-- ----------------------------
-- Table structure for `dna_node`
-- ----------------------------
DROP TABLE IF EXISTS `dna_node`;
CREATE TABLE `dna_node` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL COMMENT '名称',
  `route` varchar(200) NOT NULL COMMENT '路由',
  `parent_id` int(11) DEFAULT '0' COMMENT '父级',
  `sort` int(11) DEFAULT '1' COMMENT '排序',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` smallint(1) DEFAULT '0' COMMENT '是否删除',
  `status` smallint(1) DEFAULT '0' COMMENT '是否正常',
  `operate_id` int(11) DEFAULT '1' COMMENT '操作者',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dna_node
-- ----------------------------
INSERT INTO `dna_node` VALUES ('1', '首页', 'index/index', '0', '1', '1528185085', '1528185085', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('2', '系统设置', 'system/index', '1', '1', '1528777141', '1528779358', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('3', '节点设置', 'node/index', '2', '2', '1528777331', '1528782290', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('5', '系统参数', 'system/index', '2', '1', '1528779035', '1528782290', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('7', '添加', 'node/create', '3', '1', '1528782324', '1528782324', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('6', '成员管理', 'role/index', '1', '2', '1528780708', '1528785450', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('8', '修改', 'node/update', '3', '2', '1528782342', '1528782342', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('9', '删除', 'node/delete', '3', '3', '1528782366', '1528782366', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('10', '上移', 'node/up', '3', '4', '1528782392', '1528782392', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('11', '下移', 'node/down', '3', '5', '1528782407', '1528782407', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('12', '左移', 'left', '3', '6', '1528782428', '1528784448', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('13', '右移', 'right', '3', '7', '1528782448', '1528784448', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('14', '角色浏览', 'role/index', '6', '1', '1528785431', '1528785431', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('15', '管理员', 'manage/index', '6', '2', '1528785603', '1530152197', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('16', '助教管理', 'assistant/index', '6', '3', '1528785635', '1528785635', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('17', '销售管理', 'sale/index', '6', '4', '1528785676', '1528785676', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('18', '角色添加', 'role/create', '14', '1', '1528785923', '1528785923', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('19', '角色修改', 'role/update', '14', '2', '1528785939', '1528785939', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('20', '角色删除', 'role/delete', '14', '3', '1528785960', '1528785960', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('21', '添加', 'manage/create', '15', '1', '1528785988', '1528785988', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('22', '修改', 'manage/update', '15', '2', '1528786009', '1528786009', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('23', '删除', 'manage/delete', '15', '3', '1528786026', '1528786026', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('24', '添加', 'assistant/create', '16', '1', '1528786056', '1528786056', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('25', '修改', 'assistant/update', '16', '2', '1528786076', '1528786076', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('26', '删除', 'assistant/delete', '16', '3', '1528786257', '1528786257', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('27', '添加', 'sale/create', '17', '1', '1528786278', '1528786278', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('28', '修改', 'sale/update', '17', '2', '1528786303', '1528786303', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('29', '删除', 'sale/delete', '17', '3', '1528786331', '1528786331', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('30', '支付设置', 'payment/index', '2', '3', '1529388778', '1529393674', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('31', '来源设置', 'source/index', '2', '4', '1529388801', '1529388801', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('32', '添加', 'payment/create', '30', '1', '1529388816', '1529393680', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('33', '修改', 'payment/update', '30', '2', '1529388832', '1529393687', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('34', '删除', 'payment/delete', '30', '3', '1529388844', '1529393692', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('35', '添加', 'source/create', '31', '1', '1529388864', '1529388864', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('36', '修改', 'source/update', '31', '2', '1529388880', '1529388880', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('37', '删除', 'source/delete', '31', '3', '1529388897', '1529388897', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('38', '课程管理', 'course/index', '1', '3', '1529398279', '1529398279', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('39', '课程列表', 'course/index', '38', '1', '1529398318', '1529398318', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('40', '添加', 'course/create', '39', '1', '1529398336', '1529398336', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('41', '修改', 'course/update', '39', '2', '1529398355', '1529398355', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('42', '删除', 'course/delete', '39', '3', '1529398380', '1529398380', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('43', '订单管理', 'order/index', '1', '4', '1529398414', '1529398414', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('44', '订单列表', 'order/index', '43', '1', '1529398430', '1529398430', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('45', '添加', 'order/create', '44', '1', '1529398447', '1529398447', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('46', '修改', 'order/update', '44', '2', '1529398469', '1529398469', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('47', '删除', 'order/delete', '44', '3', '1529398490', '1529398490', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('48', '课程服务', 'course-service/index', '38', '2', '1530152284', '1530152284', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('49', '打卡', 'course-service/add-card', '48', '1', '1530152338', '1530152338', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('50', '添加日志', 'course-service/add-log', '48', '2', '1530152361', '1530152361', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('51', '查看日志', 'course-service-log/index', '48', '3', '1530152422', '1530152422', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('52', '详情', 'order-detail/index', '44', '4', '1530152477', '1530152477', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('53', '转单', 'order/return', '44', '5', '1530152523', '1530152523', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('54', '转课', 'order/redirect', '44', '6', '1530152544', '1530152544', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('55', '提成', 'order/push', '44', '7', '1530152587', '1530152587', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('56', '启动服务', 'order/play', '44', '8', '1530152703', '1530152703', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('57', '暂停服务', 'order/pause', '44', '9', '1530152734', '1530152734', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('58', '查看日志', 'order-log/index', '44', '10', '1530152768', '1530152768', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('59', '财务管理', 'finance/index', '1', '5', '1530152831', '1530152831', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('60', '未审订单', 'finance/index', '59', '1', '1530152925', '1530152925', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('61', '通过', 'finance/pass', '60', '1', '1530152998', '1530152998', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('62', '奖励', 'finance/bonus', '60', '2', '1530153022', '1530153022', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('63', '收入管理', 'income/index', '1', '6', '1530153075', '1530153075', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('64', '销售收入', 'income/index', '63', '1', '1530153102', '1530153102', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('65', '销售组业绩', 'sale-group/index', '63', '2', '1530153162', '1530153162', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('66', '导师收入', 'tutor-income/index', '63', '3', '1530153209', '1530153209', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('67', '权限', 'role/auth', '14', '4', '1530177671', '1530177671', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('68', '添加', 'order-detail/create', '52', '1', '1530178383', '1530178383', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('69', '修改', 'order-detail/update', '52', '2', '1530178429', '1530178429', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('70', '删除', 'order-detail/delete', '52', '3', '1530178487', '1530178487', '0', '0', '1');
INSERT INTO `dna_node` VALUES ('71', '点单查询', 'search/index', '43', '2', '1530265806', '1530266360', '0', '0', '1');

-- ----------------------------
-- Table structure for `dna_order`
-- ----------------------------
DROP TABLE IF EXISTS `dna_order`;
CREATE TABLE `dna_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT '订单名词',
  `order_no` varchar(255) NOT NULL COMMENT '订单号',
  `course_id` int(11) NOT NULL COMMENT '课程id',
  `course_title` varchar(255) NOT NULL COMMENT '课程名称',
  `belong_teacher_id` int(11) NOT NULL COMMENT '所属导师',
  `belong_teacher_ratio` varchar(255) NOT NULL COMMENT '导师收益率',
  `push_money` decimal(10,2) DEFAULT '0.00' COMMENT '提成金额',
  `belong_sale_id` int(11) NOT NULL COMMENT '所属销售',
  `belong_sale_group_id` int(11) NOT NULL COMMENT '销售组',
  `bonus` int(11) DEFAULT '0' COMMENT '奖金',
  `total_price` decimal(10,2) DEFAULT '0.00' COMMENT '总价',
  `paid_price` decimal(10,2) DEFAULT '0.00' COMMENT '已付金额',
  `coupon` decimal(10,2) DEFAULT '0.00' COMMENT '优惠金额',
  `big_price` decimal(10,2) DEFAULT '0.00' COMMENT '大课价格',
  `small_price` decimal(10,2) DEFAULT '0.00' COMMENT '小课价格',
  `seek_price` decimal(10,2) DEFAULT '0.00' COMMENT '咨询价格',
  `source_id` int(11) NOT NULL COMMENT '来源',
  `student_id` int(11) NOT NULL COMMENT '用户id',
  `order_status` smallint(6) DEFAULT '0' COMMENT '订单状态',
  `service_status` smallint(6) DEFAULT '0' COMMENT '是否开课',
  `service_time` int(11) DEFAULT NULL COMMENT '服务开始时间',
  `pay_status` smallint(6) DEFAULT '0' COMMENT '订单状态',
  `pay_end_time` int(11) DEFAULT NULL COMMENT '尾款时间',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` smallint(1) DEFAULT '0' COMMENT '是否删除',
  `status` smallint(1) DEFAULT '0' COMMENT '是否正常',
  `operate_id` int(11) unsigned DEFAULT '1' COMMENT '操作者',
  `remark` varchar(200) DEFAULT '' COMMENT '备注',
  `child_no` varchar(200) DEFAULT '',
  `duration` int(11) unsigned DEFAULT NULL,
  `small_duration` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dna_order
-- ----------------------------
INSERT INTO `dna_order` VALUES ('1', '测试标题', '8DSKQ20180621110432', '1', '浪子蓉城计划', '4', '{\"big\":\"0.4\",\"small\":\"0.4\",\"seek\":\"0.4\"}', '0.00', '10', '8', '0', '20000.00', '4000.00', '500.00', '7800.00', '7800.00', '3900.00', '1', '1', '5', '1', null, '1', '0', '1529550272', '1529655307', '0', '0', '1', '这是个大土豪', null, '15', '6');
INSERT INTO `dna_order` VALUES ('2', '测试标题', '8DSKQ20180621110432', '1', '浪子蓉城计划', '4', '{\"big\":\"0.4\",\"small\":\"0.4\",\"seek\":\"0.4\"}', '120.00', '13', '9', '100', '20000.00', '7000.00', '500.00', '7800.00', '7800.00', '3900.00', '1', '1', '7', '5', '1529724996', '2', '1528860959', '1529655307', '1529918356', '0', '0', '1', '这是个大土豪', 'HN92R20180623113636', '15', '6');
INSERT INTO `dna_order` VALUES ('4', '转课测试', 'HN92R20180623113636', '2', '左阳广州计划', '5', '{\"big\":\"0.4\",\"small\":\"0.4\",\"seek\":\"0.4\"}', '0.00', '11', '8', '2000', '22000.00', '20000.00', '0.00', '8800.00', '8800.00', '4400.00', '1', '1', '8', '4', '1529737879', '2', '1528860959', '1529724996', '1529925507', '0', '0', '1', '', '', '30', '10');

-- ----------------------------
-- Table structure for `dna_order_detail`
-- ----------------------------
DROP TABLE IF EXISTS `dna_order_detail`;
CREATE TABLE `dna_order_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `child_no` varchar(200) DEFAULT NULL,
  `order_no` varchar(200) NOT NULL COMMENT '订单',
  `payment_id` smallint(5) NOT NULL COMMENT '支付方式',
  `payment_type` smallint(5) NOT NULL COMMENT '支付类型',
  `course_id` int(11) NOT NULL COMMENT '课程',
  `belong_teacher_id` int(11) NOT NULL COMMENT '所属导师',
  `belong_sale_id` int(11) NOT NULL COMMENT '所属销售',
  `sale_wechat_no` varchar(200) DEFAULT '' COMMENT '微信号',
  `belong_sale_group_id` int(11) NOT NULL COMMENT '销售组',
  `sale_price` decimal(10,2) DEFAULT '0.00' COMMENT '当前收款金额',
  `student_id` int(11) NOT NULL COMMENT '用户id',
  `position` smallint(1) DEFAULT '0' COMMENT '位置',
  `order_detail_status` smallint(6) DEFAULT '0' COMMENT '订单状态',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` smallint(1) DEFAULT '0' COMMENT '是否删除',
  `status` smallint(1) DEFAULT '0' COMMENT '是否正常',
  `operate_id` int(11) DEFAULT '1' COMMENT '操作者',
  `remark` varchar(255) DEFAULT '""' COMMENT '备注',
  `images` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dna_order_detail
-- ----------------------------
INSERT INTO `dna_order_detail` VALUES ('1', null, '8DSKQ20180621110432', '1', '0', '1', '4', '13', 'xiaos', '9', '3000.00', '1', '1', '2', '1529550272', '1529922631', '0', '0', '1', '转单后第一单', 'order/1529907205548.jpg,order/1529907209263.jpg');
INSERT INTO `dna_order_detail` VALUES ('2', null, '8DSKQ20180621110432', '2', '1', '1', '4', '10', 'two', '9', '1000.00', '1', '0', '2', '1529644558', '1529922763', '0', '0', '1', '这是个大土豪', 'order/1529907594934.jpg');
INSERT INTO `dna_order_detail` VALUES ('3', null, '8DSKQ20180621110432', '3', '1', '1', '4', '13', 'four', '9', '2000.00', '1', '0', '2', '1529656831', '1529922582', '0', '0', '1', '转单后第一单', null);
INSERT INTO `dna_order_detail` VALUES ('5', null, 'HN92R20180623113636', '2', '3', '2', '5', '11', 'two', '8', '20000.00', '1', '1', '2', '1529724996', '1529925507', '0', '0', '1', '', 'order/1529905060786.jpg,order/1529905067179.jpg,order/1529906119518.jpg');
INSERT INTO `dna_order_detail` VALUES ('6', null, '8DSKQ20180621110432', '2', '1', '1', '4', '13', 'fix', '9', '1000.00', '1', '0', '2', '1529907682', '1529922673', '0', '0', '1', '', 'order/1529907680742.jpg');

-- ----------------------------
-- Table structure for `dna_order_log`
-- ----------------------------
DROP TABLE IF EXISTS `dna_order_log`;
CREATE TABLE `dna_order_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `child_no` varchar(200) DEFAULT '',
  `order_no` varchar(255) NOT NULL DEFAULT '' COMMENT '订单号',
  `course_id` varchar(11) DEFAULT '' COMMENT '课程',
  `belong_teacher_id` varchar(11) DEFAULT '' COMMENT '所属导师',
  `belong_sale_id` varchar(11) DEFAULT '' COMMENT '所属销售',
  `belong_sale_group_id` varchar(11) DEFAULT '' COMMENT '销售组',
  `payment_id` varchar(5) DEFAULT '' COMMENT '支付方式',
  `payment_type` varchar(5) DEFAULT '' COMMENT '支付类型',
  `total_price` varchar(10) DEFAULT '' COMMENT '总价',
  `sale_price` varchar(10) DEFAULT '',
  `paid_price` varchar(10) DEFAULT '' COMMENT '已付金额',
  `coupon` varchar(10) DEFAULT '' COMMENT '优惠金额',
  `big_price` varchar(10) DEFAULT '' COMMENT '大课价格',
  `small_price` varchar(10) DEFAULT '' COMMENT '小课价格',
  `seek_price` varchar(10) DEFAULT '' COMMENT '咨询价格',
  `bonus` varchar(11) DEFAULT '' COMMENT '奖金',
  `push_money` varchar(10) DEFAULT '' COMMENT '提成金额',
  `order_status` varchar(6) DEFAULT '' COMMENT '订单状态',
  `service_status` varchar(6) DEFAULT '' COMMENT '是否开课',
  `service_time` varchar(11) DEFAULT '' COMMENT '服务开始时间',
  `pay_status` varchar(6) DEFAULT '' COMMENT '订单状态',
  `pay_end_time` varchar(11) DEFAULT '' COMMENT '尾款时间',
  `sale_wechat_no` varchar(200) DEFAULT '' COMMENT '微信号',
  `order_detail_status` varchar(6) DEFAULT '' COMMENT '订单状态',
  `wechat_name` varchar(255) DEFAULT '' COMMENT '微信名',
  `wechat_no` varchar(200) DEFAULT '' COMMENT '学员微信号',
  `income` varchar(10) DEFAULT '' COMMENT '月收入',
  `age` varchar(3) DEFAULT '' COMMENT '年龄',
  `major` varchar(200) DEFAULT '' COMMENT '职业',
  `address` varchar(255) DEFAULT '' COMMENT '地址',
  `add_time` varchar(11) DEFAULT '' COMMENT '添加好友时间',
  `inside` varchar(3) DEFAULT '' COMMENT '内部学员',
  `compact` varchar(3) DEFAULT '' COMMENT '合同',
  `actions` varchar(200) DEFAULT '0' COMMENT '行为',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` smallint(1) DEFAULT '0' COMMENT '是否删除',
  `status` smallint(1) DEFAULT '0' COMMENT '是否正常',
  `operate_id` int(11) DEFAULT '1' COMMENT '操作者',
  `source_id` varchar(11) DEFAULT '-' COMMENT '来源',
  `remark` varchar(255) DEFAULT '',
  `mobile` varchar(11) DEFAULT '',
  `images` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dna_order_log
-- ----------------------------
INSERT INTO `dna_order_log` VALUES ('1', '', '8DSKQ20180621110432', '1', '4', '10', '8', '1', '0', '20000.00', '1000.00', '1000.00', '500.00', '7800.00', '7800.00', '3900.00', null, null, '3', '1', null, '1', null, 'first_no', '1', null, 'xiaming', null, null, 'IT', '', '0', '0', '0', 'create', '1529550272', '1529550272', '0', '0', '1', '1', '简单备注', '13408187117', null);
INSERT INTO `dna_order_log` VALUES ('17', '', '8DSKQ20180621110432', '-', '-', '-', '-', '-', '-', '-', '2000.00', '2000.00', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '1530258546', '-', '-', 'update', '1529567347', '1529567347', '0', '0', '1', '-', '-', '-', null);
INSERT INTO `dna_order_log` VALUES ('18', '', '8DSKQ20180621110432', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 'update', '1529567377', '1529567377', '0', '0', '1', '-', '-', '-', null);
INSERT INTO `dna_order_log` VALUES ('19', '', '8DSKQ20180621110432', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '1529567399', '-', '-', 'update', '1529567400', '1529567400', '0', '0', '1', '-', '-', '-', null);
INSERT INTO `dna_order_log` VALUES ('22', '', '8DSKQ20180621110432', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 'xiaos', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 'update', '1529568264', '1529568264', '0', '0', '1', '-', '-', '-', null);
INSERT INTO `dna_order_log` VALUES ('21', '', '8DSKQ20180621110432', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 'update', '1529567674', '1529567674', '0', '0', '1', '-', '土豪一枚1', '-', null);
INSERT INTO `dna_order_log` VALUES ('23', '', '8DSKQ20180621110432', '1', '4', '10', '8', '2', '1', '', '2000', '4000', '', '', '', '', '', '', '', '1', null, '1', null, 'two', '1', '', '', '', '', '', '', '', '', '', 'create', '1529644558', '1529644558', '0', '0', '1', '-', '', '', null);
INSERT INTO `dna_order_log` VALUES ('24', '', '8DSKQ20180621110432', '', '', '-', '', '-', '-', '', '-', '-', '', '', '', '', '', '', '', '-', '-', '-', '-', '-', '', '', '', '', '', '', '', '', '', '', 'update', '1529647659', '1529647659', '0', '0', '1', '-', '这是个大土豪', '', null);
INSERT INTO `dna_order_log` VALUES ('25', '', '8DSKQ20180621110432', '', '', '', '', '', '', '', '', '2000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'delete', '1529649068', '1529649068', '0', '0', '1', '-', '', '', null);
INSERT INTO `dna_order_log` VALUES ('27', '', '8DSKQ20180621110432', '', '', '-', '', '-', '-', '', '1000.00', '3000', '', '', '', '', '', '', '', '-', '-', '-', '-', '-', '', '', '', '', '', '', '', '', '', '', 'update', '1529649720', '1529649720', '0', '0', '1', '-', '-', '', null);
INSERT INTO `dna_order_log` VALUES ('28', '', '8DSKQ20180621110432', '-', '-', '-', '-', '-', '-', '-', '3000.00', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '', '-', '-', 'update', '1529654692', '1529654692', '0', '0', '1', '-', '-', '-', null);
INSERT INTO `dna_order_log` VALUES ('29', '', '8DSKQ20180621110432', '-', '-', '-', '-', '-', '-', '-', '-', '4000', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 'update', '1529655144', '1529655144', '0', '0', '1', '-', '-', '-', null);
INSERT INTO `dna_order_log` VALUES ('30', '', '8DSKQ20180621110432', '', '', '13', '9', '', '', '', '', '', '', '', '', '', '', '', '6', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'return', '1529655307', '1529655307', '0', '0', '1', '-', '', '', null);
INSERT INTO `dna_order_log` VALUES ('31', '', '8DSKQ20180621110432', '1', '4', '13', '9', '3', '1', '', '2000', '6000', '', '', '', '', '', '', '', '1', null, '1', '0', 'four', '1', '', '', '', '', '', '', '', '', '', 'create', '1529656832', '1529656832', '0', '0', '1', '-', '转单后第一单', '', null);
INSERT INTO `dna_order_log` VALUES ('32', '', '8DSKQ20180621110432', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '1528277063', '-', '-', 'update', '1529660339', '1529660339', '0', '0', '1', '-', '-', '-', null);
INSERT INTO `dna_order_log` VALUES ('33', '', '8DSKQ20180621110432', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '1528364838', '-', '-', 'update', '1529660839', '1529660839', '0', '0', '1', '-', '-', '-', null);
INSERT INTO `dna_order_log` VALUES ('34', '', '8DSKQ20180621110432', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '1528365166', '-', '-', 'update', '1529661169', '1529661169', '0', '0', '1', '-', '-', '-', null);
INSERT INTO `dna_order_log` VALUES ('35', '', 'YTZ7420180623112012', '2', '5', '11', '8', '1', '3', '22000', '20000', '20000', '0', '8800', '8800', '4400', null, null, '8', '1', null, '2', '1529032766', 'two', '1', '小米', 'xiaming', '5000.00', '22', 'IT1', '区区小事', '1528365166', '0', '0', 'create', '1529724012', '1529724012', '0', '0', '1', '1', null, '13408187117', null);
INSERT INTO `dna_order_log` VALUES ('36', '', 'HN92R20180623113636', '2', '5', '11', '8', '2', '3', '22000', '20000', '20000', '0', '8800', '8800', '4400', null, null, '8', '1', null, '2', '1528860959', 'two', '1', '小米', 'xiaming', '5000.00', '22', 'IT1', '区区小事', '1528365166', '0', '0', 'create', '1529724996', '1529724996', '0', '0', '1', '1', null, '13408187117', null);
INSERT INTO `dna_order_log` VALUES ('37', 'HN92R20180623113636', '8DSKQ20180621110432', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '5', '1529724996', '', '', '', '', '', '', '', '', '', '', '', '', '', 'redirect', '1529724996', '1529724996', '0', '0', '1', '-', '', '', null);
INSERT INTO `dna_order_log` VALUES ('38', '', 'HN92R20180623113636', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2', '1529737327', '', '', '', '', '', '', '', '', '', '', '', '', '', 'play', '1529737327', '1529737327', '0', '0', '1', '-', '', '', null);
INSERT INTO `dna_order_log` VALUES ('39', '', 'HN92R20180623113636', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '3', '1529737734', '', '', '', '', '', '', '', '', '', '', '', '', '', 'pause', '1529737734', '1529737734', '0', '0', '1', '-', '', '', null);
INSERT INTO `dna_order_log` VALUES ('40', '', 'HN92R20180623113636', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '4', '1529737806', '', '', '', '', '', '', '', '', '', '', '', '', '', 'replay', '1529737806', '1529737806', '0', '0', '1', '-', '', '', null);
INSERT INTO `dna_order_log` VALUES ('41', '', 'HN92R20180623113636', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '4', '1529737879', '', '', '', '', '', '', '', '', '', '', '', '', '', 'replay', '1529737879', '1529737879', '0', '0', '1', '-', '', '', null);
INSERT INTO `dna_order_log` VALUES ('42', '', 'HN92R20180623113636', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 'update', '1529905072', '1529905072', '0', '0', '1', '-', '-', '-', 'order/1529905060786.jpg,order/1529905067179.jpg');
INSERT INTO `dna_order_log` VALUES ('43', '', 'HN92R20180623113636', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 'update', '1529906121', '1529906121', '0', '0', '1', '-', '-', '-', 'order/1529905060786.jpg,order/1529905067179.jpg,order/1529906119518.jpg');
INSERT INTO `dna_order_log` VALUES ('44', '', '8DSKQ20180621110432', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 'update', '1529907211', '1529907211', '0', '0', '1', '-', '-', '-', 'order/1529907205548.jpg,order/1529907209263.jpg');
INSERT INTO `dna_order_log` VALUES ('45', '', '8DSKQ20180621110432', '', '', '-', '', '-', '-', '', '-', '-', '', '', '', '', '', '', '', '-', '-', '-', '-', '-', '', '', '', '', '', '', '', '', '', '', 'update', '1529907599', '1529907599', '0', '0', '1', '-', '-', '', 'order/1529907594934.jpg');
INSERT INTO `dna_order_log` VALUES ('46', '', '8DSKQ20180621110432', '1', '4', '13', '9', '2', '1', '', '1000', '7000', '', '', '', '', '', '', '', '5', '1529724996', '1', '0', 'fix', '1', '', '', '', '', '', '', '', '', '', 'create', '1529907682', '1529907682', '0', '0', '1', '-', '', '', 'order/1529907680742.jpg');
INSERT INTO `dna_order_log` VALUES ('47', '', '8DSKQ20180621110432', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '100', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'push', '1529918238', '1529918238', '0', '0', '1', '-', '', '', null);
INSERT INTO `dna_order_log` VALUES ('48', '', '8DSKQ20180621110432', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'push', '1529918280', '1529918280', '0', '0', '1', '-', '', '', null);
INSERT INTO `dna_order_log` VALUES ('49', '', '8DSKQ20180621110432', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '120', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'push', '1529918356', '1529918356', '0', '0', '1', '-', '', '', null);
INSERT INTO `dna_order_log` VALUES ('50', '', '8DSKQ20180621110432', '1', '', '13', '9', '2', '1', '', '1000.00', '', '', '', '', '', '', '', '', '', '', '', '', 'fix', '2', '', '', '', '', '', '', '', '', '', 'pass', '1529922210', '1529922210', '0', '0', '1', '-', '审核订单', '', null);
INSERT INTO `dna_order_log` VALUES ('51', '', '8DSKQ20180621110432', '1', '', '13', '9', '1', '0', '', '3000.00', '', '', '', '', '', '', '', '', '', '', '', '', 'xiaos', '2', '', '', '', '', '', '', '', '', '', 'pass', '1529922264', '1529922264', '0', '0', '1', '-', '审核订单', '', null);
INSERT INTO `dna_order_log` VALUES ('52', '', '8DSKQ20180621110432', '1', '', '10', '9', '2', '1', '', '1000.00', '', '', '', '', '', '', '', '', '', '', '', '', 'two', '2', '', '', '', '', '', '', '', '', '', 'pass', '1529922371', '1529922371', '0', '0', '1', '-', '审核订单', '', null);
INSERT INTO `dna_order_log` VALUES ('53', '', '8DSKQ20180621110432', '1', '', '13', '9', '3', '1', '', '2000.00', '', '', '', '', '', '', '', '', '', '', '', '', 'four', '2', '', '', '', '', '', '', '', '', '', 'pass', '1529922582', '1529922582', '0', '0', '1', '-', '审核订单', '', null);
INSERT INTO `dna_order_log` VALUES ('54', '', '8DSKQ20180621110432', '1', '', '13', '9', '1', '0', '', '3000.00', '', '', '', '', '', '', '', '', '', '', '', '', 'xiaos', '2', '', '', '', '', '', '', '', '', '', 'pass', '1529922631', '1529922631', '0', '0', '1', '-', '审核订单', '', null);
INSERT INTO `dna_order_log` VALUES ('55', '', '8DSKQ20180621110432', '1', '', '13', '9', '2', '1', '', '1000.00', '', '', '', '', '', '', '', '', '', '', '', '', 'fix', '2', '', '', '', '', '', '', '', '', '', 'pass', '1529922673', '1529922673', '0', '0', '1', '-', '审核订单', '', null);
INSERT INTO `dna_order_log` VALUES ('56', '', '8DSKQ20180621110432', '1', '', '10', '9', '2', '1', '', '1000.00', '', '', '', '', '', '', '', '', '', '', '', '', 'two', '2', '', '', '', '', '', '', '', '', '', 'pass', '1529922763', '1529922763', '0', '0', '1', '-', '审核订单', '', null);
INSERT INTO `dna_order_log` VALUES ('57', '', 'HN92R20180623113636', '2', '', '11', '8', '2', '3', '', '20000.00', '', '', '', '', '', 'undefined', '', '', '', '', '', '', 'two', '2', '', '', '', '', '', '', '', '', '', 'bonus', '1529924449', '1529924449', '0', '0', '1', '-', '审核订单', '', null);
INSERT INTO `dna_order_log` VALUES ('58', '', 'HN92R20180623113636', '2', '', '11', '8', '2', '3', '', '20000.00', '', '', '', '', '', '200', '', '', '', '', '', '', 'two', '2', '', '', '', '', '', '', '', '', '', 'bonus', '1529924717', '1529924717', '0', '0', '1', '-', '审核并奖励订单', '', null);
INSERT INTO `dna_order_log` VALUES ('59', '', 'HN92R20180623113636', '2', '', '11', '8', '2', '3', '', '20000.00', '', '', '', '', '', '2000', '', '', '', '', '', '', 'two', '2', '', '', '', '', '', '', '', '', '', 'bonus', '1529925507', '1529925507', '0', '0', '1', '-', '审核并奖励订单', '', null);

-- ----------------------------
-- Table structure for `dna_payment`
-- ----------------------------
DROP TABLE IF EXISTS `dna_payment`;
CREATE TABLE `dna_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '支付方式',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` smallint(1) DEFAULT '0' COMMENT '是否删除',
  `status` smallint(1) DEFAULT '0' COMMENT '是否正常',
  `operate_id` int(11) DEFAULT '1' COMMENT '操作者',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dna_payment
-- ----------------------------
INSERT INTO `dna_payment` VALUES ('1', '微信', '1529391053', '1529391053', '0', '0', '1');
INSERT INTO `dna_payment` VALUES ('2', '支付宝', '1529391407', '1529391407', '0', '0', '1');
INSERT INTO `dna_payment` VALUES ('3', '网银', '1529392692', '1529392953', '0', '0', '1');

-- ----------------------------
-- Table structure for `dna_role`
-- ----------------------------
DROP TABLE IF EXISTS `dna_role`;
CREATE TABLE `dna_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL COMMENT '名称',
  `income_status` smallint(1) DEFAULT '1' COMMENT '收益状态',
  `income` decimal(10,2) DEFAULT NULL COMMENT '底薪',
  `income_ratio` varchar(2000) DEFAULT NULL COMMENT '收益比',
  `parent_id` int(11) DEFAULT '0' COMMENT '父级',
  `sort` int(11) DEFAULT '1' COMMENT '排序',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` smallint(1) DEFAULT '0' COMMENT '是否删除',
  `status` smallint(1) DEFAULT '0' COMMENT '是否正常',
  `operate_id` int(11) DEFAULT '1' COMMENT '操作者',
  `auth` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dna_role
-- ----------------------------
INSERT INTO `dna_role` VALUES ('1', '超级管理员', '1', '20000.00', null, '0', '1', '1525954616', '1525954616', '0', '0', '1', null);
INSERT INTO `dna_role` VALUES ('2', '管理员', '1', '1800.00', null, '1', '1', '1528884843', '1530183202', '0', '0', '1', '2,5,3,7,8,9,10,11,12,13,30,32,33,34,31,35,36,37,6,14,18,19,20,67,15,21,22,23,16,24,25,26,17,27,28,29,38,39,40,41,42,48,49,50,51,43,44,45,46,47,52,68,69,70,53,54,55,56,57,58,63,64,65,66');
INSERT INTO `dna_role` VALUES ('3', '人事', '1', '1000.00', null, '2', '1', '1528947158', '1530183318', '0', '0', '1', '63,64,65,66');
INSERT INTO `dna_role` VALUES ('4', '财务', '1', '1000.00', null, '2', '1', '1528947171', '1528947171', '0', '0', '1', null);
INSERT INTO `dna_role` VALUES ('5', '导师', '2', '0.00', '{\"big\":\"0.4\",\"small\":\"0.4\",\"seek\":\"0.4\"}', '1', '1', '1528947587', '1530245264', '0', '0', '1', '38,48,49,50,51,39,40,41,42,63,64,65,66');
INSERT INTO `dna_role` VALUES ('6', '助教', '1', '0.00', null, '5', '1', '1528947636', '1530176867', '0', '0', '1', '6,14,18,19');
INSERT INTO `dna_role` VALUES ('7', '销售总监', '1', '0.00', null, '1', '1', '1528947708', '1528947708', '0', '0', '1', null);
INSERT INTO `dna_role` VALUES ('8', '销售主管', '1', '0.00', '', '7', '1', '1528947740', '1530264180', '0', '0', '1', '63,64,65');
INSERT INTO `dna_role` VALUES ('9', '销售员', '1', '0.00', null, '8', '1', '1528947761', '1528947761', '0', '0', '1', null);
INSERT INTO `dna_role` VALUES ('10', '售后经理', '1', '0.00', null, '1', '1', '1528955570', '1528955570', '0', '0', '1', null);
INSERT INTO `dna_role` VALUES ('11', '售后主管', '1', '0.00', null, '10', '1', '1528955745', '1528957461', '0', '0', '1', null);

-- ----------------------------
-- Table structure for `dna_source`
-- ----------------------------
DROP TABLE IF EXISTS `dna_source`;
CREATE TABLE `dna_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '来源',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` smallint(1) DEFAULT '0' COMMENT '是否删除',
  `status` smallint(1) DEFAULT '0' COMMENT '是否正常',
  `operate_id` int(11) DEFAULT '1' COMMENT '操作者',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dna_source
-- ----------------------------
INSERT INTO `dna_source` VALUES ('1', '百度', '1529394303', '1529394303', '0', '0', '1');
INSERT INTO `dna_source` VALUES ('2', '神马', '1529394330', '1529394344', '0', '0', '1');

-- ----------------------------
-- Table structure for `dna_student`
-- ----------------------------
DROP TABLE IF EXISTS `dna_student`;
CREATE TABLE `dna_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wechat_name` varchar(255) DEFAULT '' COMMENT '微信名',
  `wechat_no` varchar(200) NOT NULL COMMENT '学员微信号',
  `income` decimal(10,2) DEFAULT '0.00' COMMENT '月收入',
  `age` tinyint(3) DEFAULT '0' COMMENT '年龄',
  `major` varchar(200) DEFAULT '' COMMENT '职业',
  `address` varchar(255) DEFAULT '' COMMENT '地址',
  `add_time` int(11) DEFAULT '0' COMMENT '添加好友时间',
  `inside` tinyint(3) DEFAULT '0' COMMENT '内部学员',
  `compact` tinyint(3) DEFAULT '0' COMMENT '合同',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` smallint(1) DEFAULT '0' COMMENT '是否删除',
  `status` smallint(1) DEFAULT '0' COMMENT '是否正常',
  `operate_id` int(11) DEFAULT '1' COMMENT '操作者',
  `mobile` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dna_student
-- ----------------------------
INSERT INTO `dna_student` VALUES ('1', '小米', 'xiaming', '5000.00', '22', 'IT1', '区区小事', '1528365166', '0', '0', '1529550272', '1529907211', '0', '0', '1', '13408187117');

-- ----------------------------
-- Table structure for `dna_user`
-- ----------------------------
DROP TABLE IF EXISTS `dna_user`;
CREATE TABLE `dna_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of dna_user
-- ----------------------------
