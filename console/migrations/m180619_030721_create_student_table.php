<?php

use yii\db\Migration;

/**
 * Handles the creation of table `student`.
 */
class m180619_030721_create_student_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%student}}', [
            'id' => $this->primaryKey(),
            'wechat_name'=>$this->string()->defaultValue("")->comment("微信名"),
            'wechat_no'=>$this->string(200)->notNull()->comment("学员微信号"),
            'income'=>$this->decimal(10,2)->defaultValue(0)->comment("月收入"),
            'age'=>$this->tinyInteger()->defaultValue(0)->comment("年龄"),
            'major'=>$this->string(200)->defaultValue("")->comment("职业"),
            'address'=>$this->string(255)->defaultValue("")->comment("地址"),
            'add_time'=>$this->integer()->defaultValue(0)->comment("添加好友时间"),
            'inside'=>$this->tinyInteger()->defaultValue(0)->comment("内部学员"),
            'compact'=>$this->tinyInteger()->defaultValue(0)->comment("合同"),
            'mobile'=>$this->string(11)->defaultValue("")->comment("电话"),
            'create_time'=>$this->integer(11)->comment("创建时间"),
            'update_time'=>$this->integer(11)->comment("更新时间"),
            'is_deleted'=>$this->smallInteger(1)->defaultValue(0)->comment("是否删除"),
            'status'=>$this->smallInteger(1)->defaultValue(0)->comment("是否正常"),
            'operate_id'=>$this->integer(11)->defaultValue(1)->comment("操作者"),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%student}}');
    }
}
