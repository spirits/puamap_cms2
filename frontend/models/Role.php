<?php
namespace frontend\models;
use common\models\ParentActiveRecord;
use common\utils\RedisUtils;

class Role extends \common\models\Role{

    /**
     * @return array
     * 显示列
     */
    public function getFields(){
        return [
            [
                "label"=>$this->getAttributeLabel("id"),
                'field'=>"id",
                'type'=>"String"
            ],
            [
                "label"=>$this->getAttributeLabel("name"),
                'field'=>"name",
                'type'=>"String"
            ],
            [
                "label"=>$this->getAttributeLabel("income"),
                'field'=>"income",
                'type'=>"String"
            ],
            [
                "label"=>$this->getAttributeLabel("income_status"),
                'field'=>"income_status",
                'type'=>"Switch"
            ],
            [
                "label"=>$this->getAttributeLabel("income_ratio"),
                'field'=>"income_ratio",
                'type'=>"String"
            ],
            [
                "label"=>$this->getAttributeLabel("status"),
                'field'=>"status",
                'type'=>"Switch"
            ],
            [
                "label"=>$this->getAttributeLabel("create_time"),
                'field'=>"create_time",
                'type'=>"String"
            ],
            [
                "label"=>$this->getAttributeLabel("update_time"),
                'field'=>"update_time",
                'type'=>"String"
            ],
            [
                "label"=>$this->getAttributeLabel("operate_id"),
                'field'=>"operate_id",
                'type'=>"String"
            ]
        ];
    }

    /**
     * @return array
     * 表单数据
     */
    public function getFormFields(){
        return [
                [
                    "label"=>$this->getAttrLabel('parent_id'),
				    'name'=>"parent_id",
				    'type'=>"ChosenSelect",
				    'value'=>0,
					"rules"=>['required'=>true]
				],
				[
                    "label"=>$this->getAttrLabel('name'),
                    'name'=>"name",
                    'type'=>"String",
                    'value'=>"",
                    "rules"=>['required'=>true]
                ],
                [
                    "label"=>$this->getAttrLabel('income'),
                    'name'=>"income",
                    'type'=>"String",
                    'value'=>"0",
                    "rules"=>['required'=>true,"number"=>true]
                ],
                [
                    "label"=>$this->getAttrLabel('income_status'),
                    'name'=>"income_status",
                    'type'=>"Switch",
                    'value'=>1,
                    'options'=>[['label'=>"固定",'value'=>1],['label'=>"收益",'value'=>2]]
                ],
                [
                    "label"=>$this->getAttrLabel('income_ratio'),
                    'name'=>"income_ratio",
                    'type'=>"Ratio",
                    'value'=>['big'=>"0.4",'small'=>"0.4",'seek'=>"0.4"],
                    'options'=>[['label'=>"大课:",'name'=>"big",'value'=>"0.4"],['label'=>"小课:",'name'=>"small",'value'=>"0.4"],['label'=>"咨询:",'name'=>"seek",'value'=>"0.4"]]
                ],
                [
                    "label"=>$this->getAttrLabel('status'),
                    'name'=>"status",
                    'type'=>"Switch",
                    'value'=>0,
                    'options'=>[['label'=>"开启",'value'=>0],['label'=>"禁用",'value'=>1]]
                ]
        ];
    }


    public function getALL(){
      return  self::find()->where($this->baseWhere())->orderBy(['parent_id'=>SORT_ASC])->asArray()->all();
    }

    /**
     * @return array
     * 获取列表
     */
    public function getTreeData(){
       $data =  self::find()->select(['id','name','parent_id'])->where($this->baseWhere())
           ->andWhere(['status'=>ParentActiveRecord::TRUE_STATUS])
           ->andWhere(['<>','id',self::ASSISTANT_NO])
           ->andWhere(['<>','id',self::SALE_NO])
           ->orderBy(['parent_id'=>SORT_ASC])->asArray()->all();
       return $this->formTreeData($data);
    }

    public function formTreeData($items,$parent_id=0,$index=0){
        $data= [];
        foreach ($items as $item) {
            if($item['parent_id'] == $parent_id){
                $item['name'] = str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;",$index).$item['name'];
                $data[] = $item;
                $num = $index + 1;
                $temp = $this->formTreeData($items,$item['id'],$num);
                if($temp){
                    foreach ($temp as $t){
                        $data[] = $t;
                    }
                }
            }
        }
        return $data;
    }

    public static function getBaseIncome($id){
        $model = static::findOne($id);
        return $model->income;
    }

    public function afterSave($insert, $changedAttributes)
    {
        $key = "role_".$this->id;
        \Yii::$app->redis->set($key,$this->auth);
        RedisUtils::regexDelete("node_*_list_".$this->id);
        parent::afterSave($insert, $changedAttributes);
    }

}