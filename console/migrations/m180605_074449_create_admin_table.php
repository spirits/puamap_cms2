<?php

use yii\db\Migration;

/**
 * Handles the creation of table `admin`.
 */
class m180605_074449_create_admin_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%admin}}', [
            'id' => $this->primaryKey(),
            "username"=>$this->string(200)->notNull()->comment("账户"),
            'nickname'=>$this->string(200)->comment("昵称"),
            'password'=>$this->string(200)->notNull()->comment("密码"),
            'role_id'=>$this->smallInteger()->defaultValue(0)->comment("类型"),
            'leader_id'=>$this->integer(11)->defaultValue(0)->comment("领导"),
            'create_time'=>$this->integer(11)->comment("创建"),
            'update_time'=>$this->integer(11)->comment("更新"),
            'access_token'=>$this->string(200)->comment("token"),
            'login_time' =>$this->integer(11)->comment("本次登录"),
            'is_deleted'=>$this->smallInteger(1)->defaultValue(0)->comment("删除"),
            'status'=>$this->smallInteger(1)->defaultValue(0)->comment("状态"),
            'operate_id'=>$this->integer(11)->defaultValue(0)->comment("操作者")
        ]);
        $sql ="insert dna_admin(`username`,`nickname`,`password`,`role_id`,`create_time`,`update_time`) values('admin','系统管理员','$2y$13$8DM.vV0QzR.jd.GQKsoeFOnoWVohf6gqiQnveGitlmB9vISjyPnrK',1,".time().",".time().")";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%admin}}');
    }
}
