<?php
namespace frontend\exceptions;
use yii\base\Exception;

class NotPermissionException extends Exception{
    public function __construct()
    {
        parent::__construct("权限不允许", 401, null);
    }
}