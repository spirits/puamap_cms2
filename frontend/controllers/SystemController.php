<?php
namespace frontend\controllers;
use frontend\models\Node;

class SystemController extends BaseController{
    public function actionIndex(){
        $node = new Node();
        $node_item = \Yii::$app->request->get('node');
        $this->in['status'] = true;
        $this->in['data'] = $node->getListByRoute($node_item);
        return $this->in;
    }


    public function actionMain(){
        return $this->render($this->action->id);
    }
}