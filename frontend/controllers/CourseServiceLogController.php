<?php
namespace frontend\controllers;
use frontend\models\CourseServiceLog;

class CourseServiceLogController extends BaseController{
    public function actionIndex(){
        $id = \Yii::$app->request->get("id",0);
        $model = new CourseServiceLog();
        $data['fields'] = $model->getFields();
        $pageData = $model->getPage(['service_id'=>$id]);
        $data['items'] = $pageData['list'];
        $data['pages'] = $model->getPageInfo(['service_id'=>$id]);
        $data['tableChose'] = $model->getTableChoseSelect();
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }

    public function actionPage(){
        $page = \Yii::$app->request->post('page',1);
        $params = \Yii::$app->request->post();
        $id = \Yii::$app->request->get("id",0);
        $model = new CourseServiceLog();
        $data = $model->getPage(['service_id'=>$id],$page,$params);
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }
}