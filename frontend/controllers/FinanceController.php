<?php
namespace frontend\controllers;
use frontend\models\Order;
use frontend\models\OrderDetail;
use frontend\models\OrderLog;

class FinanceController extends BaseController{
    public function actionIndex(){
        $model = new OrderDetail();
        $data['fields'] = $model->getFields();
        $data['formFields'] = $model->getFormFields2();
        $data['controller'] = $model->getAuthController();
        $data['tableChose'] = $model->getTableChoseSelect();
        $data['formChose']= [];
        $pageData = $model->getPage(['order_detail_status'=>OrderDetail::ORDER_DETAIL_STATUS_1]);
        $data['items'] = $pageData['list'];
        $data['pages'] = $model->getPageInfo(['order_detail_status'=>OrderDetail::ORDER_DETAIL_STATUS_1]);
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }

    public function actionPass(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $model = OrderDetail::findOne($post_data['id']);
            if($model){
                if($model->order_detail_status == OrderDetail::ORDER_DETAIL_STATUS_1){
                    $model->order_detail_status = OrderDetail::ORDER_DETAIL_STATUS_2;
                    $order_log_model= new OrderLog();
                    $order_log_model->order_no = $model->order_no;
                    $order_log_model->actions=OrderLog::PASS;
                    $order_log_model->remark ="审核订单";
                    $order_log_model->sale_price = $model->sale_price;
                    $order_log_model->payment_id = $model->payment_id;
                    $order_log_model->payment_type = $model->payment_type;
                    $order_log_model->course_id = $model->course_id;
                    $order_log_model->belong_sale_group_id = $model->belong_sale_group_id;
                    $order_log_model->belong_sale_id = $model->belong_sale_id;
                    $order_log_model->sale_wechat_no = $model->sale_wechat_no;
                    $order_log_model->order_detail_status = $model->order_detail_status;
                    if($model->save()  && $order_log_model->save()){
                        $this->in['status'] = true;
                    }else{
                        $this->in['message']= "操作失败！";
                    }
                }else{
                    $this->in['message']= "该数据已经审核过了";
                }
            }else{
                $this->in['message']= "数据不存在！";
            }
        }
        return $this->in;
    }

    /**
     * @return array
     * 奖励
     */
    public function actionBonus(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $model = OrderDetail::findOne($post_data['id']);
            if($model){
                if($model->order_detail_status == OrderDetail::ORDER_DETAIL_STATUS_1){
                    $order_model = (new Order())->getOrderInfo($model->order_no);
                    if($order_model){
                        $order_model->bonus = $post_data['bonus'];
                        $model->order_detail_status = OrderDetail::ORDER_DETAIL_STATUS_2;
                        $order_log_model= new OrderLog();
                        $order_log_model->order_no = $model->order_no;
                        $order_log_model->actions=OrderLog::BONUS;
                        $order_log_model->remark ="审核并奖励订单";
                        $order_log_model->sale_price = $model->sale_price;
                        $order_log_model->payment_id = $model->payment_id;
                        $order_log_model->payment_type = $model->payment_type;
                        $order_log_model->course_id = $model->course_id;
                        $order_log_model->belong_sale_group_id = $model->belong_sale_group_id;
                        $order_log_model->belong_sale_id = $model->belong_sale_id;
                        $order_log_model->sale_wechat_no = $model->sale_wechat_no;
                        $order_log_model->order_detail_status = $model->order_detail_status;
                        $order_log_model->bonus = $order_model->bonus;
                        if($order_model->save() && $model->save()  && $order_log_model->save()){
                            $this->in['status'] = true;
                        }else{
                            $this->in['message']= "操作失败！";
                        }
                    }else{
                        $this->in['message']= "当前订单不存在";
                    }

                }else{
                    $this->in['message']= "该数据已经审核过了";
                }
            }else{
                $this->in['message']= "数据不存在！";
            }
        }
        return $this->in;
    }
}