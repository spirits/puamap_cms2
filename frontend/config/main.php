<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute'=>"index/index",
    'language' => 'zh-CN',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'cookieValidationKey' => '_DNA_ZG5hQmFja2VuZAbeta5',
        ],
        'response'=>[
            'class'=>'yii\web\Response',
            'format'=>\yii\web\Response::FORMAT_JSON,
            "on beforeSend"=>function($event){
                $response = $event->sender;
                if ($response->data !== null) {
                    $response->data = [
                        'statusCode' => $response->statusCode,
                        'isSuccessful' => $response->isSuccessful,
                        'timestamp'=>time(),
                        'data' => $response->data,
                    ];
                    $response->statusCode = 200;
                }
            }
        ],
        'user' => [
            'identityClass' => 'common\models\Admin',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'puamap-cms',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'i18n'=>[
            'translations' => [
                'common'=>[
                    'class' => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'common' => 'common.php',
                    ],
                ]
            ],
        ],
        'urlManager'=>[
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules'=>[
                'login/sign-in'=>"/login/sign-in",
                'node/index'=>"/node/index",
                'node/create'=>"/node/create",
                'node/update'=>"/node/update",
                "<controller:\w+>/<action:\w+>"=>"/<controller>/<action>"
            ]
        ],
    ],
    'params' => $params,
];
