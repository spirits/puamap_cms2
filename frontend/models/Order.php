<?php
namespace frontend\models;
use common\utils\RedisUtils;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class Order extends \common\models\Order{
    public function getFields(){
        $order_detail_model = new OrderDetail();
        $student_model = new Student();
        return [
            [
                "label"=>$this->getAttributeLabel("id"),
                'field'=>"id",
                'type'=>"String",
                'search'=>0,
                'style'=>"width:80px;"
            ],
            [
                "label"=>$this->getAttributeLabel("title"),
                'field'=>"title",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$this->getAttributeLabel("order_no"),
                'field'=>"order_no",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$this->getAttributeLabel("child_no"),
                'field'=>"child_no",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$this->getAttributeLabel("course_type"),
                'field'=>"course_type",
                'type'=>"ChosenSelect",
                'search'=>1,
                'style'=>"width:240px;"
            ],
            [
                "label"=>$this->getAttributeLabel("course_id"),
                'field'=>"course_id",
                'type'=>"ChosenSelect",
                'search'=>1,
                'style'=>"width:240px;"
            ],
            [
                "label"=>$this->getAttributeLabel("duration"),
                'field'=>"duration",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("small_duration"),
                'field'=>"small_duration",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("belong_teacher_id"),
                'field'=>"belong_teacher_id",
                'type'=>"ChosenSelect",
                'search'=>1,
                'style'=>"width:240px;"
            ],
            [
                "label"=>$this->getAttributeLabel("belong_sale_group_id"),
                'field'=>"belong_sale_group_id",
                'type'=>"ChosenSelect",
                'search'=>1,
                'style'=>"width:240px;"
            ],
            [
                "label"=>$this->getAttributeLabel("belong_sale_id"),
                'field'=>"belong_sale_id",
                'type'=>"ChosenSelect",
                'search'=>1,
                'style'=>"width:240px;"
            ],
            [
                "label"=>$order_detail_model->getAttributeLabel("sale_wechat_no"),
                'field'=>"sale_wechat_no",
                'type'=>"String",
                'search'=>1,
                'style'=>"width:180px;"
            ],
            [
                "label"=>$this->getAttributeLabel("source_id"),
                'field'=>"source_id",
                'type'=>"ChosenSelect",
                'search'=>1,
                'style'=>"width:240px;"
            ],

            [
                "label"=>$this->getAttributeLabel("total_price"),
                'field'=>"total_price",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("paid_price"),
                'field'=>"paid_price",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("coupon"),
                'field'=>"coupon",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>"欠款",
                'field'=>"debt",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("big_price"),
                'field'=>"big_price",
                'type'=>"String",
                'search'=>0
            ],[
                "label"=>$this->getAttributeLabel("small_price"),
                'field'=>"small_price",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("seek_price"),
                'field'=>"seek_price",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("push_money"),
                'field'=>"push_money",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("bonus"),
                'field'=>"bonus",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$order_detail_model->getAttributeLabel("payment_id"),
                'field'=>"payment_id",
                'type'=>"ChosenSelect",
                'search'=>1,
                'style'=>"width:240px;"
            ],
            [
                "label"=>$order_detail_model->getAttributeLabel("payment_type"),
                'field'=>"payment_type",
                'type'=>"ChosenSelect",
                'search'=>1
            ],
            [
                "label"=>$student_model->getAttributeLabel("wechat_name"),
                'field'=>"wechat_name",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$student_model->getAttributeLabel("wechat_no"),
                'field'=>"wechat_no",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$student_model->getAttributeLabel("mobile"),
                'field'=>"mobile",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$student_model->getAttributeLabel("income"),
                'field'=>"income",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$student_model->getAttributeLabel("age"),
                'field'=>"age",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$student_model->getAttributeLabel("major"),
                'field'=>"major",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$student_model->getAttributeLabel("address"),
                'field'=>"address",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$student_model->getAttributeLabel("add_time"),
                'field'=>"add_time",
                'type'=>"String",
                'search'=>0,
                'style'=>'width:200px;display: inline-block'
            ],
            [
                "label"=>$student_model->getAttributeLabel("inside"),
                'field'=>"inside",
                'type'=>"Switch",
                'search'=>1
            ],
            [
                "label"=>$student_model->getAttributeLabel("compact"),
                'field'=>"compact",
                'type'=>"Switch",
                'search'=>1
            ],
            [
                "label"=>$order_detail_model->getAttributeLabel("images"),
                'field'=>"images",
                'type'=>"Image",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("remark"),
                'field'=>"remark",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("pay_status"),
                'field'=>"pay_status",
                'type'=>"ChosenSelect",
                'search'=>1
            ],
            [
                "label"=>$this->getAttributeLabel("pay_end_time"),
                'field'=>"pay_end_time",
                'type'=>"String",
                'search'=>0,
                'style'=>'width:200px;display: inline-block'
            ],
            [
                "label"=>$this->getAttributeLabel("service_status"),
                'field'=>"service_status",
                'type'=>"ChosenSelect",
                'search'=>1
            ],
            [
                "label"=>$this->getAttributeLabel("service_time"),
                'field'=>"service_time",
                'type'=>"String",
                'search'=>0,
                'style'=>'width:200px;display: inline-block'
            ],
            [
                "label"=>$this->getAttributeLabel("order_status"),
                'field'=>"order_status",
                'type'=>"ChosenSelect",
                'search'=>1
            ],
            [
                "label"=>$this->getAttributeLabel("status"),
                'field'=>"status",
                'type'=>"Switch",
                'search'=>1
            ],
            [
                "label"=>$this->getAttributeLabel("create_time"),
                'field'=>"create_time",
                'type'=>"String",
                'search'=>0,
                'style'=>'width:200px;display: inline-block'
            ],
            [
                "label"=>$this->getAttributeLabel("operate_id"),
                'field'=>"operate_id",
                'type'=>"String"
            ]
        ];
    }


    public function getSearchFields(){
        $order_detail_model = new OrderDetail();
        $student_model = new Student();
        return [
            [
                "label"=>$this->getAttributeLabel("title"),
                'field'=>"title",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$this->getAttributeLabel("order_no"),
                'field'=>"order_no",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$this->getAttributeLabel("child_no"),
                'field'=>"child_no",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$this->getAttributeLabel("course_id"),
                'field'=>"course_id",
                'type'=>"ChosenSelect",
                'search'=>1,
                'style'=>"width:240px;"
            ],
            [
                "label"=>$this->getAttributeLabel("belong_teacher_id"),
                'field'=>"belong_teacher_id",
                'type'=>"ChosenSelect",
                'search'=>1,
                'style'=>"width:240px;"
            ],
            [
                "label"=>$this->getAttributeLabel("belong_sale_group_id"),
                'field'=>"belong_sale_group_id",
                'type'=>"ChosenSelect",
                'search'=>1,
                'style'=>"width:240px;"
            ],
            [
                "label"=>$this->getAttributeLabel("belong_sale_id"),
                'field'=>"belong_sale_id",
                'type'=>"ChosenSelect",
                'search'=>1,
                'style'=>"width:240px;"
            ],
            [
                "label"=>$order_detail_model->getAttributeLabel("sale_wechat_no"),
                'field'=>"sale_wechat_no",
                'type'=>"String",
                'search'=>1,
                'style'=>"width:180px;"
            ],
            [
                "label"=>$this->getAttributeLabel("source_id"),
                'field'=>"source_id",
                'type'=>"ChosenSelect",
                'search'=>1,
                'style'=>"width:240px;"
            ],
            [
                "label"=>$order_detail_model->getAttributeLabel("payment_id"),
                'field'=>"payment_id",
                'type'=>"ChosenSelect",
                'search'=>1,
                'style'=>"width:240px;"
            ],
            [
                "label"=>$order_detail_model->getAttributeLabel("payment_type"),
                'field'=>"payment_type",
                'type'=>"ChosenSelect",
                'search'=>1
            ],
            [
                "label"=>$student_model->getAttributeLabel("wechat_no"),
                'field'=>"wechat_no",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$student_model->getAttributeLabel("mobile"),
                'field'=>"mobile",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$student_model->getAttributeLabel("inside"),
                'field'=>"inside",
                'type'=>"Switch",
                'search'=>1
            ],
            [
                "label"=>$student_model->getAttributeLabel("compact"),
                'field'=>"compact",
                'type'=>"Switch",
                'search'=>1
            ],
        ];
    }

    /**
     * @return array
     * 表单数据
     */
    public function getFormFields(){
        $order_detail_model = new OrderDetail();
        $student_model = new Student();
        return [
            [
                "label"=>$this->getAttrLabel('course_id'),
                'name'=>"course_id",
                'type'=>"ChosenSelect",
                'value'=>0,
                "rules"=>['required'=>true]
            ],
            [
                "label"=>$this->getAttrLabel('total_price'),
                'name'=>"total_price",
                'type'=>"String",
                'value'=>"",
            ],
            [
                "label"=>$this->getAttrLabel("title"),
                'name'=>"title",
                'type'=>"String",
                'value'=>"",
                "rules"=>['required'=>true]
            ],

            [
                "label"=>$this->getAttrLabel("source_id"),
                'name'=>"source_id",
                'type'=>"ChosenSelect",
                'value'=>"",
                "rules"=>['required'=>true]
            ],
            [
                "label"=>$order_detail_model->getAttrLabel("payment_id"),
                'name'=>"payment_id",
                'type'=>"ChosenSelect",
                'value'=>"",
                "rules"=>['required'=>true]
            ],
            [
                "label"=>$order_detail_model->getAttrLabel('payment_type'),
                'name'=>"payment_type",
                'type'=>"Switch",
                'value'=>0,
                'options'=>[['label'=>"定金",'value'=>0],['label'=>"全款",'value'=>3]]
            ],
            [
                "label"=>$this->getAttrLabel('pay_end_time'),
                'name'=>"pay_end_time",
                'type'=>"DateTime",
            ],
            [
                "label"=>$order_detail_model->getAttrLabel('sale_price'),
                'name'=>"sale_price",
                'type'=>"String",
                'value'=>"0",
                "rules"=>['required'=>true,"number"=>true]
            ],
            [
                "label"=>$this->getAttrLabel('coupon'),
                'name'=>"coupon",
                'type'=>"String",
                'value'=>"0",
            ],
            [
                "label"=>$this->getAttrLabel('belong_sale_id'),
                'name'=>"belong_sale_id",
                'type'=>"ChosenSelect",
                'value'=>0,
                "rules"=>['required'=>true]
            ],
            [
                "label"=>$order_detail_model->getAttrLabel('sale_wechat_no'),
                'name'=>"sale_wechat_no",
                'type'=>"String",
                'value'=>"",
                "rules"=>['required'=>true]
            ],
            [
                "label"=>$this->getAttrLabel('remark'),
                'name'=>"remark",
                'type'=>"Text",
                'value'=>""
            ],
            [
                "label"=>$student_model->getAttrLabel('wechat_name'),
                'name'=>"wechat_name",
                'type'=>"String",
                'value'=>"",
            ],
            [
                "label"=>$student_model->getAttrLabel('wechat_no'),
                'name'=>"wechat_no",
                'type'=>"String",
                'value'=>"",
                "rules"=>['required'=>true]
            ],
            [
                "label"=>$student_model->getAttrLabel('mobile'),
                'name'=>"mobile",
                'type'=>"String",
                'value'=>"",
                "rules"=>['required'=>true,"number"=>true,'mobile'=>true]
            ],
            [
                "label"=>$student_model->getAttrLabel('income'),
                'name'=>"income",
                'type'=>"String",
                'value'=>"0",
            ],
            [
                "label"=>$student_model->getAttrLabel('age'),
                'name'=>"age",
                'type'=>"String",
                'value'=>"0",
            ],
            [
                "label"=>$student_model->getAttrLabel('major'),
                'name'=>"major",
                'type'=>"String",
                'value'=>"",
            ],
            [
                "label"=>$student_model->getAttrLabel('address'),
                'name'=>"address",
                'type'=>"String",
            ],
            [
                "label"=>$student_model->getAttrLabel('add_time'),
                'name'=>"add_time",
                'type'=>"DateTime",
                "value"=>''
            ],
            [
                "label"=>$order_detail_model->getAttrLabel('images'),
                'name'=>"images",
                'type'=>"Image",
                'value'=>[]
            ],

            [
                "label"=>$student_model->getAttrLabel('inside'),
                'name'=>"inside",
                'type'=>"Switch",
                'value'=>0,
                'options'=>[['label'=>"否",'value'=>0],['label'=>"是",'value'=>1]]
            ],
            [
                "label"=>$student_model->getAttrLabel('compact'),
                'name'=>"compact",
                'type'=>"Switch",
                'value'=>0,
                'options'=>[['label'=>"否",'value'=>0],['label'=>"是",'value'=>1]]
            ],
            [
                "label"=>$this->getAttrLabel('status'),
                'name'=>"status",
                'type'=>"Switch",
                'value'=>0,
                'options'=>[['label'=>"开启",'value'=>0],['label'=>"禁用",'value'=>1]]
            ]
        ];
    }


    /**
     *
     */
   public function getFormChoseSelect(){
        $course_model = new Course();
        $admin_model = new Admin();
        $source_model = new Source();
        $payment_model = new Payment();
        return [
            'course_id'=>$course_model->courseArray(true),
            'belong_sale_id'=>$admin_model->getSaleList(true),
            'source_id'=>$source_model->sourceArray(true),
            'payment_id'=>$payment_model->paymentArray(true)
        ];

   }
    public function getTableChoseSelect(){
        $course_model = new Course();
        $admin_model = new Admin();
        $source_model = new Source();
        $payment_model = new Payment();
        $payment_types = [];
        foreach (static::$payment_type_maps as $k=>$v){
            if($k==static::PAYMENT_TYPE_0 || $k== static::PAYMENT_TYPE_3){
                $data = [
                    'id'=>$k,
                    'name'=>$v
                ];
                $payment_types[] = $data;
                unset($data);
            }
        }
        return [
            'course_id'=>$course_model->courseArray(),
            'belong_teacher_id'=>$admin_model->getTeacherList(),
            'belong_sale_group_id'=>$admin_model->getSaleLeaderList(),
            'belong_sale_id'=>$admin_model->getSaleList(),
            'source_id'=>$source_model->sourceArray(),
            'payment_id'=>$payment_model->paymentArray(),
            'payment_type'=>$payment_types,
            'pay_status'=>$this->formatArray(static::$pay_status_maps),
            'service_status'=>$this->formatArray(static::$service_status_maps),
            'order_status'=>$this->formatArray(static::$order_status_maps),
            'course_type'=>$this->formatArray(Course::$course_type_maps)
        ];

    }

    public function getPage($page=1,$params=[],$search=false){
        $offset = ($page - 1) * $this->pageSize;
        $query = new Query();
        $field = "o.id as id,o.title as title,o.order_no as order_no,o.child_no as child_no,o.course_id as course_id,o.course_type as course_type,";
        $field .="o.belong_teacher_id as belong_teacher_id,o.belong_sale_id as belong_sale_id,o.belong_sale_group_id as belong_sale_group_id,";
        $field .="d.sale_wechat_no as sale_wechat_no,o.source_id as source_id,d.payment_id as payment_id,o.total_price as total_price,";
        $field .="o.paid_price as paid_price,o.coupon as coupon,o.big_price as big_price,o.small_price as small_price,o.seek_price as seek_price,o.push_money as push_money,";
        $field .="o.bonus as bonus,d.payment_type as payment_type,s.wechat_name as wechat_name,s.wechat_no as wechat_no,s.income as income,s.age as age,s.major as major,s.address as address,";
        $field .="s.add_time as add_time,s.inside as inside,s.compact as compact,o.remark as remark,o.pay_status as pay_status,o.pay_end_time as pay_end_time,o.service_status as service_status,";
        $field .="o.service_time as service_time,o.order_status as order_status,o.status as status,o.create_time as create_time,d.sale_price as sale_price,s.mobile as mobile,d.images as images,o.operate_id as operate_id,";
        $field .="o.duration as duration,o.small_duration as small_duration";
        $prefix = \Yii::$app->db->tablePrefix;
        $query->select($field)->from($prefix."order as o")
            ->leftJoin($prefix."order_detail as d",'o.order_no = d.order_no')
            ->leftJoin($prefix."student as s",'o.student_id = s.id')
            ->where("o.is_deleted=".self::NO_DELETED)
            ->andWhere("d.position =1 ");
        if(!$search){
            $role_id = RedisUtils::getLogin($this->getAccessToken(),['role_id']);
            $uid = RedisUtils::getLogin($this->getAccessToken(),['uid']);
            if($role_id){
                switch ($role_id){
                    case Role::SALE_NO;
                        $query->andWhere(["=",'o.belong_sale_id',$uid]);
                        break;
                    case Role::SALE_LEADER_NO:
                        $sale_list = Admin::find()->select(['id'])->where($this->baseWhere())->andWhere(['leader_id'=>$uid])->asArray()->all();
                        if($sale_list){
                            $uids  = ArrayHelper::getColumn($sale_list,'id');
                            $query->andWhere(["in",'o.belong_sale_id',$uids]);
                        }
                        break;
                }
            }
        }

        if($params){
            if(isset($params['title'])){
                $query->andFilterWhere(['like','o.title',$params['title']]);
            }
            if(isset($params['order_no'])){
                $query->andFilterWhere(['like','o.order_no',$params['order_no']]);
            }
            if(isset($params['child_no'])){
                $query->andFilterWhere(['like','o.child_no',$params['child_no']]);
            }
            if(isset($params['sale_wechat_no'])){
                $query->andFilterWhere(['like','d.sale_wechat_no',$params['sale_wechat_no']]);
            }
            if(isset($params['wechat_name'])){
                $query->andFilterWhere(['like','s.wechat_name',$params['wechat_name']]);
            }
            if(isset($params['wechat_no'])){
                $query->andFilterWhere(['like','s.wechat_no',$params['wechat_no']]);
            }
            if(isset($params['major'])){
                $query->andFilterWhere(['like','s.major',$params['major']]);
            }
            if(isset($params['address'])){
                $query->andFilterWhere(['like','s.address',$params['address']]);
            }
            if(isset($params['course_id']) && $params['course_id'] > -1){
                $query->andFilterWhere(["=",'o.course_id',$params['course_id']]);
            }
            if(isset($params['course_type']) && $params['course_type'] > -1){
                $query->andFilterWhere(["=",'o.course_type',$params['course_type']]);
            }
            if(isset($params['belong_sale_id']) && $params['belong_sale_id'] > -1){
                $query->andFilterWhere(["=",'o.belong_sale_id',$params['belong_sale_id']]);
            }
            if(isset($params['belong_sale_group_id']) && $params['belong_sale_group_id'] > -1){
                $query->andFilterWhere(["=",'o.belong_sale_group_id',$params['belong_sale_group_id']]);
            }
            if(isset($params['belong_teacher_id']) && $params['belong_teacher_id'] > -1){
                $query->andFilterWhere(["=",'o.belong_teacher_id',$params['belong_teacher_id']]);
            }
            if(isset($params['source_id']) && $params['source_id'] > -1){
                $query->andFilterWhere(["=",'o.source_id',$params['source_id']]);
            }
            if(isset($params['pay_status']) && $params['pay_status'] > -1){
                $query->andFilterWhere(["=",'o.pay_status',$params['pay_status']]);
            }
            if(isset($params['service_status']) && $params['service_status'] > -1){
                $query->andFilterWhere(["=",'o.service_status',$params['service_status']]);
            }
            if(isset($params['order_status']) && $params['order_status'] > -1){
                $query->andFilterWhere(["=",'o.order_status',$params['order_status']]);
            }
            if(isset($params['payment_id']) && $params['payment_id'] > -1){
                $query->andFilterWhere(["=",'d.payment_id',$params['payment_id']]);
            }
            if(isset($params['payment_type']) && $params['payment_type'] > -1){
                $query->andFilterWhere(["=",'d.payment_type',$params['payment_type']]);
            }
            if(isset($params['inside']) && $params['inside'] > -1){
                $query->andFilterWhere(["=",'s.inside',$params['inside']]);
            }
            if(isset($params['compact']) && $params['compact'] > -1){
                $query->andFilterWhere(["=",'s.compact',$params['compact']]);
            }
            if(isset($params['status']) && $params['status'] > -1){
                $query->andFilterWhere(["=",'o.status',$params['status']]);
            }
        }

        $totalCell = $query->count();
        $data = $query->offset($offset)->limit($this->pageSize)->orderBy('o.id desc')->all();
        if($data){
            foreach ($data as &$v){
                $v['add_time'] = $v['add_time'] ? date($this->timeFormat,$v['add_time']) : "-";
                $v['create_time'] = $v['create_time'] ? date($this->timeFormat,$v['create_time']) : "-";
                $v['service_time'] = $v['service_time'] ? date($this->timeFormat,$v['service_time']) : "-";
                $v['pay_end_time'] = $v['pay_end_time'] ? date($this->timeFormat,$v['pay_end_time']) : "-";
                $v['push_money'] = $v['push_money'] ? $v['push_money'] : "-";
                $v['bonus'] = $v['bonus'] ? $v['bonus'] : "-";
                $v['debt'] = round($v['total_price'] - $v['paid_price'] - $v['coupon'],2);
                $v['images'] = $v['images'] ? explode(",",$v['images']) : [];
                $v['operate_id'] = $v['operate_id'] ? RedisUtils::getNickname($v['operate_id']) : "-";
            }
        }
        return [
            'totalCell'=>$totalCell,
            'list'=>$data
        ];
    }

    public function getInfo($id){
        $field = "o.id as id,o.title as title,o.order_no as order_no,o.course_id as course_id,o.belong_teacher_id as belong_teacher_id,o.belong_sale_id as belong_sale_id,o.course_type as course_type,";
        $field .="o.belong_sale_group_id as belong_sale_group_id,d.sale_wechat_no as sale_wechat_no,o.source_id as source_id,d.payment_id as payment_id,o.total_price as total_price,";
        $field .="o.paid_price as paid_price,o.coupon as coupon,o.big_price as big_price,o.small_price as small_price,o.seek_price as seek_price,o.push_money as push_money,";
        $field .="o.bonus as bonus,d.payment_type as payment_type,s.wechat_name as wechat_name,s.wechat_no as wechat_no,s.income as income,s.age as age,s.major as major,s.address as address,";
        $field .="s.add_time as add_time,s.inside as inside,s.compact as compact,o.remark as remark,o.pay_status as pay_status,o.pay_end_time as pay_end_time,o.service_status as service_status,";
        $field .="o.service_time as service_time,o.order_status as order_status,o.status as status,o.create_time as create_time,d.sale_price as sale_price,s.mobile as mobile,d.images as images,o.operate_id as operate_id,";
        $field .="o.duration as duration,o.small_duration as small_duration";
        $prefix = \Yii::$app->db->tablePrefix;
        $query = new Query();
        $query->select($field)->from($prefix."order as o")
            ->leftJoin($prefix."order_detail as d",'o.order_no = d.order_no')
            ->leftJoin($prefix."student as s",'o.student_id = s.id')
            ->where("o.is_deleted=".self::NO_DELETED)
            ->andWhere('o.id='.$id)
            ->andWhere("d.position =1");
        $data = $query->one();
        if($data){
            $data['add_time'] = $data['add_time'] ? date($this->timeFormat,$data['add_time']) : "-";
            $data['create_time'] = $data['create_time'] ? date($this->timeFormat,$data['create_time']) : "-";
            $data['service_time'] = $data['service_time'] ? date($this->timeFormat,$data['service_time']) : "-";
            $data['pay_end_time'] = $data['pay_end_time'] ? date($this->timeFormat,$data['pay_end_time']) : "-";
            $data['push_money'] = $data['push_money'] ? $data['push_money'] : "-";
            $data['bonus'] = $data['bonus'] ? $data['bonus'] : "-";
            $data['debt'] = round($data['total_price'] - $data['paid_price'] - $data['coupon'],2);
            $data['images'] =  $data['images'] ? explode(",",$data['images']) :[];
            $data['operate_id'] = $data['operate_id'] ? RedisUtils::getNickname($data['operate_id']) : "-";
        }
        return $data;

    }






}