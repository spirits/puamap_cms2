<?php
namespace frontend\controllers;
use common\models\ParentActiveRecord;
use frontend\forms\NodeForm;
use frontend\models\Node;
use yii\bootstrap\Nav;
use yii\db\Exception;

class NodeController extends BaseController{
    public function actionIndex(){
        $this->in['data'] = $this->getTreeData();
        $this->in['status'] = true;
        return $this->in;

    }

    /**
     * @return array
     * 添加
     */
    public function actionCreate(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $data["NodeForm"] =$post_data;
            $nodeForm = new NodeForm();
            $nodeForm->load($data);
            if($nodeForm->validate()){
                $model = new Node();
                $model->name = $nodeForm->name;
                $model->route =$nodeForm->route;
                $model->parent_id = $nodeForm->parent_id;
                $model->status = $nodeForm->status;
                if($model->save()){
                    $this->in['status'] = true;
                    $this->in['data'] = $this->getTreeData();
                }
            }else{
                $this->in['status'] = false;
                $this->in['message'] = "验证失败";
            }
        }
        return $this->in;
    }

    /**
     * @return array
     * 修改
     */
    public function actionUpdate(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $data["NodeForm"] =$post_data;
            $nodeForm = new NodeForm();
            $nodeForm->load($data);
            if($nodeForm->validate()){
                $model = Node::findOne($nodeForm->id);
                $model->name = $nodeForm->name;
                $model->route =$nodeForm->route;
                $model->status = $nodeForm->status;
                if($model->save()){
                    $this->in['status'] = true;
                    $this->in['data'] = $this->getTreeData();
                }
            }else{
                $this->in['status'] = false;
                $this->in['message'] = "验证失败";
            }
        }
        return $this->in;
    }

    /**
     * @return array
     * 删除
     */
    public function actionDelete(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $model = Node::findOne($post_data['id']);
            $model->is_deleted = ParentActiveRecord::IS_DELETED;
            if($model->save()){
                $this->in['status'] = true;
                $this->in['data'] = $this->getTreeData();
            }
        }
        return $this->in;
    }

    /**
     * @return array
     * 上移
     */
    public function actionUp(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $transaction = \Yii::$app->db->beginTransaction();
            try{
                $model = Node::findOne($post_data['id']);
                if($model){
                    $pre_model = Node::find()->where($model->baseWhere())->andWhere(["<",'sort',$model->sort])
                        ->andWhere(['parent_id'=>$model->parent_id])->orderBy(['sort'=>SORT_DESC])->one();
                    if($pre_model){
                       $sort =  $pre_model->sort;
                       $pre_model->sort = $model->sort;
                       $model->sort = $sort;
                       if($model->save() && $pre_model->save()){
                           $this->in['status'] = true;
                           $this->in['data'] = $this->getTreeData();
                       }else{
                           throw  new Exception("操作失败");
                       }
                    }else{
                        throw  new Exception("无法移动");
                    }
                }else{
                    throw  new Exception("不存在");
                }
                $transaction->commit();
            }catch (Exception $e){
                $this->in['message'] = $e->getMessage();
                $transaction->rollBack();
            }
        }
        return $this->in;
    }

    /**
     * @return array
     * 下移
     */
    public function actionDown(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $transaction = \Yii::$app->db->beginTransaction();
            try{
                $model = Node::findOne($post_data['id']);
                if($model){
                    $pre_model = Node::find()->where($model->baseWhere())->andWhere([">",'sort',$model->sort])
                        ->andWhere(['parent_id'=>$model->parent_id])->orderBy(['sort'=>SORT_ASC])->one();
                    if($pre_model){
                        $sort =  $pre_model->sort;
                        $pre_model->sort = $model->sort;
                        $model->sort = $sort;
                        if($model->save() && $pre_model->save()){
                            $this->in['status'] = true;
                            $this->in['data'] = $this->getTreeData();
                        }else{
                            throw  new Exception("操作失败");
                        }
                    }else{
                        throw  new Exception("无法移动");
                    }
                }else{
                    throw  new Exception("不存在");
                }
                $transaction->commit();
            }catch (Exception $e){
                $this->in['message'] = $e->getMessage();
                $transaction->rollBack();
            }
        }
        return $this->in;
    }

    /**
     * @return array
     * 左移
     */
    public function actionLeft(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $transaction = \Yii::$app->db->beginTransaction();
            try{
                $model = Node::findOne($post_data['id']);
                if($model){
                    $parent_model = Node::findOne($model->parent_id);
                    if($parent_model){
                        $parent_id = $model->parent_id;
                        $sort= $model->sort;
                        $parents = Node::find()->where($model->baseWhere())->andWhere(['parent_id'=>$parent_model->parent_id])->andWhere(['>','sort',$parent_model->sort])->all();
                            if($parents){
                                foreach ($parents as $item){
                                    $item->updateCounters(['sort'=>1]);
                                }
                            }
                        $model->parent_id = $parent_model->parent_id;
                        $model->sort = $parent_model->sort+1;
                        if($model->save()){
                            $childs = Node::find()->where($model->baseWhere())->andWhere(['parent_id'=>$parent_id])->andWhere(['>','sort',$sort])->all();
                            if($childs){
                                foreach ($childs as $item){
                                    $item->updateCounters(['sort'=>-1]);
                                }
                            }
                            $this->in['status'] = true;
                            $this->in['data'] = $this->getTreeData();
                        }else{
                            throw  new Exception("操作失败");
                        }
                    }else{
                        throw  new Exception("无法移动");
                    }
                }else{
                    throw  new Exception("不存在");
                }
                $transaction->commit();
            }catch (Exception $e){
                $this->in['message'] = $e->getMessage();
                $transaction->rollBack();
            }
        }
        return $this->in;
    }

    public function actionRight(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $transaction = \Yii::$app->db->beginTransaction();
            try{
                $model = Node::findOne($post_data['id']);
                if($model){
                    $pre_model = Node::find()->where($model->baseWhere())->andWhere(["<",'sort',$model->sort])
                        ->andWhere(['parent_id'=>$model->parent_id])->orderBy(['sort'=>SORT_DESC])->one();
                    if($pre_model){
                        $sort = $model->sort;
                        $count = Node::find()->where($model->baseWhere())->andWhere(['parent_id'=>$pre_model->id])->count();
                        $model->parent_id = $pre_model->id;
                        $model->sort = ++$count;
                        if($model->save()){
                            $models = Node::find()->where($model->baseWhere())->andWhere(['parent_id'=>$pre_model->parent_id])->andWhere(['>','sort',$sort])->all();
                            if($models){
                                foreach ($models as $item){
                                    $item->updateCounters(['sort'=>-1]);
                                }
                            }
                            $this->in['status'] = true;
                            $this->in['data'] = $this->getTreeData();
                        }else{
                            throw  new Exception("操作失败");
                        }
                    }else{
                        throw  new Exception("无法移动");
                    }
                }else{
                    throw  new Exception("不存在");
                }
                $transaction->commit();
            }catch (Exception $e){
                $this->in['message'] = $e->getMessage();
                $transaction->rollBack();
            }
        }
        return $this->in;
    }

    public function getTreeData(){
        $model = new Node();
        $fields = ['id','parent_id','name','route','status'];
        $data = $model->getALL($fields);
        return $model->formatTree($data);
    }


}