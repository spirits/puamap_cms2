<?php
namespace common\models;
/**
 * Class Role
 * @package common\models
 * @property int $id
 * @property string $name
 * @property int $income_status
 * @property int $income
 * @property string $income_ratio
 * @property string $auth
 * @property int $parent_id
 * @property int $create_time
 * @property int $update_time
 * @property int $sort
 * @property int $is_deleted
 * @property int $status
 * @property int $operate_id
 */
class Role extends ParentActiveRecord{
    //导师
    const TEACHER_NO = 5;
    //助教
    const ASSISTANT_NO=6;
    //销售总监
    const SALE_BOSS_NO = 7;
    //销售主管
    const SALE_LEADER_NO = 8;
    //销售
    const SALE_NO = 9;
    public static function tableName()
    {
        return "{{%role}}";
    }

    public function attributeLabels()
    {
        return parent::attributeLabels() + [
                'id'=>"ID",
                'name'=>'名称',
                'income_status'=>"状态",
                'income'=>'底薪',
                'income_ratio'=>'收益率',
                'parent_id'=>'父级',
                'auth'=>"权限"
            ];
    }

    public static function getTeacherRatio(){
        $model = static::findOne(static::TEACHER_NO);
        return $model->income_ratio;
    }
}