<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_log`.
 */
class m180619_055457_create_order_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_log}}', [
            'id' => $this->primaryKey(),
            'order_no'=>$this->string()->notNull()->comment("订单号"),
            'source_id'=>$this->string()->defaultValue("-")->comment("来源"),
            'course_id'=>$this->string()->defaultValue("-")->comment("课程"),
            'belong_teacher_id'=>$this->string()->defaultValue("-")->comment("所属导师"),
            'belong_sale_id'=>$this->string()->defaultValue("-")->comment("所属销售"),
            "belong_sale_group_id"=>$this->string()->defaultValue("-")->comment("销售组"),
            'payment_id'=>$this->string()->defaultValue("-")->comment("支付方式"),
            'payment_type'=>$this->string()->defaultValue("-")->comment("支付类型"),//1.定金 2.回款 3.尾款  4.全款  5.退款
            'total_price'=>$this->string()->defaultValue("-")->comment("总价"),
            'paid_price'=>$this->string()->defaultValue("-")->comment("已付金额"),
            'sale_price'=>$this->string()->defaultValue("-")->comment("支付金额"),
            'coupon'=>$this->string()->defaultValue("-")->comment("优惠金额"),
            'big_price'=>$this->string()->defaultValue("-")->comment("大课价格"),
            'small_price'=>$this->string()->defaultValue("-")->comment("小课价格"),
            'seek_price'=>$this->string()->defaultValue("-")->comment("咨询价格"),
            'bonus'=>$this->string()->defaultValue("-")->comment("奖金"),
            'push_money'=>$this->string()->defaultValue("-")->comment("提成金额"),
            'order_status'=>$this->string()->defaultValue("-")->comment("订单状态"),//1.未审核 3.已审核 4.退款  5.转课  6.转接
            'service_status'=>$this->string()->defaultValue("-")->comment("是否开课"),//1.未开始服务，//2.服务中 //3.服务冻结,4.继续服务解冻 5服务结束
            'service_time'=>$this->string()->defaultValue("-")->comment("服务开始时间"),
            'pay_status'=>$this->string()->defaultValue("-")->comment("订单状态"),//1未完成 2已完成
            "pay_end_time"=>$this->string()->defaultValue("-")->comment("尾款时间"),
            'sale_wechat_no'=>$this->string()->defaultValue("-")->comment("微信号"),
            'order_detail_status'=>$this->string()->defaultValue("-")->comment("订单状态"),//1未审核 2.财务审核 3已审核
            'mobile'=>$this->string()->defaultValue("-")->comment("电话"),
            'wechat_name'=>$this->string()->defaultValue("-")->comment("微信名"),
            'wechat_no'=>$this->string()->defaultValue("-")->comment("学员微信号"),
            'income'=>$this->string()->defaultValue("-")->comment("月收入"),
            'age'=>$this->string()->defaultValue("-")->comment("年龄"),
            'major'=>$this->string(200)->defaultValue("-")->comment("职业"),
            'address'=>$this->string(255)->defaultValue("-")->comment("地址"),
            'add_time'=>$this->string()->defaultValue("-")->comment("添加好友时间"),
            'inside'=>$this->string()->defaultValue("-")->comment("内部学员"),
            'compact'=>$this->string()->defaultValue("-")->comment("合同"),
            'actions'=>$this->string()->defaultValue("-")->comment("行为"),
            'remark'=>$this->string(255)->defaultValue("")->comment("备注"),
            "images"=>$this->string(1000)->defaultValue("")->comment("图片"),
            'create_time'=>$this->integer(11)->comment("创建时间"),
            'update_time'=>$this->integer(11)->comment("更新时间"),
            'is_deleted'=>$this->smallInteger(1)->defaultValue(0)->comment("是否删除"),
            'status'=>$this->smallInteger(1)->defaultValue(0)->comment("是否正常"),
            'operate_id'=>$this->integer(11)->defaultValue(1)->comment("操作者"),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_log}}');
    }
}
