<?php
namespace frontend\controllers;
use frontend\models\Order;

class SearchController extends BaseController{



    public function actionIndex(){
        $model = new Order();
        $data['fields'] = $model->getSearchFields();
        $data['tableChose'] = $model->getTableChoseSelect();
        $data['formFields'] = $model->getFormFields();
        $data['pages'] = $model->getPageInfo();
        $data['pages']['totalCell']= "0";
        $this->in['data'] = $data;
        $this->in['status'] = true;

        return $this->in;
    }
    public function actionPage(){
        $page = \Yii::$app->request->post('page',1);
        $params = \Yii::$app->request->post();
        $model = new Order();
        $data = $model->getPage($page,$params,true);
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }
}