<?php
namespace common\models;
/**
 * Class OrderLog
 * @package common\models
 * @property int $id
 * @property string $order_no
 * @property string $child_no
 * @property int $course_id
 * @property int $course_type
 * @property int $belong_teacher_id
 * @property int $belong_sale_id
 * @property int $belong_sale_group_id
 * @property int $payment_id
 * @property int $payment_type
 * @property double $total_price
 * @property double $paid_price
 * @property double $sale_price
 * @property double $coupon
 * @property double $big_price
 * @property double $small_price
 * @property double $seek_price
 * @property double $push_money
 * @property int $bonus
 * @property int $order_status
 * @property int $service_status
 * @property int $service_time
 * @property int $pay_status
 * @property int $pay_end_time
 * @property string $sale_wechat_no
 * @property int $order_detail_status
 * @property string $wechat_name
 * @property string $wechat_no
 * @property  double $income
 * @property int $age
 * @property string $major
 * @property string $address
 * @property int $add_time
 * @property int $inside
 * @property int $compact
 * @property string $actions
 * @property string $images
 * @property int $create_time
 * @property int $update_time
 * @property int $id_deleted
 * @property int $status
 * @property int $operate_id
 * @property int $source_id
 * @property string $remark
 * @property string $mobile
 */
class OrderLog extends ParentActiveRecord{
    const CREATE = 'create';
    const UPDATE = "update";
    const DELETE = "delete";
    const RESALE = "return";
    const RECOURSE='redirect';
    const PLAY="play";
    const REPLAY = 'replay';
    const PAUSE ="pause";
    const PUSH = "push";
    const PASS = 'pass';
    const BONUS='bonus';
    const END="end";

    public static $actions_maps = [
        self::CREATE=>"创建",
        self::UPDATE=>"更新",
        self::DELETE=>"删除",
        self::RESALE=>"更换销售",
        self::RECOURSE=>"转课",
        self::PLAY=>"开始服务",
        self::REPLAY=>"继续服务",
        self::PAUSE=>"暂停服务",
        self::PUSH=>"修改提成",
        self::PASS=>"财务审核",
        self::BONUS=>"销售奖金",
        self::END=>'系统结束'
    ];

    public static function tableName()
    {
        return "{{%order_log}}";
    }

    public function attributeLabels()
    {
        return parent::attributeLabels() +[
                'order_no'=>"订单号",
                'child_no'=>'接单号',
                'course_id'=>'课程',
                'course_type'=>'课程类型',
                'belong_teacher_id'=>'导师',
                'belong_sale_id'=>'销售',
                'belong_sale_group_id'=>"销售组",
                'payment_id'=>'付款方式',
                'payment_type'=>'付款类型',
                'push_money'=>'提成',
                'total_price'=>'总价',
                'paid_price'=>'已付',
                'sale_price'=>'支付',
                'coupon'=>'优惠',
                'big_price'=>'大课',
                'small_price'=>'小课',
                'seek_price'=>'咨询',
                'source_id'=>'来源',
                'bonus'=>"奖金",
                'order_status'=>'状态',
                'service_status'=>'服务',
                'service_time'=>'开始服务',
                'pay_status'=>'支付状态',
                'pay_end_time'=>'尾款时间',
                'sale_wechat_no'=>'销售微信',
                'remark'=>'备注',
                'order_detail_status'=>'财务状态',
                'wechat_name'=>'微信名',
                'wechat_no'=>'微信号',
                'income'=>'月收入',
                'age'=>'年龄',
                'major'=>"职业",
                'address'=>'地址',
                'add_time'=>'好友时间',
                'inside'=>'内部',
                'compact'=>'合同',
                'actions'=>'行为',
                'mobile'=>'电话',
                'images'=>'图片'
            ];
    }

}