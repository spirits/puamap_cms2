<?php
namespace common\models;
use yii\web\IdentityInterface;

/**
 * Class Admin
 * @package common\models
 * @property int $id
 * @property string $username
 * @property string $nickname
 * @property string $access_token
 * @property string $password
 * @property int $role_id
 * @property int $leader_id
 * @property int $create_time
 * @property int $update_time
 * @property int $login_time
 * @property int $id_deleted
 * @property int $status
 * @property int $operate_id
 */
class Admin extends ParentActiveRecord implements IdentityInterface{
    public $auth_key;
    public function attributeLabels()
    {
        return parent::attributeLabels() + [
                'username'=>"账号",
                'nickname'=>'昵称',
                'password'=>'密码',
                'leader_id'=>"领导",
                'access_token'=>'token',
                'role_id'=>'角色',
                'login_time'=>'登录时间',
            ];
    }

    /**
     * @return string
     * 表名
     */
    public static function tableName()
    {
        return "{{%admin}}";
    }

    /**
     * Finds an identity by the given ID.
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id){
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null){
        throw static::findOne(['access_token'=>$token]);
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|int an ID that uniquely identifies a user identity.
     */
    public function getId(){
        return $this->getPrimaryKey();
    }


    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey(){
        return $this->auth_key;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey){
        return $this->getAuthKey() == $authKey;
    }

}