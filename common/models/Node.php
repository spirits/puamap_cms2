<?php
namespace common\models;
/**
 * Class Node
 * @package common\models
 * @property int $id
 * @property string $name
 * @property string $route
 * @property int $parent_id
 * @property int $create_time
 * @property int $update_time
 * @property int $sort
 * @property int $is_deleted
 * @property int $status
 * @property int $operate_id
 */
class Node extends  ParentActiveRecord{
    public static function tableName()
    {
        return "{{%node}}";
    }

    public function attributeLabels()
    {
        return parent::attributeLabels() + [
                'name'=>"名称",
                'route'=>'路由',
                'parent_id'=>'父级',
                'status'=>'激活',
                'sort'=>'排序',
            ];
    }




}