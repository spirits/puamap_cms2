<?php
namespace frontend\controllers;
use common\utils\Qiniu;
use frontend\models\Node;

class IndexController extends BaseController{
    public function actionIndex(){
        $node = new Node();
        $this->in['status'] = true;
        $this->in['data'] = $node->getListByParentId();
        return $this->in;
    }

    public function actionAuthNode(){
        $node = new Node();
        $node_item = \Yii::$app->request->get('node');
        $this->in['status'] = true;
        $this->in['data'] = $node->getListByRoute($node_item);
        return $this->in;
    }



    public function actionUploadToken(){
        $qiniu = new Qiniu();
        $this->in['status'] = true;
        $this->in['data'] = $qiniu->token();
        return $this->in;
    }
}