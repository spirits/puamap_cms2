<?php
namespace frontend\filters;
use common\utils\RedisUtils;
use frontend\exceptions\AuthException;
use frontend\exceptions\NotFundAuthException;
use frontend\exceptions\NotPermissionException;
use frontend\models\Node;
use frontend\models\Role;
use yii\base\ActionFilter;

class PermissionFilterAction extends ActionFilter{
    public function beforeAction($action)
    {
        $params =\Yii::$app->request->queryParams;
        if($access_token = $params['access_token']){
            $controller_name = $action->controller->id;
            $action_name = $action->id;
            if($controller_name != "index" && $action_name != 'page'){
                $route = $controller_name."/".$action_name;
                $model  = Node::find()->where(['is_deleted'=>0,'status'=>0,'route'=>$route])->orderBy(['id'=>SORT_DESC])->one();
                if($model){
                    $role_id = RedisUtils::getLogin($access_token,['role_id']);
                    $auth = \Yii::$app->redis->get("role_".$role_id);
                    if(!$auth){
                        $auth = (Role::findOne($role_id)->auth);
                        if($auth){
                            \Yii::$app->redis->set("role_".$role_id,$auth);
                        }
                    }
                    $auth =  $auth ? explode(",",$auth) : [];
                    $auth = $role_id == 1 ? "all": $auth;
                    if($auth != 'all' && !in_array($model->id,$auth)){
                        throw new NotPermissionException();
                    }
                }else{
                    throw new NotFundAuthException();
                }
            }
        }else{
            throw  new AuthException();
        }
        return parent::beforeAction($action);
    }
}