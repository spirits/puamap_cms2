<?php
namespace frontend\controllers;
use common\models\ParentActiveRecord;
use frontend\forms\OrderDetailForm;
use frontend\models\Course;
use frontend\models\Order;
use frontend\models\OrderDetail;
use frontend\models\OrderLog;
use yii\db\Exception;

class OrderDetailController extends BaseController{
    public function actionIndex(){
        $order_no = \Yii::$app->request->get("order_no","");
        $model = new OrderDetail();
        $data['fields'] = $model->getFields();
        $data['formFields'] = $model->getFormFields($order_no);
        $data['controller'] = $model->getAuthController();
        $data['tableChose'] = $model->getTableChoseSelect();
        $data['formChose']= $model->getFormChoseSelect($order_no);
        $pageData = $model->getPage(['order_no'=>$order_no]);
        $data['items'] = $pageData['list'];
        $data['pages'] = $model->getPageInfo(['order_no'=>$order_no]);
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }

    public function actionPage(){
        $page = \Yii::$app->request->post('page',1);
        $params = \Yii::$app->request->post();
        $order_no = \Yii::$app->request->get("order_no","");
        $model = new OrderDetail();
        $data = $model->getPage(['order_no'=>$order_no],$page,$params);
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }
    public function actionCreate(){
        $order_no = \Yii::$app->request->get("order_no","");
        if(\Yii::$app->request->isPost){
            $data['OrderDetailForm'] = \Yii::$app->request->post();
            $orderDetailForm = new OrderDetailForm();
            $orderDetailForm->load($data);
            if($orderDetailForm->validate()){
                $transaction = \Yii::$app->db->beginTransaction();
                try{
                    $order_model = (new Order())->getOrderInfo($order_no);
                    if(!$order_model){
                        throw  new Exception("订单不存在");
                    }
                    $order_detail_model = new OrderDetail();
                    $order_detail_model->order_no = $order_no;
                    $order_detail_model->payment_id = $orderDetailForm->payment_id;
                    $order_detail_model->payment_type = $orderDetailForm->payment_type;
                    $order_detail_model->course_id = $orderDetailForm->course_id;
                    $order_detail_model->belong_teacher_id = $order_model->belong_teacher_id;
                    $order_detail_model->belong_sale_id = $orderDetailForm->belong_sale_id;
                    $order_detail_model->belong_sale_group_id = $order_model->belong_sale_group_id;
                    $order_detail_model->sale_wechat_no = $orderDetailForm->sale_wechat_no;
                    $order_detail_model->sale_price = $orderDetailForm->sale_price;
                    $order_detail_model->student_id = $order_model->student_id;
                    $order_detail_model->position = 0;
                    $order_detail_model->order_detail_status = OrderDetail::ORDER_DETAIL_STATUS_1;
                    $order_detail_model->remark = $orderDetailForm->remark;
                    $order_detail_model->images = $orderDetailForm->images ? implode(",",$orderDetailForm->images) : "";
                    if($order_detail_model->save())
                    {
                        switch ($orderDetailForm->payment_type){
                            case Order::PAYMENT_TYPE_1:
                                $order_model->paid_price += $order_detail_model->sale_price;
                                break;
                            case Order::PAY_STATUS_2:
                                $order_model->paid_price += $order_detail_model->sale_price;
                                $order_model->pay_status = Order::PAY_STATUS_2;
                                $order_model->pay_end_time = $orderDetailForm->pay_end_time ? (@strtotime($orderDetailForm->pay_end_time) ? strtotime($orderDetailForm->pay_end_time): time()) : time();
                                break;
                            case Order::PAYMENT_TYPE_4;
                                $order_model->paid_price -= $order_detail_model->sale_price;
                                if($order_model->service_status > 1 ){
                                    $order_model->service_status = Order::SERVICE_STATUS_5;
                                    $order_model->service_time = time();
                                }

                            break;
                        }
                        if($order_detail_model->remark){
                            $order_model->remark= $order_detail_model->remark;
                        }
                        if($order_model->save()){
                            $order_log_model = new OrderLog();
                            $order_log_model->order_no= $order_no;
                            $order_log_model->actions = OrderLog::CREATE;
                            $order_log_model->payment_id = $order_detail_model->payment_id;
                            $order_log_model->payment_type = $order_detail_model->payment_type;
                            $order_log_model->course_id = $order_detail_model->course_id;
                            $order_log_model->belong_teacher_id = $order_detail_model->belong_teacher_id;
                            $order_log_model->belong_sale_id = $order_detail_model->belong_sale_id;
                            $order_log_model->belong_sale_group_id = $order_detail_model->belong_sale_group_id;
                            $order_log_model->sale_wechat_no = $order_detail_model->sale_wechat_no;
                            $order_log_model->sale_price = $order_detail_model->sale_price;
                            $order_log_model->order_detail_status =$order_detail_model->order_detail_status;
                            $order_log_model->remark = $order_detail_model->remark;
                            $order_log_model->paid_price = $order_model->paid_price;
                            $order_log_model->pay_status = $order_model->pay_status;
                            $order_log_model->pay_end_time = $order_model->pay_end_time;
                            $order_log_model->service_status = $order_model->service_status;
                            $order_log_model->service_time = $order_model->service_time;
                            $order_log_model->images = $order_detail_model->images;
                            if($order_log_model->save()){
                                $this->in['status'] = true;
                                $pageData =  $order_detail_model->getPage(['order_no'=>$order_no]);
                                $this->in['data'] = $pageData['list'];
                            }else{
                                throw  new Exception("日志保存错误");
                            }
                        }else{
                            throw  new Exception("数据源保存错误");
                        }
                    }else{
                        throw  new Exception("数据保存错误");
                    }

                    $transaction->commit();
                }catch (Exception $e){
                    $this->in['status'] = false;
                    $this->in['message'] = $e->getMessage();
                    $transaction->rollBack();
                }
            }else{
                $this->in['message'] = $orderDetailForm->getErrors();
            }
        }
        return $this->in;
    }

    public function actionUpdate(){
        $order_no = \Yii::$app->request->get("order_no","");
        if(\Yii::$app->request->isPost){
            $data['OrderDetailForm'] = \Yii::$app->request->post();
            $orderDetailForm = new OrderDetailForm();
            $orderDetailForm->load($data);
            if($orderDetailForm->validate()){
                $transaction = \Yii::$app->db->beginTransaction();
                try{
                    $order_model = (new Order())->getOrderInfo($order_no);
                    if(!$order_model){
                        throw  new Exception("订单不存在");
                    }
                    $order_detail_model = OrderDetail::findOne($orderDetailForm->id);
                    $order_detail_model->payment_id = $orderDetailForm->payment_id;
                    $order_detail_model->payment_type = $orderDetailForm->payment_type;
                    $order_detail_model->belong_sale_id = $orderDetailForm->belong_sale_id;
                    $order_detail_model->belong_sale_group_id = $order_model->belong_sale_group_id;
                    $order_detail_model->sale_wechat_no = $orderDetailForm->sale_wechat_no;
                    $order_detail_model->sale_price = $orderDetailForm->sale_price;
                    $order_detail_model->remark = $orderDetailForm->remark;
                    $order_detail_model->images = $orderDetailForm->images ? implode(",",$orderDetailForm->images) : "";
                    $old_sale_price = $order_detail_model->getOldAttribute("sale_price");
                    $old_payment_type = $order_detail_model->getOldAttribute('payment_type');
                    $updateOrderDetailAttr =ParentActiveRecord::descAttr($order_detail_model->getOldAttributes(),$order_detail_model->getDirtyAttributes());
                    if($order_detail_model->save())
                    {
                        if(isset($updateOrderDetailAttr['payment_type'])){
                            switch ($orderDetailForm->payment_type){
                                case Order::PAYMENT_TYPE_1:
                                    if($old_payment_type == Order::PAYMENT_TYPE_4){
                                        Course::findOne($order_detail_model->course_id)->updateCounters(['refund_persons'=>-1]);
                                        $order_model->paid_price += ($order_detail_model->sale_price + $old_sale_price);
                                    }else{
                                        $order_model->paid_price += ($order_detail_model->sale_price - $old_sale_price);
                                    }
                                    $order_model->pay_status = Order::PAY_STATUS_1;
                                    $order_model->pay_end_time = 0;

                                    break;
                                case Order::PAY_STATUS_2:
                                    if($old_payment_type == Order::PAYMENT_TYPE_4){
                                        Course::findOne($order_detail_model->course_id)->updateCounters(['refund_persons'=>-1]);
                                        $order_model->paid_price += ($order_detail_model->sale_price + $old_sale_price);
                                    }else{
                                        $order_model->paid_price += ($order_detail_model->sale_price - $old_sale_price);
                                    }
                                    $order_model->pay_status = Order::PAY_STATUS_2;
                                    $order_model->pay_end_time = $orderDetailForm->pay_end_time ? (@strtotime($orderDetailForm->pay_end_time) ? strtotime($orderDetailForm->pay_end_time): time()) : time();
                                    break;
                                case Order::PAYMENT_TYPE_4;
                                    $order_model->paid_price -= ($order_detail_model->sale_price + $old_sale_price);
                                    if($order_model->service_status > 1 ){
                                        $order_model->service_status = Order::SERVICE_STATUS_5;
                                        $order_model->service_time = time();
                                    }
                                    Course::findOne($order_detail_model->course_id)->updateCounters(['refund_persons'=>1]);
                                    break;
                            }
                        }elseif (isset($updateOrderDetailAttr['sale_price'])){//单纯的修改了价格
                            switch ($order_detail_model->payment_type){
                                case Order::PAYMENT_TYPE_1:
                                    $order_model->paid_price += ($order_detail_model->sale_price - $old_sale_price);
                                    break;
                                case Order::PAY_STATUS_2:
                                    $order_model->paid_price += ($order_detail_model->sale_price - $old_sale_price);
                                    break;
                                case Order::PAYMENT_TYPE_4;
                                    $order_model->paid_price -= ($order_detail_model->sale_price - $old_sale_price);
                                    break;
                            }
                        }


                        if($order_detail_model->remark){
                            $order_model->remark= $order_detail_model->remark;
                        }
                        $updateOrderAttr = ParentActiveRecord::descAttr($order_model->getOldAttributes(),$order_model->getDirtyAttributes());
                        if($order_model->save()){
                            if(count($updateOrderDetailAttr) > 0 || count($updateOrderAttr) > 0){
                                $order_log_model = new OrderLog();
                                $order_log_model->order_no= $order_no;
                                $order_log_model->actions = OrderLog::UPDATE;
                                $orderDetailAttr = ["payment_id","payment_type","belong_sale_id",'remark','sale_price','sale_wechat_no','images'];
                                foreach ($orderDetailAttr as $attr){
                                    $order_log_model->$attr =  isset($updateOrderDetailAttr[$attr]) ? $updateOrderDetailAttr[$attr] : "-";
                                }
                                $orderAttr = ['paid_price','pay_status','pay_end_time','service_status','service_time'];
                                foreach ($orderAttr as $attr){
                                    $order_log_model->$attr =  isset($updateOrderAttr[$attr]) ? $updateOrderAttr[$attr] : "-";
                                }
                                if($order_log_model->save()){
                                    $this->in['status'] = true;
                                    $this->in['data'] = $order_detail_model->toArray();
                                }else{
                                    throw  new Exception("日志保存错误");
                                }
                            }else{
                                $this->in['status'] = true;
                                $this->in['data'] = $order_detail_model->toArray();
                            }
                        }else{
                            throw  new Exception("数据源保存错误");
                        }
                    }else{
                        throw  new Exception("数据保存错误");
                    }

                    $transaction->commit();
                }catch (Exception $e){
                    $this->in['status'] = false;
                    $this->in['message'] = $e->getMessage();
                    $transaction->rollBack();
                }
            }else{
                $this->in['message'] = $orderDetailForm->getErrors();
            }
        }
        return $this->in;
    }

    public function actionDelete(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $model = OrderDetail::findOne($post_data['id']);
            if($model){
                $model->is_deleted = OrderDetail::IS_DELETED;
                $order_model = (new Order())->getOrderInfo($model->order_no);
                $course_model = Course::findOne($model->course_id);
                $order_log_model = new OrderLog();
                $order_log_model->order_no = $model->order_no;
                $order_log_model->actions = OrderLog::DELETE;
                switch ($model->payment_type){
                    case  Order::PAYMENT_TYPE_1:
                        $order_model->paid_price -= $model->sale_price;

                        break;
                    case Order::PAYMENT_TYPE_2:
                        $order_model->paid_price -= $model->sale_price;
                        $order_model->pay_status = Order::PAY_STATUS_1;
                        $order_model->pay_end_time = 0;
                        $order_log_model->pay_status = $order_model->pay_status;
                        $order_log_model->pay_end_time = $order_model->pay_end_time;
                        break;
                    case Order::PAYMENT_TYPE_4:
                        $order_model->paid_price += $model->sale_price;
                        $course_model->refund_persons += 1;
                        break;
                }
                $order_log_model->paid_price = $order_model->paid_price;
                if($model->save() && $order_model->save() && $course_model->save() && $order_log_model->save()){
                    $this->in['status'] = true;
                    $this->in['message'] = '';
                }else{
                    $this->in['message']= "操作失败！";
                }
            }else{
                $this->in['message']= "数据不存在！";
            }
        }
        return $this->in;
    }
}