<?php
namespace common\models;
/**
 * Class OrderDetail
 * @package common\models
 * @property int $id
 * @property string $order_no
 * @property int $payment_id
 * @property int $payment_type
 * @property int $course_id
 * @property int $belong_teacher_id
 * @property int $belong_sale_id
 * @property int $belong_sale_group_id
 * @property string $sale_wechat_no
 * @property string $remark
 * @property string $images
 * @property double $sale_price
 * @property int $student_id
 * @property int $position
 * @property int $order_detail_status
 * @property int $create_time
 * @property int $update_time
 * @property int $is_deleted
 * @property int $status
 * @property int $operate_id
 */
class OrderDetail extends ParentActiveRecord{
    const ORDER_DETAIL_STATUS_1 = 1;//财务未审核
    const ORDER_DETAIL_STATUS_2 = 2;//财务审核
    public static $order_detail_status_maps = [
        self::ORDER_DETAIL_STATUS_1=>"未审核",
        self::ORDER_DETAIL_STATUS_2=>"已审核"
    ];
    public static function tableName()
    {
        return "{{%order_detail}}";
    }

    public function attributeLabels()
    {
        return parent::attributeLabels() +[
                'order_no'=>'订单',
                'payment_id'=>'付款方式',
                'payment_type'=>'付款类型',
                'course_id'=>'课程',
                'belong_teacher_id'=>'导师',
                'belong_sale_id'=>'销售',
                'belong_sale_group_id'=>'销售组',
                'remark'=>'备注',
                'sale_wechat_no'=>'销售微信',
                'sale_price'=>'金额',
                'student_id'=>'学员',
                'position'=>'位置',
                'order_detail_status'=>'财务状态',
                'images'=>'图片'
            ];
    }
}