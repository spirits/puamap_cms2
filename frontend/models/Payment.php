<?php
namespace frontend\models;
use common\utils\RedisUtils;

class Payment extends  \common\models\Payment{
    public function getFields(){
        return [
            [
                "label"=>$this->getAttributeLabel("id"),
                'field'=>"id",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("name"),
                'field'=>"name",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$this->getAttributeLabel("status"),
                'field'=>"status",
                'type'=>"Switch",
                'search'=>1,
                'style'=>"width:80px;"
            ],
            [
                "label"=>$this->getAttributeLabel("create_time"),
                'field'=>"create_time",
                'type'=>"String",
                'search'=>0,
                'style'=>"width:200px;"
            ],
            [
                "label"=>$this->getAttributeLabel("update_time"),
                'field'=>"update_time",
                'type'=>"String",
                'search'=>0,
                'style'=>"width:200px;"
            ],
            [
                "label"=>$this->getAttributeLabel("operate_id"),
                'field'=>"operate_id",
                'type'=>"String",
                'search'=>0,
                'style'=>"width:200px;"
            ]
        ];
    }

    /**
     * @return array
     * 表单数据
     */
    public function getFormFields(){
        return [
            [
                "label"=>$this->getAttrLabel('name'),
                'name'=>"name",
                'type'=>"String",
                'value'=>"",
                "rules"=>['required'=>true]
            ],
            [
                "label"=>$this->getAttrLabel('status'),
                'name'=>"status",
                'type'=>"Switch",
                'value'=>0,
                'options'=>[['label'=>"开启",'value'=>0],['label'=>"禁用",'value'=>1]]
            ]
        ];
    }




    public function getPage($page=1,$params=[]){
        $query = self::find()->where($this->baseWhere())
            ->orderBy(['id'=>SORT_DESC]);
        if($params){
            $query->andFilterWhere(['like','name',$params['name']]);

            if(isset($params['status']) && $params['status'] > -1){
                $query->andFilterWhere(['status'=>intval($params['status'])]);
            }
        }
        $offset = ($page - 1) * $this->pageSize;
        $totalCell = $query->count();
        $models= $query->offset($offset)->limit($this->pageSize)->all();
        $data = [];
        if($models){
            foreach ($models as $model) {
                $data[] = $model->toArray();
            }
        }
        return [
            'totalCell'=>$totalCell,
            'list'=>$data
        ];
    }


    public function paymentArray($real= false){
        $query = static::find()->select(['id','name'])->where($this->baseWhere());
        if($real){
            $query->andWhere(['status'=>self::TRUE_STATUS]);
        }
        return $query->asArray()->all();
    }
}