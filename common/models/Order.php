<?php
namespace common\models;
/**
 * Class Order
 * @package common\models
 * @property int $id
 * @property string $title
 * @property string $order_no
 * @property string $child_no
 * @property int $course_type
 * @property int $course_id
 * @property string $course_title
 * @property int $belong_teacher_id
 * @property string $belong_teacher_ratio
 * @property double $push_money
 * @property int $belong_sale_id
 * @property int $belong_sale_group_id
 * @property int $bonus
 * @property double $total_price
 * @property double $paid_price
 * @property double $coupon
 * @property double $big_price
 * @property double $small_price
 * @property double $seek_price
 * @property int $source_id
 * @property int $student_id
 * @property int $order_status
 * @property int $service_status
 * @property int $duration
 * @property int $small_duration
 * @property int $service_time
 * @property int $pay_status
 * @property int $pay_end_time
 * @property int $create_time
 * @property int $update_time
 * @property int $is_deleted
 * @property int $status
 * @property int $operate_id
 * @property string $remark
 *
 */
class Order extends ParentActiveRecord{
    const ORDER_STATUS_1 = 1;//1.未审
    const ORDER_STATUS_3 = 3;//3.已审
    const ORDER_STATUS_4 = 4;//4.退款
    const ORDER_STATUS_5 = 5;//5.转单
    const ORDER_STATUS_6 = 6;//6.接单
    const ORDER_STATUS_7 = 7;//7.转课
    const ORDER_STATUS_8 = 8;//8.接课
    /**
     * @var array
     * 订单可做条件
     */
    public static $return_maps = [self::ORDER_STATUS_3,self::ORDER_STATUS_6,self::ORDER_STATUS_7,self::ORDER_STATUS_8];

    public static $order_status_maps = [
        self::ORDER_STATUS_1=>"未审",
        self::ORDER_STATUS_3=>'已审',
        self::ORDER_STATUS_4=>"退款",
        self::ORDER_STATUS_5=>'转单',
        self::ORDER_STATUS_6=>"接单",
        self::ORDER_STATUS_7=>"转课",
        self::ORDER_STATUS_8=>"接课"
    ];

    const SERVICE_STATUS_1 = 1;//未服务
    const SERVICE_STATUS_2 = 2;//服务中
    const SERVICE_STATUS_3 = 3;//暂停
    const SERVICE_STATUS_4 = 4;//继续服务
    const SERVICE_STATUS_5 = 5;//服务完成

    public static $able_service_maps = [self::SERVICE_STATUS_2,self::SERVICE_STATUS_4];

    public static $service_status_maps = [
        self::SERVICE_STATUS_1 => "未服务",
        self::SERVICE_STATUS_2 =>"服务中",
        self::SERVICE_STATUS_3 =>"暂停",
        self::SERVICE_STATUS_4 => "继续服务",
        self::SERVICE_STATUS_5 =>"服务完成"
    ];

    const PAY_STATUS_1 = 1;//未支付完
    const PAY_STATUS_2 = 2;//支付完成

    public static $pay_status_maps = [
        self::PAY_STATUS_1 => "未支付完",
        self::PAY_STATUS_2 => "支付完成"
    ];

    const PAYMENT_TYPE_0 = 0;//定金
    const PAYMENT_TYPE_1 = 1;//回款
    const PAYMENT_TYPE_2 = 2;//尾款
    const PAYMENT_TYPE_3 = 3;//全款
    const PAYMENT_TYPE_4 = 4;//退款

    public static $payment_type_maps = [
        self::PAYMENT_TYPE_0=>"定金",
        self::PAYMENT_TYPE_1=>"回款",
        self::PAYMENT_TYPE_2=>"尾款",
        self::PAYMENT_TYPE_3=>"全款",
        self::PAYMENT_TYPE_4=>"退款"
    ];

    public static function tableName()
    {
        return "{{%order}}";
    }

    public function attributeLabels()
    {
        return parent::attributeLabels() + [
                'title'=>'标题',
                'order_no'=>"订单号",
                'child_no'=>"接单号",
                'course_type'=>'课程类型',
                'course_id'=>'课程',
                'course_title'=>'课程名称',
                'belong_teacher_id'=>'导师',
                'belong_teacher_ratio'=>'分层比例',
                'belong_sale_id'=>'销售',
                'belong_sale_group_id'=>"销售组",
                'bonus'=>"奖金",
                'push_money'=>'提成',
                'total_price'=>'总价',
                'paid_price'=>'已付',
                'coupon'=>'优惠',
                'big_price'=>'大课',
                'small_price'=>'小课',
                'seek_price'=>'咨询',
                'source_id'=>'来源',
                'student_id'=>'学生',
                'order_status'=>'订单状态',
                'service_status'=>'服务',
                'service_time'=>'服务时间',
                'pay_status'=>'支付',
                'pay_end_time'=>'时间',
                'remark'=>'备注',
                'duration'=>'服务时长',
                'small_duration'=>'小课节数'
            ];
    }

    public function getOrderInfo($order_no){
        return self::find()->where($this->baseWhere())->andWhere(['order_no'=>$order_no])
            ->andWhere(['or',['order_status'=>static::ORDER_STATUS_3],['order_status'=>static::ORDER_STATUS_6],['order_status'=>static::ORDER_STATUS_7],['order_status'=>static::ORDER_STATUS_8]])->one();
    }



   public function beforeSave($insert)
   {
       if(!$this->push_money || $this->push_money <= 0){
           if($this->service_status == static::SERVICE_STATUS_5){
               $ratio = Role::getTeacherRatio();
               $ratio = $ratio ? json_decode($ratio,true) : [];
               if($ratio){
                   $course_service_model = CourseService::findOne(['order_no'=>$this->order_no]);
                   if($course_service_model){
                       $big = $course_service_model->big_process / $this->duration * $this->big_price * $ratio['big'];
                       $seek = $course_service_model->big_process / $this->duration * $this->seek_price * $ratio['seek'];
                       $small = $course_service_model->small_process / $this->small_duration * $this->small_price * $ratio['small'];
                       $this->push_money = round($big + $small + $seek,2);
                   }
               }

           }
       }

       return parent::beforeSave($insert); // TODO: Change the autogenerated stub
   }


}