<?php
namespace frontend\controllers;

use common\utils\RedisUtils;
use frontend\forms\LoginForm;
use frontend\models\Admin;
use \yii\web\Controller;
class LoginController extends Controller{

    public $in = [
        'status'=>false,
        'message'=>'',
        'data'=>''
    ];
    public $layout =false;
    public function actionLogin(){

        return $this->render($this->action->id);
    }

    /**
     * @return array
     * 登录
     */
    public function actionSignIn(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $data["LoginForm"] =$post_data;
            $loginForm = new LoginForm();
            $loginForm->load($data);
            if($access_token= $loginForm->login()){
                $this->in['status'] = true;
                $this->in['data'] =[
                    'username'=> RedisUtils::getLogin($access_token,['nickname']),
                    'token'=> $access_token
                    ];

            }else{
                $errors  = $loginForm->getFirstErrors();
                $this->in['message'] = isset($errors['username']) ? $errors['username'] : (isset($errors['password']) ? $errors['password'] :"");
            }
        }
        return $this->in;
    }

    public function actionSignOut(){
        $access_token = \Yii::$app->request->get("access_token",'');
        RedisUtils::deleteLoginKey($access_token);
        $this->in['status'] = true;
        return $this->in;

    }

    public function actionUpdatePassword(){
        if(\Yii::$app->request->isPost){
            $access_token = \Yii::$app->request->get("access_token",'');
            $password = \Yii::$app->request->post('password',"");
            if($password){
                $admin = Admin::findOne(['access_token'=>$access_token]);
                if($admin){
                    $admin->password = \Yii::$app->security->generatePasswordHash($password);
                    $admin->update();
                    $this->in['status'] = true;
                }else{
                    $this->in['message'] ="user";
                }
            }else{
                $this->in['message'] ="密码不能为空";
            }

        }
        return $this->in;
    }
}