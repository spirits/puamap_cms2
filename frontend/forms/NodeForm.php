<?php
namespace frontend\forms;
use yii\base\Model;

class NodeForm extends Model{
    public $name;
    public $route;
    public $parent_id;
    public $id;
    public $status;
    public function rules()
    {
        return parent::rules() + [
                [['name','route'],'required'],
                [['parent_id','id','status'],'integer'],
                [['status'],'default','value'=>0]
            ];
    }
}