<?php
namespace frontend\forms;
use yii\base\Model;

class RoleForm extends  Model{
    public $parent_id;
    public $name;
    public $income;
    public $income_status;
    public $big;
    public $small;
    public $seek;
    public $status;

    public function rules()
    {
        return [
            [['parent_id','name','income'],'required'],
            [['income'],'double'],
            [['income_status','status'],'integer'],
            [['seek','big','small'],'double','max'=>"0.99",'min'=>0],
            [['income_status'],'default','value'=>1],
            [['status'],'default','value'=>0]
        ];
    }
}