<?php
namespace frontend\filters;
use common\utils\RedisUtils;
use frontend\exceptions\AuthException;
use yii\base\ActionFilter;

class AuthFilterAction extends ActionFilter{

    public function beforeAction($action)
    {

        if(!$this->checkLogin()){
            throw  new AuthException();
        }
        return parent::beforeAction($action);
    }

    /**
     * @return bool
     * 判读登录
     */
    public function checkLogin(){
       $queryParams = \Yii::$app->request->queryParams;
       if(!isset($queryParams['access_token'])){
           return false;
       }
       if(!RedisUtils::existsLogin($queryParams['access_token'])){
           return false;
       }
        return true;

    }

}
