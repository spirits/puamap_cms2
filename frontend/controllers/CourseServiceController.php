<?php
namespace frontend\controllers;
use frontend\models\AccountLog;
use frontend\models\CourseService;
use frontend\models\CourseServiceLog;
use frontend\models\Order;
use yii\db\Exception;

class CourseServiceController extends BaseController{
    public function actionIndex(){
        $model = new CourseService();
        $data['fields'] = $model->getFields();
        $data['formFields'] = $model->getFormFields();
        $data['controller'] = $model->getAuthController();
        $data['tableChose'] = $model->getTableChoseSelect();
        $data['formChose']= [];
        $pageData = $model->getPage();
        $data['items'] = $pageData['list'];
        $data['pages'] = $model->getPageInfo();
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }

    public function actionPage(){
        $page = \Yii::$app->request->post('page',1);
        $params = \Yii::$app->request->post();
        $model = new CourseService();
        $data = $model->getPage($page,$params);
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }

    /**
     * @return array
     * 添加日志
     */
    public function actionAddLog(){
        if(\Yii::$app->request->isPost){
            $data = \Yii::$app->request->post();
            $course_service_model = CourseService::findOne($data['id']);
            if($course_service_model){
                if(trim($data['remark'])){
                    $course_service_log_model = new CourseServiceLog();
                    $course_service_log_model->service_id = $course_service_model->id;
                    $course_service_log_model->service_status = $course_service_model->service_status;
                    $course_service_log_model->remark = trim($data['remark']);
                    if($course_service_log_model->save()){
                        $this->in['status'] = true;
                    }else{
                        $this->in['message'] = "备注信息失败";
                    }
                }else{
                    $this->in['message'] = "备注信息不能为空";
                }
            }else{
                $this->in['message'] = "改服务学员不存在";
            }
        }
        return $this->in;
    }

    /**
     * 打卡
     */
    public function actionAddCard(){
        if(\Yii::$app->request->isPost){
            $transaction = \Yii::$app->db->beginTransaction();
            try{
                $data = \Yii::$app->request->post();
                $course_service_model = CourseService::findOne($data['id']);
                if($course_service_model){
                    $order_model = (new Order)->getOrderInfo($course_service_model->order_no);
                    if(in_array($course_service_model->service_status,Order::$able_service_maps) && $order_model && $course_service_model->small_process < $order_model->small_duration){
                        $account_log_model = new AccountLog();
                        $account_log_model->order_no = $course_service_model->order_no;
                        $account_log_model->actions = AccountLog::ACTIONS_CARD;
                        if($course_service_model->small_process < ($order_model->small_duration-1)){
                            if($account_log_model->save() && $course_service_model->updateCounters(['small_process'=>1])){
                                $this->in['status'] = true;
                                $this->in['data'] = $course_service_model->small_process;
                            }else{
                                throw  new Exception("打卡失败");
                            }
                        }else{
                            $course_service_model->small_process += 1;
                            if($course_service_model->big_process >= $order_model->duration){
                                $order_model->service_status = Order::SERVICE_STATUS_5;
                                $order_model->service_time = time();
                                $course_service_model->service_status = $order_model->service_status;
                                if($account_log_model->save() && $course_service_model->save() && $order_model->save()){
                                    $this->in['status'] = true;
                                    $this->in['data'] = $course_service_model->small_process;
                                }else{
                                    throw  new Exception("打卡失败");
                                }
                            }else{
                                if($account_log_model->save() && $course_service_model->updateCounters(['small_process'=>1])){
                                    $this->in['status'] = true;
                                    $this->in['data'] = $course_service_model->small_process;
                                }else{
                                    throw  new Exception("打卡失败");
                                }
                            }
                        }
                    }else{
                        throw  new Exception("当前不可打卡");
                    }
                }else{
                    throw  new Exception("该服务学员不存在");
                }
                $transaction->commit();
            }catch (Exception $e){
                $this->in['status'] = false;
                $this->in['message'] = $e->getMessage();
                $transaction->rollBack();
            }

        }
        return $this->in;
    }
}