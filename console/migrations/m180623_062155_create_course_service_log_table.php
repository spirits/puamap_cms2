<?php

use yii\db\Migration;

/**
 * Handles the creation of table `course_service_log`.
 */
class m180623_062155_create_course_service_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%course_service_log}}', [
            'id' => $this->primaryKey(),
            'service_id'=>$this->integer()->notNull()->comment("服务"),
            'service_status'=>$this->tinyInteger()->comment("服务状态"),
            'remark'=>$this->string()->defaultValue("")->comment("备注"),
            'create_time'=>$this->integer(11)->comment("创建时间"),
            'update_time'=>$this->integer(11)->comment("更新时间"),
            'is_deleted'=>$this->smallInteger(1)->defaultValue(0)->comment("是否删除"),
            'status'=>$this->smallInteger(1)->defaultValue(0)->comment("是否结束"),
            'operate_id'=>$this->integer(11)->defaultValue(1)->comment("操作者")
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%course_service_log}}');
    }
}
