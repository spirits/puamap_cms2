<?php
namespace frontend\models;
class Sale extends  Admin{
    public function getFields()
    {
        $fields = parent::getFields();
        $add[] = [
            "label"=>$this->getAttributeLabel("leader_id"),
            'field'=>"leader_id",
            'type'=>"ChosenSelect",
            'search'=>1,
            'style'=>"width:180px;"
        ];
        array_splice($fields,3,1,$add);
        return $fields;
    }

    public function getFormFields(){
        return [
            [
                "label"=>$this->getAttrLabel('leader_id'),
                'name'=>"leader_id",
                'type'=>"ChosenSelect",
                'value'=>0,
                "rules"=>['required'=>true]
            ],
            [
                "label"=>$this->getAttrLabel('username'),
                'name'=>"username",
                'type'=>"String",
                'value'=>"",
                "rules"=>['required'=>true]
            ],
            [
                "label"=>$this->getAttrLabel('nickname'),
                'name'=>"nickname",
                'type'=>"String",
                'value'=>"",
            ],
            [
                "label"=>$this->getAttrLabel('password'),
                'name'=>"password",
                'type'=>"Password",
                'value'=>"",
            ],
            [
                "label"=>$this->getAttrLabel('status'),
                'name'=>"status",
                'type'=>"Switch",
                'value'=>0,
                'options'=>[['label'=>"开启",'value'=>0],['label'=>"禁用",'value'=>1]]
            ]
        ];
    }

}