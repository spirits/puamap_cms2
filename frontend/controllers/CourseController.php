<?php
namespace frontend\controllers;
use frontend\forms\CourseForm;
use frontend\models\Admin;
use frontend\models\Course;

class CourseController extends BaseController{
    public function actionIndex(){
        $model = new Course();
        $adminModel = new Admin();
        $data['fields'] = $model->getFields();
        $data['formFields'] = $model->getFormFields();
        $data['controller'] = $model->getAuthController();
        $data['tableChose'] = $model->getTableChoseSelect();
        $data['ratio'] = $model->ratio;
        $pageData =  $model->getPage();
        $data['items'] = $pageData['list'];
        $data['pages'] = $model->getPageInfo();
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }
    public function actionPage(){
        $page = \Yii::$app->request->post('page',1);
        $params = \Yii::$app->request->post();
        $model = new Course();
        $data = $model->getPage($page,$params);
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }

    public function actionCreate(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $data["CourseForm"] =$post_data;
            $courseForm = new CourseForm();
            $courseForm->load($data);
            if($courseForm->validate()){
                $model = new Course();

                $model->title = $courseForm->title;
                $model->total_price = $courseForm->total_price;
                $model->course_type = $courseForm->course_type;
                if($courseForm->course_type == Course::COURSE_TYPE_1){
                    $model->belong_teacher_id = $courseForm->belong_teacher_id;
                    $model->duration = $courseForm->duration;
                    $model->small_duration = $courseForm->small_duration;
                }
                $model->status = $courseForm->status;
                if($model->save()){
                    $this->in['status'] = true;
                    $pageData =  $model->getPage();
                    $this->in['data'] = $pageData['list'];
                }
            }else{
                $this->in['status'] = false;
                $this->in['message'] = "验证失败";
            }
        }
        return $this->in;
    }

    public function actionUpdate(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $data["CourseForm"] =$post_data;
            $courseForm = new CourseForm();
            $courseForm->load($data);
            if($courseForm->validate()){
                $model = Course::findOne($post_data['id']);
                $model->belong_teacher_id = $courseForm->belong_teacher_id;
                $model->title = $courseForm->title;
                $model->total_price = $courseForm->total_price;
                $model->duration = $courseForm->duration;
                $model->small_duration = $courseForm->small_duration;
                $model->status = $courseForm->status;
                if($model->save()){
                    $this->in['status'] = true;
                    $this->in['data'] = $model->toArray();
                }
            }else{
                $this->in['status'] = false;
                $this->in['message'] = "验证失败";
            }
        }
        return $this->in;
    }
    public function actionDelete(){
        if(\Yii::$app->request->isPost){
            $post_data = \Yii::$app->request->post();
            $model = Course::findOne($post_data['id']);
            if($model){
                $model->is_deleted = Course::IS_DELETED;
                if($model->save()){
                    $this->in['status'] = true;
                    $this->in['message'] = '';
                }else{
                    $this->in['message']= "操作失败！";
                }
            }else{
                $this->in['message']= "数据不存在！";
            }
        }
        return $this->in;
    }
}