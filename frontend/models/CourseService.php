<?php
namespace frontend\models;
use common\utils\RedisUtils;
use yii\db\Query;

class CourseService extends \common\models\CourseService{





    public function getFields(){
        $order_model = new Order();
        $student_model= new Student();
        return [
            [
                "label"=>$this->getAttributeLabel("id"),
                'field'=>"id",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("order_no"),
                'field'=>"order_no",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$order_model->getAttributeLabel("course_id"),
                'field'=>"course_id",
                'type'=>"ChosenSelect",
                'search'=>1,
                'style'=>"width:200px;"
            ],
            [
                "label"=>$order_model->getAttributeLabel("belong_teacher_id"),
                'field'=>"belong_teacher_id",
                'type'=>"ChosenSelect",
                'search'=>1,
                'style'=>"width:240px;"
            ],
            [
                "label"=>$order_model->getAttributeLabel("service_status"),
                'field'=>"service_status",
                'type'=>"ChosenSelect",
                'search'=>1
            ],
            [
                "label"=>$student_model->getAttributeLabel("wechat_name"),
                'field'=>"wechat_name",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$student_model->getAttributeLabel("wechat_no"),
                'field'=>"wechat_no",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$student_model->getAttributeLabel("mobile"),
                'field'=>"mobile",
                'type'=>"String",
                'search'=>1
            ],
            [
                "label"=>$student_model->getAttributeLabel("age"),
                'field'=>"age",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("big_process"),
                'field'=>"big_process",
                'type'=>"Process",
                'search'=>0,
            ],
            [
                "label"=>$this->getAttributeLabel("small_process"),
                'field'=>"small_process",
                'type'=>"Process",
                'search'=>0,
            ],
            [
                "label"=>$this->getAttributeLabel("remark"),
                'field'=>"remark",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>$this->getAttributeLabel("create_time"),
                'field'=>"create_time",
                'type'=>"String",
                'search'=>0,
                'style'=>'width:200px;display:inline-block'
            ],
            [
                "label"=>$this->getAttributeLabel("operate_id"),
                'field'=>"operate_id",
                'type'=>"String",
                'search'=>0,
            ]
        ];
    }

    public function getFormFields(){
        return [
            [
                "label"=>$this->getAttrLabel("remark"),
                'name'=>"remark",
                'type'=>"String",
                'value'=>"",
                "rules"=>['required'=>true]
            ]
        ];
    }


    public function getTableChoseSelect(){
        $course_model = new Course();
        $admin_model = new Admin();
        $service_maps = [];
        foreach (Order::$service_status_maps as $k=>$v){
            if($k != Order::SERVICE_STATUS_1){
                $service_maps[$k]=$v;
            }
        }
        return [
            'course_id'=>$course_model->courseArray(),
            'belong_teacher_id'=>$admin_model->getTeacherList(),
            'service_status'=>$this->formatArray($service_maps),
        ];
    }

    public function getPage($page=1,$params=[]){
        $offset = ($page - 1) * $this->pageSize;
        $query = new Query();
        $field = "cs.id as id,cs.order_no,cs.service_status,cs.big_process,cs.small_process,cs.remark,cs.create_time,cs.belong_teacher_id,cs.operate_id as operate_id,cs.create_time as create_time,";
        $field .= "o.course_id as course_id,order_status,duration,small_duration,";
        $field .="s.wechat_name as wechat_name,s.wechat_no as wechat_no,s.age as age,s.mobile as mobile";
        $prefix = \Yii::$app->db->tablePrefix;
        $query->select($field)->from($prefix."course_service as cs")
            ->leftJoin($prefix."order as o",'o.order_no = cs.order_no')
            ->leftJoin($prefix."student as s",'cs.student_id = s.id')
            ->where("cs.is_deleted=".self::NO_DELETED)
            ->andWhere(['or',['order_status'=>Order::ORDER_STATUS_3],['order_status'=>Order::ORDER_STATUS_6],['order_status'=>Order::ORDER_STATUS_7],['order_status'=>Order::ORDER_STATUS_8]]);

        $role_id = RedisUtils::getLogin($this->getAccessToken(),['role_id']);
        $uid = RedisUtils::getLogin($this->getAccessToken(),['uid']);
        if($role_id){
            switch ($role_id){
                case Role::TEACHER_NO;
                    $query->andFilterWhere(["=",'cs.belong_teacher_id',$uid]);
                    break;
                case Role::ASSISTANT_NO:
                    $leader_id = RedisUtils::getLogin($this->getAccessToken(),['leader_id']);
                    if(!$leader_id){
                        $leader_id =  Admin::findOne($uid)->leader_id;
                    }
                    $query->andFilterWhere(["=",'cs.belong_teacher_id',$leader_id]);
                    break;

            }
        }
        if($params){
            $query->andFilterWhere(['like','cs.order_no',$params['order_no']]);
            $query->andFilterWhere(['like','s.wechat_name',$params['wechat_name']]);
            $query->andFilterWhere(['like','s.wechat_no',$params['wechat_no']]);
            $query->andFilterWhere(['like','s.mobile',$params['mobile']]);
            if(isset($params['course_id']) && $params['course_id'] > -1){
                $query->andFilterWhere(["=",'o.course_id',$params['course_id']]);
            }
            if(isset($params['belong_teacher_id']) && $params['belong_teacher_id'] > -1){
                $query->andFilterWhere(["=",'cs.belong_teacher_id',$params['belong_teacher_id']]);
            }
            if(isset($params['service_status']) && $params['service_status'] > -1){
                $query->andFilterWhere(["=",'cs.service_status',$params['service_status']]);
            }
        }
        $totalCell = $query->count();
        $data = $query->offset($offset)->limit($this->pageSize)->orderBy('cs.id desc')->all();
        if($data){
            foreach ($data as &$v){
                $v['create_time'] = $v['create_time'] ? date($this->timeFormat,$v['create_time']) : "-";
                $v['operate_id'] = $v['operate_id'] ? RedisUtils::getNickname($v['operate_id']) : "-";
            }
        }
        return [
            'totalCell'=>$totalCell,
            'list'=>$data
        ];
    }
}