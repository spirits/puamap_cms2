<?php
namespace frontend\controllers;
use frontend\filters\AuthFilterAction;
use frontend\filters\PermissionFilterAction;
use frontend\models\Node;
use \yii\web\Controller;
use yii\web\Request;

class BaseController extends Controller{
    public $layout = false;
    public $in = [
        'status'=>false,
        'message'=>'',
        'data'=>''
    ];
    public function behaviors()
    {
        $behaviors = [
            [
                'class'=>AuthFilterAction::className()
            ],
            [
                'class'=>PermissionFilterAction::className()
            ]
        ];
        return parent::behaviors()+$behaviors;
    }
    public function formatTime($timer){
        return date("Y-m-d H:i:s",$timer);
    }

    public function getAccessToken(){
        $request = new Request();
        $params =$request->queryParams;
        return isset($params['access_token']) ? $params['access_token'] : "";
    }
}
