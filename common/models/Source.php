<?php
namespace common\models;
/**
 * Class Pay
 * @package common\models
 * @property int $id
 * @property string $name
 * @property int $create_time
 * @property int $update_time
 * @property int $id_deleted
 * @property int $status
 * @property int $operate_id
 */
class Source extends ParentActiveRecord{
    public static  function tableName()
    {
        return "{{%source}}";
    }

    public function attributeLabels()
    {
        return parent::attributeLabels()+[
                'name'=>"名称"
            ];
    }

}