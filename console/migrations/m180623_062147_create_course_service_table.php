<?php

use yii\db\Migration;

/**
 * Handles the creation of table `course_service`.
 */
class m180623_062147_create_course_service_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%course_service}}', [
            'id' => $this->primaryKey(),
            'belong_teacher_id'=>$this->integer()->notNull()->comment("导师"),
            'student_id'=>$this->integer()->notNull()->comment("用户id"),
            "order_no"=>$this->string()->notNull()->comment("订单id"),
            "big_process"=>$this->integer(10)->defaultValue(0)->comment("大课进度"),
            'small_process'=>$this->integer(10)->defaultValue(0)->comment("小课进度"),
            'service_status'=>$this->tinyInteger()->comment("服务状态"),
            'remark'=>$this->string()->defaultValue("")->comment("备注"),
            'create_time'=>$this->integer(11)->comment("创建时间"),
            'update_time'=>$this->integer(11)->comment("更新时间"),
            'is_deleted'=>$this->smallInteger(1)->defaultValue(0)->comment("是否删除"),
            'status'=>$this->smallInteger(1)->defaultValue(0)->comment("是否结束"),
            'operate_id'=>$this->integer(11)->defaultValue(1)->comment("操作者")
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%course_service}}');
    }
}
