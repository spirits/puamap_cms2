<?php
namespace frontend\controllers;
use common\utils\RedisUtils;
use frontend\models\Admin;
use frontend\models\Order;
use frontend\models\OrderDetail;
use frontend\models\Role;

class SaleGroupController extends BaseController{

    public function actionIndex(){
        $start_time = \Yii::$app->request->post("start_time","");
        $end_time = \Yii::$app->request->post("end_time","");
        $data['fields'] = $this->getFields();
        $data['items'] = $this->getSaleGroupIncome($start_time,$end_time);
        $this->in['data'] = $data;
        $this->in['status'] = true;
        return $this->in;
    }

    public function getFields(){
        return [
            [
                "label"=>"ID",
                'field'=>"id",
                'type'=>"String",
                'search'=>0,
                'style'=>"width:80px;"
            ],
            [
                "label"=>"销售组",
                'field'=>"name",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>"款项",
                'field'=>"number",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>"收款",
                'field'=>"income",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>"退款",
                'field'=>"refund",
                'type'=>"String",
                'search'=>0
            ],
            [
                "label"=>"收款总数",
                'field'=>"total",
                'type'=>"String",
                'search'=>0
            ],
        ];
    }

    public function getSaleGroupIncome($start_time,$end_time){
        $key =  ($start_time || $end_time)  ?  'sale_group_income_':'sale_group_income';
        if($start_time){
            $key .= $end_time ? "".strtotime($start_time)."_" : "".strtotime($start_time);
        }
        if($end_time){
            $key .="".strtotime($end_time);
        }
        $admin_model = new Admin();
        $sale_list = $admin_model->getSaleLeaderList(true);
        $query = OrderDetail::find()->select('belong_sale_group_id,sum(sale_price) as sale_price,count(id) as number')->where($admin_model->baseWhere());
        if($start_time){
            $stime = strtotime($start_time." 00:00:00");
            $query->andWhere(['>=','create_time',$stime]);
        }
        if($end_time){
            $etime = strtotime($end_time." 23:59:59");
            $query->andWhere(['<=','create_time',$etime]);
        }
        $role_id = RedisUtils::getLogin($this->getAccessToken(),['role_id']);
        $uid = RedisUtils::getLogin($this->getAccessToken(),['uid']);
        if($role_id){
            switch ($role_id){
                case Role::SALE_NO;
                    $leader_id = RedisUtils::getLogin($this->getAccessToken(),['leader_id']);
                    if(!$leader_id){
                        $leader_id =  Admin::findOne($uid)->leader_id;
                    }
                    $query->andWhere(['belong_sale_group_id'=>$leader_id]);
                    break;
                case Role::SALE_LEADER_NO:
                    $query->andWhere(['belong_sale_group_id'=>$uid]);
                    break;

            }
        }
        $order_models = $query->groupBy(['belong_sale_group_id'])->asArray()->all();
        if($sale_list){
            foreach ($sale_list as &$item){
                $item['income'] = 0;
                $item['refund'] = 0;
                $item['number'] = 0;
                $item['total'] =  0;
                if($order_models){
                    foreach ($order_models as $v){
                        if($v['belong_sale_group_id'] == $item['id']){
                            $item['income'] = $v['sale_price'];
                            $item['total'] = $v['sale_price'];
                            $item['number'] = $v['number'];
                            break;
                        }
                    }
                }
            }
            $query2 = OrderDetail::find()->select('belong_sale_group_id,sum(sale_price) as sale_price')->where($admin_model->baseWhere())->andWhere(['payment_type'=>Order::PAYMENT_TYPE_4]);
            if($start_time){
                $stime = strtotime($start_time." 00:00:00");
                $query2->andWhere(['>=','create_time',$stime]);
            }
            if($end_time) {
                $etime = strtotime($end_time." 23:59:59");
                $query2->andWhere(['<=','create_time',$etime]);
            }
            $refund_models = $query2->groupBy(['belong_sale_group_id'])->asArray()->all();
            foreach ($sale_list as &$item){
                if($refund_models){
                    foreach ($refund_models as $v){
                        if($v['belong_sale_group_id'] == $item['id']){
                            $item['income'] -= $v['sale_price'] ;
                            $item['total'] -= ($v['sale_price'] * 2);
                            break;
                        }
                    }
                }
            }
        }
        if($sale_list){
            \Yii::$app->redis->set($key,json_encode($sale_list));
            RedisUtils::SetExpire($key);
        }
        return $sale_list ? $sale_list : [];
    }
}