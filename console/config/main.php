<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@common'=>dirname(dirname(__DIR__)) . '/common',
        '@frontend'=>dirname(dirname(__DIR__)) . '/frontend',
        '@console'=>dirname(dirname(__DIR__)) . '/console'
    ],
    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
          ],
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db'=>[
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=rdsref3zq63ky6xefv0jepublic.mysql.rds.aliyuncs.com;dbname=yihe_from',
            'username' => 'tom',
            'password' => 'pua1900110',
            'charset' => 'utf8',
            'tablePrefix' => 'dna_',
        ],
    ],
    'params' => $params,
];
