<?php
namespace frontend\forms;
use yii\base\Model;

class AssistantForm extends Model{
    public $username;
    public $nickname;
    public $password;
    public $leader_id;
    public $status;

    public function rules()
    {
        return parent::rules() + [
                [['username','leader_id'],'required'],
                [['username','password','nickname'],'string'],
                [['leader_id','status'],'integer'],
                [['status'],'default','value'=>0]
            ];
    }
}